# -*- coding: utf-8 -*-
import time

__author__ = 'marcel'

import re
import lib.ProxerRequest
import lib.ProxerSerie

try:
    import json
except:
    import simplejson as json

try:
    import youtube_dl.YoutubeDL as ydl
    __youtube_dl__ = True
except Exception as e:
    __youtube_dl__ = False

__urlFinder__ = re.compile("source type=\".*\"")
__urlSrcFinder__ = re.compile("src=\".*?\"")
__crunchyroll_config__ = re.compile("{\"config_url\".*?}", re.DOTALL)
__crunchyroll_xml_url__ = re.compile("<default:blockingClickthroughUrl>.*?</")

__priority_list__ = {"Proxer-Stream": 1, "Streamcloud": 4, "MP4Upload": 2, "other": 60, "Novamov": 3, "Crunchyroll": 107, "VideoWeed": 5}


class ProxerVideo:
    @staticmethod
    def find_video(ep: lib.ProxerSerie.Episode, language, status, check_other_langs=True, ignore_str=None):

        url = None
        listt = []

        if language is not None and language != "":
            if not ep.loadHosters(language):
                if check_other_langs:
                    return ProxerVideo.find_video(ep, None, status, check_other_langs=check_other_langs, ignore_str=ignore_str)
                return None
            listt = ep.getHostersByType(language)
            if listt is None:
                return
            # Setting priority
            for item in listt:
                priority = int(__priority_list__.get(item["name"], __priority_list__.get("other", 100)))
                item["priority"] = priority
            from operator import itemgetter
            listt = sorted(listt, key=itemgetter('priority'))
            for item in listt:
                val = None
                if "Proxer-Stream" in item["name"]:
                    val = ProxerVideo.get_proxer_video(item["replace"].replace("#", item["code"]), ep.pxreq)
                elif "Streamcloud" in item["name"]:
                    try:
                        val = "http://" + ProxerVideo.get_streamcloud_video(item["code"], ep.pxreq, status)
                    except:
                        try:
                            val = ProxerVideo.get_streamcloud_video(item["replace"].replace("#", item["code"]), ep.pxreq, status)
                        except:
                            print("Streamcloud video offline")
                elif "MP4Upload" in item["name"]:
                    val = ProxerVideo.get_mp4upload_video(item["replace"].replace("#", item["code"]), ep.pxreq)
                elif "Clipfish" in item["name"]:
                    val = ProxerVideo.get_clipfish_video(item["replace"].replace("#", item["code"]), ep.pxreq)
                elif "Novamov" in item["name"] or "VideoWeed" in item["name"]:
                    val = ProxerVideo.get_novamov_video(item["replace"].replace("#", item["code"]), ep.pxreq)
                elif "Crunchyroll" in item["name"] and __youtube_dl__:
                    val = ProxerVideo.crunchyroll(item["replace"].replace("#", item["code"]), ep.pxreq, status)
                else:
                    val = ProxerVideo.youtube_dl(item["replace"].replace("#", item["code"]), download=True, status=status)
                if ignore_str is not None and val is not None:
                    if ignore_str in val:
                        val = None

                ###################### Check if online ################################
                if val is not None:
                    if "http:" not in val and "https:" not in val:
                        val = "http://" + val
                        val = val.replace("////", "//")
                    from requests.exceptions import InvalidSchema
                    try:
                        if ep.pxreq.is_online(val):
                            return val
                        else:
                            time.sleep(0.75)
                            if ep.pxreq.is_online(val):
                                return val
                    except InvalidSchema as e:
                        print("File may or may not online")
                        return val
                    except Exception as e:
                        time.sleep(0.75)
                        try:
                            if "evented" in val:
                                return val
                            if ep.pxreq.is_online(val):
                                return val
                        except Exception as e:
                            print("File is offline")

            print("Kein Proxer-Stream für diese Sprache verlinkt! (" + language + ")")

        if check_other_langs:
            for type in ep.get_subtypes().keys():
                url = ProxerVideo.find_video(ep, type, status, False, ignore_str)
                if url is not None:
                    return url

        if ignore_str is not None:
            print("Not ignoring the errored URL!")
            return ProxerVideo.find_video(ep, language, status, check_other_langs, None)
        return None

    @staticmethod
    def get_proxer_video(url, pxreq):
        strpage, r = pxreq.request(url, None, retry_count=2)

        try:
            fuzzyUrl = __urlFinder__.search(strpage).group(0)
            srcUrl = __urlSrcFinder__.search(fuzzyUrl).group(0)
            return srcUrl.replace("src=", "").replace("\"", "")
        except AttributeError:
            return None

    @staticmethod
    def get_streamcloud_video(url, requester: lib.ProxerRequest.ProxerRequest, status, retry=True):
        if url is None:
            return None
        strpage, r = requester.request(url)
        if "btn_download" in strpage:
            pat1 = '<input type="hidden" name="fname" value="(.*?)">'
            pat2 = '<input type="hidden" name="hash" value="(.*?)">'
            pat3 = '<input type="hidden" name="id" value="(.*?)">'
            pat4 = '<input type="submit" name="imhuman" id="btn_download" class="button gray" value="(.*?)">'
            pat5 = '<input type="hidden" name="op" value="(.*?)">'
            pat6 = '<input type="hidden" name="referer" value="(.*?)">'
            pat7 = '<input type="hidden" name="usr_login" value="(.*?)">'
            pat8 = 'file: "http://(.*?)",'

            mo = re.search(pat1, strpage)
            mo2 = re.search(pat2, strpage)
            mo3 = re.search(pat3, strpage)
            mo4 = re.search(pat4, strpage)
            mo5 = re.search(pat5, strpage)
            mo6 = re.search(pat6, strpage)
            mo7 = re.search(pat7, strpage)
            fname = mo.group(1)
            hash = mo2.group(1)
            id = mo3.group(1)
            imhuman = mo4.group(1)
            op = mo5.group(1)
            referer = mo6.group(1)
            usr_login = mo7.group(1)

            mydata = [('fname', fname), ('hash', hash), ('id', id), ('imhuman', imhuman), ('op', op),
                      ('referer', referer), ('usr_login', usr_login)]

            if status is not None:
                try:
                    status({"id": "PV_STRCLOUD", "visible": True, "timeout": 12, "text": "Stream wird vorbereiten... Bitte warten... [:COUNTDOWN:]"})
                except Exception as e:
                    print(e)

            time.sleep(12)

            strpage, r = requester.request(url, mydata)
            mo8 = re.search(pat8, strpage)

            if (mo8 is None or "btn_download" in strpage) and retry:
                if status is not None:
                    try:
                        status({"id": "PV_STRCLOUD", "visible": True, "timeout": 12, "text": "Fehlschlag! Neuer versuch..."})
                    except Exception as e:
                        print(e)

                time.sleep(1)
                return ProxerVideo.get_streamcloud_video(url, requester, status, False)

            try:
                fileLink = mo8.group(1)
            except AttributeError:
                if status is not None:
                    try:
                        status({"id": "PV_STRCLOUD", "visible": True, "timeout": 12, "text": "Fehlschlag! Neuer versuch..."})
                    except Exception as e:
                        print(e)

                time.sleep(1)
                return ProxerVideo.get_streamcloud_video(url, requester, status, False)

            if status is not None:
                try:
                    status({"id": "PV_STRCLOUD", "visible": True, "timeout": 2, "text": "Stream vorbereitet"})
                except Exception as e:
                    print(e)
            return fileLink
        return None

    @staticmethod
    def get_mp4upload_video(url, requester: lib.ProxerRequest.ProxerRequest, status=None, retry=True):
        page, r = requester.request(url)
        rawJson = re.search("setup\({.*?file.*?}\)", page, re.DOTALL)
        if rawJson is None:
            return None
        refindJson = rawJson.group(0).replace("setup(", "").replace(")", "").replace("'", '"').replace("\":\"",
                                                                                                       "\" : \"").replace(
            "\t", "")
        iframe = re.search("\"code\" : \"<IFRAME.*?/IFRAME>\",", refindJson, re.DOTALL)
        if iframe is None:
            return None
        refindJson = refindJson.replace(iframe.group(0), "").replace("type", "\"type\"").replace("config",
                                                                                                 "\"config\"").replace(
            "src", "\"src\"")
        refindJson = refindJson.replace("\"\"", "")
        print(refindJson)
        js = json.loads(refindJson)
        return js["file"]

    @staticmethod
    def get_clipfish_video(url, requestr: lib.ProxerRequest.ProxerRequest, retry=True):
        page, r = requestr.request(url)
        vurl = re.search("var videourl = \".*?\.*?\"", page)
        if vurl is None:
            print("No clipfish url")
            return
        vurl = vurl.group(0).replace("var videourl = ", "").replace("\"", "")
        return vurl

    @staticmethod
    def get_novamov_video(url, requester: lib.ProxerRequest.ProxerRequest):
        page, r = requester.request(url)

        ####################### GET THE FILEKEY ###################
        keya = re.search("flashvars.filekey=.*?;", page)
        if keya is None:
            return
        keya = keya.group(0).replace("flashvars.filekey=", "").replace(";", "")

        keyb = re.search("var #=\".*?\";".replace("#", keya), page)
        if keyb is None:
            print("Site uses simple method! Which may not be the case.")
            keyb = keya
        else:
            keyb = keyb.group(0).replace(keya, "")
        keyb = keyb.replace("var ", "").replace("\"", "").replace(";", "")

        filecode = re.search("flashvars.file=.*?;", page)
        if filecode is None:
            return
        filecode = filecode.group(0).replace("flashvars.file=", "").replace(";", "")

        playerapi = "http://www.videoweed.es/api/player.api.php?cid2=undefined&key=<KEY>&file=<FILE>&pass=undefined&cid=0&numOfErrors=0&user=undefined&cid3=undefined".replace(
            "<KEY>", keyb).replace("<FILE>", filecode)

        api_page, r = requester.request(playerapi)
        from urllib.parse import unquote
        api_page = unquote(api_page)
        import urllib.parse
        parsed = urllib.parse.parse_qs(api_page)

        videourl = parsed.get("url", None)
        if videourl is None:
            return None
        try:
            return videourl[0]
        except IndexError:
            return videourl

    @staticmethod
    def youtube_dl(url, download=False, status=None, ytd_args=None):
        if not ytd_args:
            ytd_args = {}
        if url == "":
            print("Empty")
        if __youtube_dl__:
            import youtube_dl.utils
            try:
                print("Try to get video url via youtube-dl...")
                if callable(status):
                    ytd_args["progress_hooks"] = [status]
                    if download:
                        status({"status": "get_infos"})
                ytd = ydl(params=ytd_args)
                info = ytd.extract_info(url, download=download)
                if info is None:
                    return None
                if download:
                    return "evented"
                return info.get("url", None)

            except youtube_dl.utils.DownloadError as de:
                print(de)
            except Exception as e:
                print(e)
        else:
            print("Youtube-DL not installed! Skipping!")
        return None

    @staticmethod
    def crunchyroll(url, pxreq, status=None):
        if "http" in url or "https" in url:
            pass
        else:
            url = "https:" + url
        if callable(status):
            status("Downloading Crunchyroll config...")
        text, r = pxreq.request(url)
        config = __crunchyroll_config__.search(text)
        if config is None:
            return None
        from urllib import parse
        config = parse.unquote(config.group(0))
        config = json.loads(config)
        crunchyurl = config.get("config_url", None)
        if crunchyurl is None:
            return None
        print("Found crunchyroll config url " + crunchyurl)
        text, r = pxreq.request(crunchyurl)
        text = __crunchyroll_xml_url__.search(parse.unquote(text))
        if text is None:
            return None
        text = text.group(0).replace("<default:blockingClickthroughUrl>", "").replace("</", "")
        print("Found crunchyroll url " + text)
        return ProxerVideo.youtube_dl(text, download=True, status=status, ytd_args={"writesubtitles": True, "allsubtitles": True})
