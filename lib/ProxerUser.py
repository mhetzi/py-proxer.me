# -*- coding: utf-8 -*-
__author__ = 'marcel'

try:
    import json
except:
    import simplejson as json

import re
from http.cookiejar import Cookie

import lib
import lib.ProxerLogin
import lib.ProxerRequest

host = "https://proxer.me"
baseurl = "https://proxer.me/user/"
lasturlpiece = "/anime"
lasturlpieceManga = "/manga"


picbaseurl = "https://cdn.proxer.me/cover/"
picurlpiece = ".jpg"

rawpattern = re.compile("<a class=\"tip\".*\\n*.*td>")
namepattern = re.compile("data-ajax=\"true\">.*</a>")
hrefpattern = re.compile("href=\".*top\"")
standpattern = re.compile("class=\"state\">.*</span>")
typepattern = re.compile("=top>.*</td><td valign=top>-")

headerpattern = re.compile("image:url\('.*'\)")
commentIDpattern = re.compile("/comment.*?#top", re.DOTALL)

__seenTablePattern__ = re.compile(
    "<table id=\"box-table-a\" align=\"center\" width=\"[0-9]*%\"><tr><th colspan=\"[0-9]*\">Geschaut</th></tr><tr .*?</tr></table>",
    re.DOTALL)
__toSeeTablePattern__ = re.compile(
    "<table id=\"box-table-a\" align=\"center\" width=\"[0-9]*%\"><tr><th colspan=\"[0-9]*\">Wird noch geschaut</th></tr><tr .*?</tr></table>",
    re.DOTALL)  # re.compile("<table id=\"box-table-a\".*?Wird noch geschaut.*?</tr></table>", re.DOTALL)
__cancledTablePattern__ = re.compile(
    "<table id=\"box-table-a\" align=\"center\" width=\"[0-9]*%\"><tr><th colspan=\"[0-9]*\">Abgebrochen</th></tr><tr .*?</tr></table>",
    re.DOTALL)  # re.compile("<table id=\"box-table-a\".*?Abgebrochen.*?</tr></table>", re.DOTALL)
__seeingTablePattern__ = re.compile(
    "<table id=\"box-table-a\" align=\"center\" width=\"[0-9]*%\"><tr><th colspan=\"[0-9]*\">Am Schauen</th></tr><tr .*?</tr></table>",
    re.DOTALL)  # re.compile("<table id=\"box-table-a\".*?Am Schauen.*?</tr></table>", re.DOTALL)

__seenMangaTablePattern__ = re.compile(
    "<table id=\"box-table-a\" align=\"center\" width=\"[0-9]*%\"><tr><th colspan=\"[0-9]*\">Gelesen</th></tr><tr .*?</tr></table>",
    re.DOTALL)
__toSeeMangaTablePattern__ = re.compile(
    "<table id=\"box-table-a\" align=\"center\" width=\"[0-9]*%\"><tr><th colspan=\"[0-9]*\">Wird noch gelesen</th></tr><tr .*?</tr></table>",
    re.DOTALL)  # re.compile("<table id=\"box-table-a\".*?Wird noch geschaut.*?</tr></table>", re.DOTALL)
__cancledMangaTablePattern__ = re.compile(
    "<table id=\"box-table-a\" align=\"center\" width=\"[0-9]*%\"><tr><th colspan=\"[0-9]*\">Abgebrochen</th></tr><tr .*?</tr></table>",
    re.DOTALL)  # re.compile("<table id=\"box-table-a\".*?Abgebrochen.*?</tr></table>", re.DOTALL)
__seeingMangaTablePattern__ = re.compile(
    "<table id=\"box-table-a\" align=\"center\" width=\"[0-9]*%\"><tr><th colspan=\"[0-9]*\">Am Lesen</th></tr><tr .*?</tr></table>",
    re.DOTALL)  # re.compile("<table id=\"box-table-a\".*?Am Schauen.*?</tr></table>", re.DOTALL)

__ProxerNotificationURL__ = "https://proxer.me/notifications?format=raw&s=count"
__NewsResetURL__ = "https://proxer.me/news?format=raw&s=notification"
__userCpAnime__ = "https://proxer.me/ucp?s=anime"
__userCpManga__ = "https://proxer.me/ucp?s=manga"

__userCpDeleteCommentBaseURL__ = "https://proxer.me/comment?format=json&json=delete&id="

class Notifications:
    def __init__(self, pxReq):
        self.__pxReq = pxReq

    def resetNews(self):
        data = self.__pxReq.request(__NewsResetURL__)
        print("Implement Reset of news")
    pn = 0
    freundschaftsAnfragen = 0
    news = 0
    andere = 0
    err = False

class SerienItem:
    name = None
    epGesehen = 0
    epInsgesammt = 0
    CoverUrl = ""
    mainUrl = ""
    type = ""
    is_anime = True

    def __init__(self, name, seen, ovEps, courl, murl, type, commentID, is_anime=True):
        """
        :type name: basestring
        :type seen: int
        :type ovEps int
        :type courl basestring
        :type murl basestring
        :type type: basestring
        """
        self.name = name
        self.epGesehen = seen
        self.epInsgesammt = ovEps
        self.CoverUrl = courl
        self.mainUrl = murl
        self.type = type
        self.commentID = commentID
        self.is_anime = is_anime

class User:
    pxreq = None
    retry = 0
    headerurl = None

    def __init__(self, pxreq):
        """
        :type self: User
        :type pxreq: ProxerRequest
        :rtype User
        """
        self.pxreq = pxreq

    def getAnmimeList(self):
        return self.getList(__userCpAnime__)

    def getMangaList(self):
        return self.getList(__userCpManga__)

    def __processRawItems(self, rawList: list):
        itemlist = []
        for bigitem in rawList:

            for rawitem in rawpattern.findall(bigitem):
                name = namepattern.findall(rawitem)
                name = name[0].replace("data-ajax=\"true\">", "").replace("</a>", "")

                href = hrefpattern.findall(rawitem)
                href = host + href[0].replace("href=\"", "").replace("#top\"", "")

                animetype = typepattern.findall(rawitem)
                animetype = animetype[0].replace("=top>", "").replace("</td><td valign=top>-", "").replace(
                    '</td><td valign-', "")

                is_manga = False
                if "Mangaserie" in animetype or "H-Manga" in animetype or "One-Shot" in animetype or "Doujinshi" in animetype:
                    is_manga = True

                commentID = commentIDpattern.findall(rawitem)
                commentID = commentID[0].replace("/comment?id=", "").replace("#top", "")

                stand = standpattern.findall(rawitem)
                try:
                    stand = stand[0].replace("class=\"state\">", "").replace("</span>", "").replace(" ", "").split("/")
                except IndexError:
                    self.retry += 1
                    if self.retry < 4:
                        return self.getList(None)
                    else:
                        print("Downloaded Incomplete List!")
                        stand = {"0", "0"}
                seen = ""
                tosee = ""
                try:
                    seen = int(stand[0])
                    tosee = int(stand[1])
                except TypeError:
                    seen = "0"
                    tosee = "0"
                itemlist.append(
                    SerienItem(name, seen, tosee,
                               picbaseurl + href.replace("/info/", "").replace(host, "") + picurlpiece,
                               href, animetype, commentID, not is_manga))
        return itemlist

    def getList(self, completeUrl, retry=True):

        strpage, r = self.pxreq.request(completeUrl, None)

        if "Du bist nicht eingeloggt. Bitte logge dich ein" in strpage:
            try:
                lib.global_no_login_manager.checkException()
            except lib.NoLoginException as e:
                if e.retry and retry:
                    return self.getList(completeUrl, False)
                else:
                    raise e

        raw = rawpattern.findall(strpage)

        toSeeRaw = __toSeeTablePattern__.findall(strpage)
        if not toSeeRaw:
            toSeeRaw = __toSeeMangaTablePattern__.findall(strpage)
        toSeeList = self.__processRawItems(toSeeRaw)

        seenRaw = __seenTablePattern__.findall(strpage)
        if not seenRaw:
            seenRaw = __seenMangaTablePattern__.findall(strpage)
        seenList = self.__processRawItems(seenRaw)

        seeingRaw = __seeingTablePattern__.findall(strpage)
        if not seeingRaw:
            seeingRaw = __seeingMangaTablePattern__.findall(strpage)
        seeingList = self.__processRawItems(seeingRaw)

        cancledRaw = __cancledTablePattern__.findall(strpage)
        if not cancledRaw:
            cancledRaw = __cancledMangaTablePattern__.findall(strpage)
        cancledList = self.__processRawItems(cancledRaw)

        items = {"toSee": toSeeList, "seen": seenList, "seeing": seeingList, "cancled": cancledList}

        try:
            self.headerurl = "http:" + headerpattern.search(strpage).group(0).replace("image:url(", "").replace("'", "")
            self.headerurl = self.headerurl.replace(")", "")
        except:
            print("Header image not found!")
        return items

    def removeFromControlPanel(self, commentID):
        data, r = self.pxreq.request(__userCpDeleteCommentBaseURL__ + commentID)
        try:
            pdata = json.loads(data)
        except ValueError as e:
            print("Error Parsing json String: " + data)
            print(e)
            return False
        except AttributeError as e:
            print("Error parsing Json! No json Response!")
            print(e)
            print("Page data:\n" + data)
            return False
        if pdata.get("error"):
            return False
        else:
            return True

    def getHeaderUrl(self=None):
        pxreq = None
        if self is None:
            pxreq = lib.ProxerRequest.ProxerRequest()
            print("getHeaderUrl static invoked!")
        else:
            pxreq = self.pxreq

        if self is None or self.headerurl is None:
            page, r = pxreq.request(host, None)
            strpage = page

            try:
                headerurl = "http:" + headerpattern.search(strpage).group(0).replace("image:url(", "").replace("'",
                                                                                                                    "")
                headerurl = headerurl.replace(")", "")

                if self is not None:
                    self.headerurl = headerurl

                return headerurl
            except:
                print("Header image not found!")
        return self.headerurl

    def setAdultMode(self, over18: bool):
        # Cookie(version, name, value, port, port_specified, domain,
        # domain_specified, domain_initial_dot, path, path_specified,
        # secure, discard, comment, comment_url, rest)
        # c = Cookie(None, 'asdf', None, '80', '80', 'www.foo.bar', None, None, '/', None, False, False, 'TestCookie', None, None, None)
        val = "0"

        if over18:
            val = "1"

        c = Cookie(0, "adult", val, "80", "80", "proxer.me", None, None, "/", None, True, None, False, "adultCookie",
                   None, None, None)
        # c1 = Cookie(0, "adult", val, "80", "80", ".proxer.me", None, None, "/", None, True, None, False, "adultCookie",
        #            None, None, None)
        self.pxreq.setCookie(c)

    def getNotifications(self):
        data, r = self.pxreq.request(__ProxerNotificationURL__)
        nots = data.split("#")
        notifications = Notifications(self.pxreq)

        if data == "1":
            notifications.err = True
            if not lib.ProxerLogin.Login(self.pxreq).checkLogin().loggedIn:
                lib.global_no_login_manager.checkException()
        else:
            try:
                notifications.pn = int(nots[2])
                notifications.freundschaftsAnfragen = int(nots[3])
                notifications.news = int(nots[4])
                notifications.andere = int(nots[5])
            except IndexError as e:
                print(e)
                notifications.err = True
            except ValueError as e:
                print(e)
                notifications.err = True
        return notifications

    def addToUserList(self):
        print("Not implemented!")

    def get_chronic(self, serie=None):
        """
        :param serie: Retrive Chronic for the serie, None for all chronics
        :return: list
        """
