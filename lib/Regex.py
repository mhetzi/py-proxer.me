import re

########################### ProxerSerie Regex Strings ################################

__host__ = "https://proxer.me/info/"
__hostDetail__ = "/details"
__hostList__ = "/list"
__hostInfo__ = "/info"

__epsodePageListPattern__ = re.compile("<p align=\"center\">.*\n*.*</p>\n<table")
__epsFinerPattern__ = re.compile("list/[0-9]*#top")
__epGroupPattern__ = re.compile("<tr>.*?</tr>", re.DOTALL)

__epSubHrefPattern__ = re.compile("> <a href=\".*\"><img")
__epSubTypePattern__ = re.compile("\w*#top")
__epHrefFinePatter__ = re.compile("\".*\"")
__epHosterListPattern__ = re.compile("\[{.*?}\]", re.DOTALL)
__epHosterDescPattern__ = re.compile("{.*?}")

__seOriginalTitel__ = re.compile("<td>.*Original Titel.*?</td>\n.*<")
__seEngTitel__ = re.compile("<td>.*Eng\. Titel.*?</td>\n.*<")
__seGerTitel__ = re.compile("<td>.*Ger\. Titel.*?</td>\n.*<")
__seJapTitel__ = re.compile("<td>.*Jap\. Titel.*?</td>\n.*<")

__seGenre__ = re.compile("<b>Genre.*?</a> </td>", re.DOTALL)
__seGenreFiner__ = re.compile("\">.*?<")

__seRatingFinder__ = re.compile("<span class=\"rating\">.*?</td>", re.DOTALL)
__seRatingValue__ = re.compile("average\">.*?<")
__seRatingVotes__ = re.compile("votes\">.*?<")

__seFSK__ = re.compile("<b>FSK</b>.*?</span> </td>", re.DOTALL)
__seFskTitle__ = re.compile("title=\".*?\"", re.DOTALL)
__seFskSrc__ = re.compile("src=\".*?\"", re.DOTALL)

__seSynonym__ = re.compile("<b>Synonym.*?</tr>", re.DOTALL)
__seAirDatesRaw__ = re.compile("<b>Season</b>.*?</td></tr>", re.DOTALL)
__seAirDatesFin__ = re.compile("e\">.*?</a", re.DOTALL)
__seAiringStatus__ = re.compile("<td><b>Status</b></td>.*?</td>", re.DOTALL)

__seIndustrieRaw__ = re.compile("<b>Industrie</b>.*?</tr>", re.DOTALL)
__seIndustrieFiner__ = re.compile("<img class.*?<br/>", re.DOTALL)
__seIndustrieFlag__ = re.compile("<img class=\"flag\" src=\".*?\"", re.DOTALL)
__seIndustrieFlagAlt__ = re.compile("alt=\".*?\"", re.DOTALL)
__seIndustrieHref__ = re.compile("href=\".*?\">", re.DOTALL)
__seIndustrieName__ = re.compile("top\">.*?</a>", re.DOTALL)
__seIndustrieArt__ = re.compile("</a>.*?<br/>", re.DOTALL)

seDescription__ = re.compile("<b>Beschreibung:</b><br>.*?</td>", re.DOTALL)
seCover = re.compile("img src=\"//cdn\.proxer\.me/cover/.*?\"")

urlValidationRegex = re.compile(
    r'^(?:http|ftp|rtmpe)s?://'  # http:// or https://
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)

__seTypeRaw__ = re.compile("Detail zu .*?span> \(.*?\)")
__seTypeFin__ = re.compile("span> \(.*?\)")
__seComments__ = re.compile("<table style.*?>.*?>Missbrauch melden!</a>", re.DOTALL)
__seCommentPages__ = re.compile("<a class=\"menu active\" data-ajax=\"true\".*?</a> </td>", re.DOTALL)

__reTable__ = re.compile("<tr align=\"left\"><td>.*?</table><br>", re.DOTALL)

__SearchgenreTable__ = re.compile("class=\"genre.*?</table>", re.DOTALL)
__SearchgenreTableItem__ = re.compile("value=.*?</span>", re.DOTALL)
__SearchgenreTitle__ = re.compile("title=\".*?\"", re.DOTALL)
__SearchGenreValue__ = re.compile("value=\".*?\"", re.DOTALL)

__SucheErgebnisFinder__ = re.compile("</tr><tr align=\"left\" class=.*?</td></tr></table><br>?", re.DOTALL)
__SucheItemFinder__ = re.compile("<td><a data-ajax=\"true\".*?</td><td>.*?</td>.*?<td>.*?</td>.*?</td></tr>", re.DOTALL)
__SucheHrefFinder__ = re.compile("href=\".*?\"", re.DOTALL)
__SucheNameFinder__ = re.compile("\">.*?</a", re.DOTALL)
__SucheTdFinder__ = re.compile("<td>.*?</td>", re.DOTALL)

########### Proxer Constants ###########

__proxerBaseUrl__ = "https://proxer.me"