# -*- coding: utf-8 -*-
from datetime import date, timedelta
from time import sleep, time
from lib import Regex
import re

__author__ = 'marcel'

import lib.ProxerUser
import urllib.error
import lib
import lib.ProxerSearch

try:
    import json
except:
    import simplejson as json

__finish_url__ = "?format=json&json=finish"
__favor_url__ = "?format=json&json=favor"
__note_url__ = "?format=json&json=note"


class EpisodeNotAvaliableException(Exception):
    def __init__(self, episode):
        self.__episode = episode

    def add_readmark(self):
        self.__episode.previous_episode.set_viewed()

    def get_previous_episode(self):
        return self.__episode.previous_episode


class Komentare:
    ORDER_NEUSTE = "latest"
    ORDER_BELIEBTESTE = "rating"

    def __init__(self, animeURL, pxReq):
        self.aniUrl = animeURL.replace(Regex.__hostDetail__, "/").replace(Regex.__hostList__, "/").replace("#top",
                                                                                                           "") + "comments/"
        self.__currOrder = self.ORDER_NEUSTE
        self.__currentPage = 1
        self.__request = pxReq

    def set_order(self, order):
        if order == self.ORDER_NEUSTE or order == self.ORDER_BELIEBTESTE:
            self.__currOrder = order
        else:
            raise AttributeError("Unknown order type")

    def __build_url(self, page_num: int):
        # /info/53/comments/?sort=rating#top
        if page_num < 1:
            page_num = 1
        return self.aniUrl + str(page_num) + "?sort=" + self.__currOrder + "#top"

    def get_comment_page(self, page_num: int):
        url = self.__build_url(page_num)
        page, r = self.__request.request(url)

        commentEntrys = Regex.__seComments__.findall(page)
        import re
        for ent in commentEntrys:
            print("\n" + ent + "\n")
            user = Regex.__proxerBaseUrl__ + re.search("href=\"/user/.*?\"", ent).group(0).replace("\"", "").replace(
                    "href=", "")
            userimg = "http:" + re.search("src=\"//.*?\"", ent).group(0).replace("src=", "").replace("\"", "")
            username = re.search("\n<br/><br/>\n.*?</a> </td>", ent, re.DOTALL).group(0).replace("<br/>", "").replace(
                    "</a> </td>", "").replace("\n", "")

            #### Counting stars
            genre = re.search("<td>Genre</td><td>.*?</td>", ent, re.DOTALL).group(0)
            genre = re.findall("/stern\.png", genre)
            genre = len(genre)

            Story = re.search("<td>Story</td><td>.*?</td>", ent, re.DOTALL).group(0)
            Story = re.findall("/stern\.png", Story)
            Story = len(Story)

            animation = re.search("<td>Animation/Bilder</td><td>.*?</td>", ent, re.DOTALL).group(0)
            animation = re.findall("/stern\.png", animation)
            animation = len(animation)

            Charaktere = re.search("<td>Charaktere</td><td>.*?</td>", ent, re.DOTALL).group(0)
            Charaktere = re.findall("/stern\.png", Charaktere)
            Charaktere = len(Charaktere)

            Musik = re.search("<td>Musik</td><td>.*?</td>", ent, re.DOTALL).group(0)
            Musik = re.findall("/stern\.png", Musik)
            Musik = len(Musik)

            kommentar = re.search("</table>.*?</td>", ent, re.DOTALL).group(0)

            pass

        pass


class Episode:
    pxreq = None
    __subType = {}
    __hosters = {}
    __uploader = None
    __Group = None
    __seen = False

    def __init__(self, subType, pxreq, gesehen, previous_Episode, next_Episode):
        """

        :type subType: dict
        """
        self.__subType = subType
        self.pxreq = pxreq
        self.previous_episode = previous_Episode
        self.next_episode = next_Episode

    def getHostersAllTypes(self):
        if self.__hosters is None:
            self.loadAllHosters()
        return self.__hosters

    def getHostersByType(self, subType):
        if subType is None or subType == '':
            if self.__subType.get("gersub") is not None:
                subType = "engsub"
            elif self.__subType.get("engsub") is not None:
                subType = "engsub"
            elif self.__subType.get("engdub") is not None:
                subType = "engdub"

        try:
            return self.__hosters[subType]
        except:
            self.loadHosters(subType)

        try:
            return self.__hosters[subType]
        except:
            return None

    def loadAllHosters(self):
        tb = False
        for ent in self.__subType.keys():
            if self.loadHosters(ent):
                tb = True
        return tb

    def loadHosters(self, subType):

        url = self.__subType[subType]
        print("Loading Hosterinfos from " + url)
        page, r = self.pxreq.request(url, {})
        strpage = page
        hlist = Regex.__epHosterListPattern__.search(strpage)
        if hlist is None:
            return False
        g = hlist.group(0)
        dlist = Regex.__epHosterDescPattern__.findall(g)

        hosterList = []

        for st in dlist:
            dicti = {}
            l = st.split(",")
            for ll in l:
                doppelp = ll.replace("\"", "").replace("{", "").replace("}", "").replace("\\", "").split(":")
                if len(doppelp) > 1:
                    if "http" in doppelp[1]:
                        doppelp[1] = doppelp[1] + ":" + doppelp[2]
                    dicti[doppelp[0]] = doppelp[1]
            hosterList.append(dicti)

            self.__hosters[subType] = hosterList
        return True

    def set_viewed(self, retry=True):
        st = None
        for k in self.__subType.keys():
            if st is None:
                st = k
        if st is None:
            return False
        url = self.__subType[st].replace("#top", "") + "?format=json&type=reminder&title=reminder_next"
        resp, r = self.pxreq.request(url)
        js = json.loads(resp)
        print(resp)
        try:
            if "Watchlist" in js["title"] or "Readlist" in js["title"]:
                if "aktualisiert" in js["msg"]:
                    self.__seen = True
                    doIt = True
                    pe = self.previous_episode
                    while doIt:
                        if pe is None:
                            doIt = False
                        else:
                            pe.__seen = True
                            pe = pe.previous_episode
                    return True
                elif "Du bist nicht eingeloggt" in js["msg"]:
                    lib.global_no_login_manager.checkException()
        except lib.NoLoginException as log:
            if log.retry and retry:
                return self.set_viewed(False)
        except Exception as e:
            return False
        return False

    def is_viewed(self):
        return self.__seen

    def get_subtypes(self):
        return self.__subType


class Serie:
    _pxreq = None

    _MainURL = ""

    OrigName = None  # Done
    EngName = None  # Done
    GerName = None  # Done
    JapName = None  # Done
    Synonym = []  # Done
    Genre = []  # Done
    FSK = []  # Done
    AiringTime = ""  # Done
    AiringStatus = ""  # Done
    Industrie = []  # Done
    Beschreibung = ""  # Done
    Rating = 0.0  # Done
    Votes = 0  # Done
    Episodes = None  # Done
    Gesehen = 0  # in loadSerienItem()
    EpisodesInsgesammt = 0  # in loadSerienItem()
    coverUrl = ""  # in loadSerienItem() und loadDetails()
    Type = ""  # in loadSerienItem()
    KommentarID = ""  # in loadSerienItem()
    is_anime = True
    __kommentare = None

    def __init__(self, pxreq):
        """
        :rtype : Serie
        """
        self._pxreq = pxreq
        self.Episodes = None

    def loadSearchResult(self, result: dict):
        # {"name": name, "url": __proxerBaseUrl__ + href, "Genres": genres, "Typ": Typ, "Bewertung": Bewertung, "Release": release}
        self.EngName = result["name"]
        self._MainURL = result["url"].replace("#top", "")
        self.Type = result["Typ"]
        self.AiringTime = result.get("Release", "")

    def loadSerienItem(self, seitem: lib.ProxerUser.SerienItem):
        '''
        :param seitem: lib.ProxerUser.SerienItem
        :return: None
        '''
        self.coverUrl = seitem.CoverUrl
        self.Gesehen = seitem.epGesehen
        self.EpisodesInsgesammt = seitem.epInsgesammt
        self._MainURL = seitem.mainUrl
        self.EngName = seitem.name
        self.Type = seitem.type
        self.KommentarID = seitem.commentID
        self.is_anime = seitem.is_anime

    def setSerienUrl(self, url):
        """
        :param url: basestring
        """
        self._MainURL = url

    def loadDetails(self, retry=True):
        try:
            strpage, r = self._pxreq.request(self._MainURL)
        except lib.NoLoginException as e:
            if e.retry and retry:
                self.loadDetails(False)
            return

        try:
            self.OrigName = Regex.__seOriginalTitel__.search(strpage).group(0).replace("<td><b>Original Titel</b></td>",
                                                                                       "").replace("\n", "").replace(
                    "<td>",
                    "").replace(
                    "<", "")
        except:
            print("No original name!")

        try:
            self.EngName = Regex.__seEngTitel__.search(strpage).group(0).replace("<td><b>Eng. Titel</b></td>",
                                                                                 "").replace(
                    "\n", "").replace("<td>", "").replace("<", "")
        except:
            print("No eng name!")

        try:
            self.GerName = Regex.__seGerTitel__.search(strpage).group(0).replace("<td><b>Ger. Titel</b></td>",
                                                                                 "").replace(
                    "\n", "").replace("<td>", "").replace("<", "")
        except:
            print("No ger name!")

        try:
            self.JapName = Regex.__seJapTitel__.search(strpage).group(0).replace("<td><b>Jap. Titel</b></td>",
                                                                                 "").replace(
                    "\n", "").replace("<td>", "").replace("<", "")
        except:
            print("No jap name!")

        try:
            genre = Regex.__seGenre__.search(strpage).group(0)
            genre = Regex.__seGenreFiner__.findall(genre)
            self.Genre = []
            for gen in genre:
                from urllib import parse
                self.Genre.append(parse.unquote(gen.replace("<", "").replace(">", "").replace("\"", "")))

        except AttributeError:
            print("No Genre found")

        try:
            bewFuzzy = Regex.__seRatingFinder__.search(strpage).group(0)
            self.Rating = float(
                    Regex.__seRatingValue__.search(bewFuzzy).group(0).replace("average\">", "").replace("<", ""))
            self.Votes = float(
                    Regex.__seRatingVotes__.search(bewFuzzy).group(0).replace("votes\">", "").replace("<", ""))
        except:
            print("Informationen vom Rating nicht gefunden!")
        self.FSK = {}

        try:
            fsk = Regex.__seFSK__.search(strpage).group(0)
            fskTl = Regex.__seFskTitle__.findall(fsk)
            fskSl = Regex.__seFskSrc__.findall(fsk)
            from urllib.parse import unquote
            for i in range(0, len(fskTl)):
                self.FSK[unquote(fskTl[i].replace("title=", "").replace("\"", ""))] = "https://proxer.me" + fskSl[
                    i].replace(
                        "src=", "").replace("\"", "")
        except Exception as e:
            print("Informationen über FSK nicht gefunden!")

        self.Synonym = []

        try:
            rawList = Regex.__seSynonym__.findall(strpage)
            for ri in rawList:
                self.Synonym.append(
                        ri.replace("<td>", "").replace("</td>", "").replace("<b>Synonym</b>", "").replace("</tr>",
                                                                                                          "").replace(
                                "\n",
                                ""))
        except:
            print("Informationen über die Synonyme nicht gefunden!")

        try:
            rawDates = Regex.__seAirDatesRaw__.search(strpage).group(0)
            finDates = Regex.__seAirDatesFin__.findall(rawDates)

            self.AiringTime = finDates[0].replace("e\">", "").replace("</a", "") + " - " + finDates[1].replace("e\">",
                                                                                                               "").replace(
                    "</a", "")
            from urllib.parse import unquote
            self.AiringTime = unquote(self.AiringTime)
        except:
            print("Airdates nicht gefunden!")

        try:
            self.AiringStatus = Regex.__seAiringStatus__.search(strpage).group(0).replace("<td><b>Status</b></td>",
                                                                                          "").replace("<td>",
                                                                                                      "").replace(
                    "</td>", "").replace("\n", "")
        except Exception as e:
            print("Airing status nicht gefunden!")

        ######### Parse Industrie Daten #########
        self.Industrie = []
        try:
            rawIndust = Regex.__seIndustrieRaw__.search(strpage).group(0)
            listIndust = Regex.__seIndustrieFiner__.findall(rawIndust)

            for finItem in listIndust:
                name = Regex.__seIndustrieName__.search(finItem).group(0).replace("top\">", "").replace("</a>", "")
                art = Regex.__seIndustrieArt__.search(finItem).group(0).replace("</a> ", "").replace("<br/>", "")
                flagHREF = Regex.__seIndustrieFlag__.search(finItem).group(0).replace("<img class=\"flag\" src=",
                                                                                      "").replace("\"", "")
                flagName = Regex.__seIndustrieFlagAlt__.search(finItem).group(0).replace("alt=", "").replace("\"", "")
                link = Regex.__seIndustrieHref__.search(finItem).group(0).replace("href=", "").replace("\">",
                                                                                                       "").replace("\"",
                                                                                                                   "")

                self.Industrie.append(
                        {"Name": name, "Art": art, "FlaggeURL": "https://proxer.me" + flagHREF, "Countrycode": flagName,
                         "Link": "https://proxer.me" + link})
        except Exception as e:
            print("Industriedaten nicht gefunden!")

        try:
            desc = Regex.seDescription__.search(strpage).group(0)
            self.Beschreibung = desc.replace("<b>Beschreibung:</b><br>", "").replace("</td>", "").replace("<br/>", "")
        except:
            print("Beschreibung nicht gefunden!")

        antype = Regex.__seTypeRaw__.search(strpage)
        if antype is not None:
            antype = Regex.__seTypeFin__.search(antype.group(0))
            if antype is not None:
                antype = antype.group(0).replace("span> (", "").replace(")", "")
                self.Type = antype
                if "Mangaserie" in antype or "H-Manga" in antype or "One-Shot" in antype or "Doujinshi" in antype:
                    self.is_anime = False

        cover = Regex.seCover.search(strpage)
        if cover is not None:
            self.coverUrl = "http:" + cover.group(0).replace("img src=", "").replace("\"", "")

    def loadEpisodeList(self, error=0, efficient_loading=True, alternative_crawler=False, retry=True):
        url = self._MainURL.replace(Regex.__hostDetail__, Regex.__hostInfo__).replace("#top", "") + Regex.__hostList__
        self.Episodes = []

        try:
            strpage, r = self._pxreq.request(url, None)
        except urllib.error.HTTPError:
            sleep(2)
            error += 1
            if error > 4:
                raise
            self.loadEpisodeList(error)
            return None
        try:
            pagesfiner = None
            try:
                pagesraws = Regex.__epsodePageListPattern__.search(strpage)

                pagesraw = pagesraws.group(0)
                pagesfiner = Regex.__epsFinerPattern__.findall(pagesraw)
            except lib.NoLoginException as e:
                if e.retry and retry:
                    return self.loadEpisodeList(error, alternative_crawler, False)
            if alternative_crawler:
                doIt = True
                pagee = 0
                while doIt:
                    pagee += 1
                    doIt = self.__loadEpisodePageList(
                            url.replace("#top", "").replace(Regex.__hostList__, "") + "/list/" + str(pagee))
            elif efficient_loading:
                last_page = pagesfiner[len(pagesfiner) - 1]
                eplist = self.__loadEpisodePageList(
                        url.replace("#top", "").replace(Regex.__hostList__, "") + "/" + last_page, download_only=True)
                last_ep = eplist[len(eplist) - 1]
                import re
                hrefs = re.findall("href=\"/watch/.*?\">", last_ep)
                if not hrefs:
                    hrefs = re.findall("href=\"/chapter/.*?\">", last_ep)
                    if hrefs:
                        self.is_anime = False
                max_episodes = 0
                subtypes = []
                serien_id = ""
                for href in hrefs:  # href="/watch/53/750/gersub#top">
                    raw = href.replace("href=\"/watch/", "").replace("href=\"/chapter/", "").replace("\">", "").split(
                            "/")  # raw = [35, 750, gersub#top]
                    serien_id = raw[0]
                    max_episodes = int(raw[1])
                    subtypes.append(raw[2])

                prev_ep = None
                current_episode_num = 1
                while (current_episode_num < (max_episodes + 1)):
                    tsubt = {}
                    for sub in subtypes:
                        tsubt[sub.replace("#top", "")] = ("https://proxer.me/watch/" if self.is_anime else "https://proxer.me/chapter/") \
                                                         + serien_id + "/" + str(current_episode_num) + "/" + sub
                    ep_gesehen = False
                    if current_episode_num < (self.Gesehen + 1):
                        ep_gesehen = True
                    c_ep = Episode(tsubt, self._pxreq, ep_gesehen, prev_ep, None)
                    if prev_ep is not None:
                        prev_ep.next_episode = c_ep
                    prev_ep = c_ep
                    self.Episodes.append(c_ep)
                    current_episode_num += 1
            else:
                seen = self.Gesehen
                for pagee in pagesfiner:
                    success, loaded = self.__loadEpisodePageList(
                            url.replace("#top", "").replace(Regex.__hostList__, "") + "/" + pagee, gesehen=seen)
                    seen -= loaded

            try:
                if (len(self.Episodes) + 1) < self.EpisodesInsgesammt:
                    print("Didn´t get the right amount of Episodes retrying...")
                    error += 1
                    if error > 4:
                        print("Getting Episodes was impossible for me :(")
                        return False
                    self.loadEpisodeList(error=error, alternative_crawler=True)
            except UnboundLocalError:
                pass
        except AttributeError as e:
            success, lenn = self.__loadEpisodePageList(url, gesehen=self.Gesehen)
            if not success:
                print("No episodes found!")
                if retry:
                    self.loadEpisodeList(retry=False)

    def __loadEpisodePageList(self, url, error=0, gesehen=0, download_only=False):
        print("Downloading EpsiodeList from: " + url)
        try:
            strpage, r = self._pxreq.request(url, None)
        except urllib.error.HTTPError:
            sleep(2)
            error += 1
            if error > 4:
                return False
            return self.__loadEpisodePageList(url, error)

        epGroups = Regex.__epGroupPattern__.findall(strpage)
        if epGroups is None:
            return False

        epGroupsRefind = []

        for group in epGroups:
            if "listState" in group:
                epGroupsRefind.append(group)
        eps = []
        last_ep = None
        for group in epGroupsRefind:
            subType = Regex.__epSubHrefPattern__.findall(group)
            types = {}
            for typee in subType:
                stype = Regex.__epSubTypePattern__.search(typee).group(0).replace("#top", "")
                href = Regex.__epHrefFinePatter__.search(typee).group(0).replace("\"", "")
                types[stype] = Regex.__host__.replace(Regex.__hostInfo__ + "/", "") + href
            if download_only:
                return epGroupsRefind
            now_ep = Episode(types, self._pxreq, False, last_ep, None)
            if last_ep is not None:
                last_ep.next_episode = now_ep
            last_ep = now_ep
            eps.append(now_ep)
        self.Episodes.extend(eps)
        return True, len(eps)

    def getName(self):

        if self.OrigName is not None:
            return self.OrigName
        elif self.EngName is not None:
            return self.EngName
        elif self.GerName is not None:
            return self.GerName
        elif self.JapName is not None:
            return self.JapName

    def getGenresAsString(self, seperator: str):
        strr = ""
        leng = len(self.Genre)

        for i in self.Genre:
            if i is self.Genre[leng - 1]:
                strr = strr + i + ""
            else:
                strr = strr + i + seperator
        return strr

    def getSynonymesAsString(self, seperator: str):
        strr = ""
        leng = len(self.Synonym)

        for i in self.Synonym:
            if i is self.Synonym[leng - 1]:
                strr = strr + i + ""
            else:
                strr = strr + i + seperator
        return strr

    def get_episode_num(self, Episode):
        for num in range(0, len(self.Episodes)):
            if self.Episodes[num] is Episode:
                return num
        return None

    def set_finished(self, retry=True):
        url = self._MainURL.replace("#top", "") + __finish_url__
        try:
            resp, r = self._pxreq.request(url, post={"checkPost": "1"}, headers={'Referer': self._MainURL})
        except lib.NoLoginException as e:
            if e.retry and retry:
                return self.set_finished(False)
            return False
        try:
            js = json.loads(resp)
            print(resp)
            if "Eintrag wurde aktualisiert" in js["title"]:
                return True
            elif "Du bist nicht eingeloggt" in js["title"]:
                lib.global_no_login_manager.checkException()
        except lib.NoLoginException as log:
            if log.retry and retry:
                return self.set_finished(False)
        except Exception as e:
            return False
        return False

    def set_favorite(self, retry=True):
        url = self._MainURL.replace("#top", "") + __favor_url__
        try:
            resp, r = self._pxreq.request(url, None)
        except lib.NoLoginException as e:
            if e.retry and retry:
                return self.set_finished(False)
            return False
        try:
            js = json.loads(resp)
            print(resp)
            if "Erfolgreich" in js["title"]:
                return True
            elif "Du bist nicht eingeloggt" in js["title"]:
                lib.global_no_login_manager.checkException()
        except lib.NoLoginException as log:
            if log.retry and retry:
                return self.set_favorite(False)
        except Exception as e:
            return False
        return False

    def do_note(self, retry=True):
        url = self._MainURL + __note_url__
        try:
            resp, r = self._pxreq.request(url, None)
        except lib.NoLoginException as e:
            if e.retry and retry:
                return self.set_finished(False)
            return False
        try:
            js = json.loads(resp)
            print(resp)
            if "Erfolgreich" in js["title"]:
                return True
            elif "Du bist nicht eingeloggt" in js["title"]:
                lib.global_no_login_manager.checkException()
            elif "Eintrag bereits in deiner Liste" in js["title"]:
                return True
        except lib.NoLoginException as log:
            if log.retry and retry:
                return self.do_note(False)
        except Exception as e:
            return False
        return False

    def get_relations(self):
        url = self._MainURL.replace("#top", "") + "/relation"
        page, r = self._pxreq.request(url, None)
        tables = Regex.__reTable__.findall(page)

        items = []
        for table in tables:
            ergebnise = Regex.__SucheItemFinder__.findall(table)

            for ergebnis in ergebnise:
                href = Regex.__SucheHrefFinder__.search(ergebnis).group(0).replace("\"", "").replace("href=", "")
                name = Regex.__SucheNameFinder__.search(ergebnis).group(0).replace("\">", "").replace("</a>",
                                                                                                      "").replace("</a",
                                                                                                                  "")
                groups = Regex.__SucheTdFinder__.findall(ergebnis)

                genres = ""
                Typ = ""
                Bewertung = ""
                Release = ""

                try:
                    genres = groups[1].replace("<td>", "").replace("</td>", "").split(" ")
                    Typ = groups[2].replace("<td>", "").replace("</td>", "")
                    Bewertung = groups[4].replace("<td>", "").replace("</td>", "")
                    Release = groups[5].replace("<td>", "").replace("</td>", "")
                except IndexError:
                    print("IndexError by indexing Serie " + name)
                items.append(
                        {"name"     : name, "url": Regex.__proxerBaseUrl__ + href, "Genres": genres, "Typ": Typ,
                         "Bewertung": Bewertung, "Release": Release})
        return items

    def get_comments(self):
        if self.__kommentare is None:
            self.__kommentare = Komentare(self._MainURL, self._pxreq)
        return self.__kommentare

    def compare_name(self, name):
        if name == self.EngName or name == self.GerName or name == self.JapName or name == self.OrigName:
            return True
        return False

    def get_id(self):
        return self._MainURL.replace(Regex.__host__, "").replace("#top", "")

    def check_new_eps(self):
        url = self._MainURL.replace("#top", "") + "/updates"
        page, r = self._pxreq.request(url, None)
        data = re.search("<tr>.*?</a>.*?</tr></table>", page, re.DOTALL)
        if data is None:
            raise Exception("Proxer Page does not contain update infos!")
        data = data.group(0)
        entrys = re.findall("<td>.*?</td>", data, re.DOTALL)
        new_entrys = []
        tmp = ""
        tmp_int = 0
        for entry in entrys:
            if tmp_int > 4:
                tmp_int = 0
                new_entrys.append(tmp)
                tmp = ""
            tmp = tmp + entry
            tmp_int += 1
        highest_ep_num = 0
        for entry in new_entrys:
            try:
                sp_entry = re.findall("<td>.*?</td>", entry, re.DOTALL)
                if sp_entry is None:
                    continue
                ep_num = sp_entry[1]
                ep_num = ep_num.replace("Episode", "").replace(" ", "").replace("-", "").replace("<td>", "").replace(
                        "</td>", "").replace("Kapitel", "")
                ep_num = int(ep_num)
                if ep_num > highest_ep_num:
                    highest_ep_num = ep_num
            except KeyError:
                print("Keyerror in check_new_eps!\nNo ep number in line!")
        if highest_ep_num > self.Gesehen:
            return (True, highest_ep_num)
        else:
            return (False, -1)


class Cache:
    class CachedSerie(Serie):
        def __init__(self, pxreq, tstamp):
            super().__init__(pxreq)
            self.__cache_timestamp = tstamp
            self.__downloaded = False

        def convert_serie(self, s: Serie):
            self.AiringStatus = s.AiringStatus
            self.AiringTime = s.AiringTime
            self.Beschreibung = s.Beschreibung
            self.coverUrl = s.coverUrl
            self.EngName = s.EngName
            self.EpisodesInsgesammt = s.EpisodesInsgesammt
            self.FSK = s.FSK
            self.Genre = s.Genre
            self.GerName = s.GerName
            self.Gesehen = s.Gesehen
            self.Industrie = s.Industrie
            self.is_anime = s.is_anime
            self.JapName = s.JapName
            self.KommentarID = s.KommentarID
            self.OrigName = s.OrigName
            self.Rating = s.Rating
            self.Synonym = s.Synonym
            self.Type = s.Type
            self.Votes = s.Votes

        def serialize(self):
            td = {"AirStat" : self.AiringStatus, "AirTime": self.AiringTime, "Desc": self.Beschreibung,
                  "curl"    : self.coverUrl, "en": self.EngName, "epc": self.EpisodesInsgesammt, "fsk": self.FSK,
                  "genre"   : self.Genre, "ger": self.GerName, "seen": self.Gesehen, "industrie": self.Industrie,
                  "is_anime": self.is_anime, "jap": self.JapName, "comID": self.KommentarID, "oname": self.OrigName,
                  "rating"  : self.Rating, "synonym": self.Synonym, "type": self.Type, "votes": self.Votes,
                  "ts"      : self.__cache_timestamp, "dl": self.__downloaded}
            return td

        def deserialize(self, dicd: dict):
            self.AiringStatus = dicd.get("AirStat", None)
            self.AiringTime = dicd.get("AirTime", None)
            self.Beschreibung = dicd.get("Desc", None)
            self.coverUrl = dicd.get("curl", None)
            self.EngName = dicd.get("en", None)
            self.EpisodesInsgesammt = dicd.get("epc", None)
            self.FSK = dicd.get("fsk", None)
            self.Genre = dicd.get("genre", None)
            self.GerName = dicd.get("ger", None)
            self.Gesehen = dicd.get("seen", None)
            self.Industrie = dicd.get("industrie", None)
            self.is_anime = dicd.get("is_anime", None)
            self.JapName = dicd.get("jap", None)
            self.KommentarID = dicd.get("comID", None)
            self.OrigName = dicd.get("oname", None)
            self.Rating = dicd.get("rating", None)
            self.Synonym = dicd.get("synonym", None)
            self.Type = dicd.get("type", None)
            self.Votes = dicd.get("votes", None)
            self.__cache_timestamp = dicd.get("ts", None)
            self.__downloaded = dicd.get("dl", None)

        def get_timestamp(self):
            return self.__cache_timestamp

        def loadDetails(self, retry=True):
            if not self.__downloaded:
                print("Serien Details not downloaded! Doing it now...")
                super().loadDetails()
                self.__downloaded = True

    def __init__(self, request):
        self.__request = request
        self.__cache = {}
        self.__serien_cache = {}
        self.__file_path = None
        self.__time_delta = timedelta(days=6)

    def load_from_file(self, path):
        self.__file_path = path
        try:
            with open(path) as data_file:
                self.__cache = json.load(data_file)
        except Exception as e:
            print("Resetting cache!")
            self.__cache = {}

    def use_dict_as_cache(self, dicd: dict):
        self.__cache = dicd.get("ProxerSerie.Cache", None)
        if self.__cache is None:
            dicd["ProxerSerie.Cache"] = {}
        self.__cache = dicd.get("ProxerSerie.Cache", None)
        if self.__cache is None:
            raise Exception("Invalid dict!")

    def get_serie_by_id(self, sid, nodownload=False):
        serie = self.__serien_cache.get(sid, None)
        if serie is None:
            template = self.__cache.get(sid, None)
            if template is not None:
                serie = Cache.CachedSerie(self.__request, time())
                self.__serien_cache[sid] = serie
                url = Regex.__host__ + sid + "#top"
                serie._MainURL = url
                serie.deserialize(template)
        if serie is None:
            serie = Cache.CachedSerie(self.__request, time())
            url = Regex.__host__ + sid + "#top"
            serie._MainURL = url
            if not nodownload:
                serie.loadDetails()
            self.__serien_cache[sid] = serie
        return serie

    def get_serie_by_serienitem(self, item, nodownload=False):
        sid = item.mainUrl.replace(Regex.__host__, "").replace("#top", "")
        serie = self.get_serie_by_id(sid, nodownload=nodownload)
        serie.loadSerienItem(item)
        return serie

    def get_serie_by_url(self, url, nodownload=False):
        return self.get_serie_by_id(url.replace(Regex.__host__, "").replace("#top", ""), nodownload=nodownload)

    def get_serie_by_searchresult(self, result: dict, nodownload=False):
        url = result["url"]
        s = self.get_serie_by_url(url, True)
        s.loadSearchResult(result)
        return s

    def save(self):
        to_del = []

        for key in self.__serien_cache.keys():
            time = date.fromtimestamp(self.__serien_cache[key].get_timestamp())
            if (time + self.__time_delta) < date.today():
                print("Cached Serie too old! Removing...")
                to_del.append(key)
                continue
            self.__cache[key] = self.__serien_cache[key].serialize()

        for d in to_del:
            try:
                del self.__cache[d]
            except KeyError:
                print("Tryd to remove " + str(d) + " from Cache, but its not there! Ignoring...")

        if self.__file_path is not None:
            with open(self.__file_path, 'w') as f:
                json.dump(self.__cache, f, ensure_ascii=False)
        print("Series Cache saved!")
