# -*- coding: utf-8 -*-

__proxerSearchBaseUrl__ = "https://proxer.me/search?s=search&name={0}&sprache=alle&typ={1}&genre={2}&nogenre={3}&fsk=&sort={4}&length=&length-limit=down#search"

__author__ = 'marcel'

import re

import lib.ProxerUser
import lib.ProxerRequest
import lib.Regex as Reg


class Search:
    pxReq = None

    _genres = ""
    _noGenres = ""
    _genreCompleteList = None

    def __init__(self, pxreq: lib.ProxerRequest.ProxerRequest):
        self.pxReq = pxreq

    def searcher(self, url):
        print("Opening " + url)
        strpage, r = self.pxReq.request(url, None)
        self.__listGenres(strpage)

        ergeList = Reg.__SucheErgebnisFinder__.search(strpage).group(0)
        ergebnise = Reg.__SucheItemFinder__.findall(ergeList)

        items = []

        for ergebnis in ergebnise:
            href = Reg.__SucheHrefFinder__.search(ergebnis).group(0).replace("\"", "").replace("href=", "")
            name = Reg.__SucheNameFinder__.search(ergebnis).group(0).replace("\">", "").replace("</a>", "").replace("</a",
                                                                                                                "")
            groups = Reg.__SucheTdFinder__.findall(ergebnis)

            genres = ""
            Typ = ""
            Bewertung = ""

            try:
                genres = groups[1].replace("<td>", "").replace("</td>", "").split(" ")
                Typ = groups[2].replace("<td>", "").replace("</td>", "")
                Bewertung = groups[4].replace("<td>", "").replace("</td>", "")
            except IndexError:
                print("IndexError by indexing Serie " + name)
            items.append(
                {"name": name, "url": Reg.__proxerBaseUrl__ + href, "Genres": genres, "Typ": Typ, "Bewertung": Bewertung})

        return items

    def allAnime(self, name):
        url = "https://proxer.me/search?s=search&name=<NAME>&typ=all-anime#top".replace("<NAME>", name)
        return self.searcher(url)

    def allNoHentai(self, name):
        url = "https://proxer.me/search?s=search&name=<name>&sprache=alle&typ=all&genre=&nogenre=&fsk=&sort=relevance&length=&length-limit=down#search".replace(
            "<NAME>", name)
        return self.searcher(url)

    def allManga(self, name):
        url = "https://proxer.me/search?s=search&name=<name>&sprache=alle&typ=all-manga&genre=&nogenre=&fsk=&sort=relevance&length=&length-limit=down#search".replace(
            "<NAME>", name)
        return self.searcher(url)

    def __listGenres(self, webcontent):
        if self._genreCompleteList is not None:
            return

        table = Reg.__SearchgenreTable__.search(webcontent).group(0)
        items = Reg.__SearchgenreTableItem__.findall(table)

        self._genreCompleteList = {}

        for item in items:
            try:
                title = Reg.__SearchgenreTitle__.search(item).group(0).replace("title=", "")
                value = Reg.__SearchGenreValue__.search(item).group(0).replace("value=", "")
                self._genreCompleteList[value.replace("\"", "")] = title.replace("\"", "")
            except:
                pass


class AdvancedSearch(Search):
    _Type = "all"
    _Sortierung = "relevance"
    _sortierungKompletteListe = ["relevance", "name", "rating", "clicks", "count"]
    _TypeKompletteListe = {"Alle (keine Hentais)": "all", "Alle Animes": "all-anime", "Serien": "animeseries",
                           "Filme": "movie",
                           "OVA": "ova", "Hentai": "hentai", "Alle Mangas": "all-manga", "Mangaserien": "mangaseries",
                           "Oneshots": "oneshot", "Doujin": "doujin", "Hentai-Manga": "hmanga"}

    def __init__(self, pxReq: lib.ProxerRequest.ProxerRequest):
        super(AdvancedSearch, self).__init__(pxReq)

    def suche(self, name):
        url = __proxerSearchBaseUrl__.format(name, self._Type, self._genres, self._noGenres, self._Sortierung)
        return self.searcher(url)

    @property
    def getAllGenres(self):

        if self._genreCompleteList is None:
            self.allAnime("Test")

        return self._genreCompleteList

    @property
    def getAllSortierungen(self):
        if self._sortierungKompletteListe is None:
            self.allAnime("Test")
        return self._sortierungKompletteListe

    def setGenres(self, genres: list):
        self._genres = ""
        liste = self.getAllGenres

        beginning = True

        if genres is None:
            self._genres = None
            return

        for g in genres:
            if g in liste:
                if beginning:
                    self._genres = g
                    beginning = False
                else:
                    self._genres = self._genres + "+" + g
            else:
                raise ValueError(g + " ist kein gültiges Genre!")

    def setExcludeGenres(self, genres: list):
        self._noGenres = ""
        liste = self.getAllGenres

        if genres is None:
            self._noGenres = None
            return

        beginning = True

        for g in genres:
            if g in liste:
                if beginning:
                    self._noGenres = g
                    beginning = False
                else:
                    self._noGenres = self._noGenres + "+" + g
            else:
                raise ValueError(g + " ist kein gültiges Genre!")

    def setSortierung(self, reihenfolge):
        if reihenfolge not in self._sortierungKompletteListe:
            raise ValueError(reihenfolge + " ist keine gültige Reihenfolge!")
        self._Sortierung = reihenfolge

    def getTypes(self):
        return self._TypeKompletteListe.keys()

    def setType(self, Typ):
        if Typ not in self._TypeKompletteListe.keys():
            raise ValueError(Typ + " ist kein gültiger Typ!")
        self._Type = self._TypeKompletteListe[Typ]
