# -*- coding: utf-8 -*-
__author__ = 'marcel'

__Host__ = "https://proxer.me"
__loginurl__ = "/login?format=json&action=login"
__avatarURL__ = "/images/comprofiler/"

try:
    import json
except:
    import simplejson as json

import re

import lib.ProxerRequest


class LoginData:
    UserID = ""
    AvatarURL = ""
    loggedIn = True
    ServerErr = False
    error = False
    ToManyConnections = False

    def __init__(self, uid, avu):
        """

        :rtype : LoginData
        """
        self.UserID = uid
        self.AvatarURL = __Host__ + __avatarURL__ + avu
        if avu == "error":
            self.ServerErr = True
            self.loggedIn = False
            self.error = True
        elif avu == "noLogin":
            self.loggedIn = False
            self.error = True
        elif avu == "tmc":
            self.ToManyConnections = True
            self.ServerErr = True
            self.loggedIn = False
            self.error = True


class Login:
    pxrequester = None

    def __init__(self, pxrequester: lib.ProxerRequest.ProxerRequest):
        """
        :rtype : Login
        :type pxrequester: ProxerRequest
        :type self: Login
        """
        self.pxrequester = pxrequester

    def doLogin(self, username, password):
        post = {"username": username, "password": password}
        data, r = self.pxrequester.request(__Host__ + __loginurl__, post)
        m = re.search("{.*}", data)

        userdata = json.loads(m.group(0))

        if userdata.get("error") == 1:
            return LoginData(-1, "")
        self.pxrequester.save()
        return LoginData(userdata.get("uid"), userdata.get("avatar"))

    def checkLogin(self):
        post = {}
        try:
            data, r = self.pxrequester.request(__Host__ + __loginurl__, post)
        except:
            return LoginData(-1, "error")

        m = re.search("{.*}", data)

        userdata = None
        try:
            userdata = json.loads(m.group(0))
        except ValueError as e:
            print("Error Parsing json String: " + m.group(0))
            print(e)
            return LoginData(-1, "error")
        except AttributeError as e:
            print("Error parsing Json! No json Response!")
            print(e)
            print("Page data:\n" + data)
            return LoginData(-1, "error")
        if userdata.get('error') == 1 and userdata.get('message') == "Missing credentials":
            return LoginData(-1, "noLogin")
        elif userdata.get('error') == 1 and userdata.get('message') == "Too many connections. Try again later.":
            return LoginData(-1, "tmc")
        elif userdata.get('error') == 1:
            return LoginData(-1, "error")

        return LoginData(userdata.get("uid"), userdata.get("avatar"))
