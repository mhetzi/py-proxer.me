__author__ = 'marcel'

import lib.ProxerRequest

try:
    import json
except:
    import simplejson as json
import datetime

__NewsBaseURL__ = "https://proxer.me/notifications?format=json&s=news&p="


class Article:
    def __init__(self, aID, author, category, description, imgURL, threadURL, timestamp, title, views):
        self.ArticleID = aID
        self.Author = author
        self.category = category
        self.Beschreibung = description
        self.imageURL = imgURL
        self.ThreadURL = threadURL
        self.Titel = title
        self.Views = views

        date = datetime.datetime.fromtimestamp(int(timestamp))
        self.Zeit = date.strftime("%H:%M %d. %b. %Y")


class News:
    __Articles = []
    __Page = 0

    def __init__(self, pxRequest: lib.ProxerRequest.ProxerRequest):
        self.__pxRequest = pxRequest

    def loadNextNews(self):
        self.__Page += 1
        return self.loadNews(self.__Page)

    def loadNews(self, page=1):
        self.__Articles = []
        data, r = self.__pxRequest.request(__NewsBaseURL__ + str(page))
        pdata = None
        try:
            pdata = json.loads(data)
        except ValueError as e:
            print("Erro Parsing json String: " + data)
            print(e)
            return None
        except AttributeError as e:
            print("Error parsing Json! No json Response!")
            print(e)
            print("Page data:\n" + data)
            return None

        if pdata.get("error") is not 0:
            return

        # Build articles
        alist = pdata.get("notifications")
        for a in alist:
            aID = a["nid"]
            aut = a["uname"]
            cat = a["catname"]
            desc = a["description"]
            imurl = "http://cdn.proxer.me/news/" + aID + "_" + a["image_id"] + ".png"
            if "Rezensionen" in a["catname"]:
                thrUrl = "https://proxer.me/review?mid=" + a["mid"] + " #top"
            else:
                thrUrl = "https://proxer.me/forum/" + a["catid"] + "/" + a["thread"]

            article = Article(aID, aut, cat, desc, imurl, thrUrl, a["time"], a["subject"], a["hits"])
            self.__Articles.append(article)
        return self.__Articles

    def reset(self):
        self.__Page = 0
        self.__Articles = None
