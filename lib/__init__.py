__author__ = 'marcel'
__package__ = "lib"


class BooleanContainer:
    def __init__(self, initial_value=False):
        self.__value = initial_value
    def get_value(self):
        return self.__value
    def set_value(self, b: bool):
        self.__value = b


class ClassContainer:
    def __init__(self, Klasse):
        self.__value = Klasse
    def get_value(self):
        return self.__value
    def set_value(self, val):
        self.__value = val


def execute_gobject_idle_add_blocking(func, *args, **kwargs):
    def exec_idle(funcc, cc, argss, kwargss):
        try:
            val = funcc(*argss, **kwargss)
            if val is None:
                val = "executed"
            cc.set_value(val)
        except Exception as e:
            cc.set_value(e)
    container = ClassContainer(None)
    from gi.repository import GObject
    GObject.idle_add(exec_idle, func, container, args, kwargs)
    while container.get_value() is None:
        from time import sleep
        sleep(0.15)
    val = container.get_value()
    if isinstance(val, Exception):
        raise val
    else:
        return val

class no_login_manager:

    __handler = None

    def set_no_login_handler(self, func):
        self.__handler = func

    def checkException(self):
        if self.__handler is not None:
            try:
                if self.__handler():
                    raise NoLoginException(True)
                else:
                    raise NoLoginException(False)
            except NoLoginException as e:
                raise e
            # except Exception as e:
            #    print("Exception occured in checkException, will emit a NoLoginException")
            #    raise NoLoginException(False)
        raise NoLoginException(False)

global_no_login_manager = no_login_manager()


class NoLoginException(Exception):

    retry = False
    def __init__(self, retry=False):
        super(NoLoginException, self).__init__("No Session or timedout")
        self.retry = retry