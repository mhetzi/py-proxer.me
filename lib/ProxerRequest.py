# -*- coding: utf-8 -*-
from http.client import NotConnected

__JavaScriptSupport__ = True
__WebFallBack__ = False
__noScrape__ = False

from time import sleep
import urllib
import urllib.error
import http
import http.cookiejar
import os
from urllib import parse

try:
    import execjs
except:
    print("Python no Javascript installed")
    __JavaScriptSupport__ = False

try:
    import cfscrape
except:
    print("Cloudflare Scraping module not found!")
    __noScrape__ = True
try:
	import libs.cfscrapeEv
except:
    print("Cloudflare Scraping module not found!")
    __noScrape__ = True
try:
    import requests
except:
    print("Python requests not installed")
    __WebFallBack__ = True

__author__ = 'marcel'
__Host__ = "https://proxer.me"
__force_no_https__ = False

class OldProxerRequest:
    jar = None
    storFile = None

    __checkUrlErr = False

    def __init__(self, StorageFile):
        """
        :rtype : ProxerRequest
        """
        if StorageFile is not None:
            self.storFile = StorageFile
            self.jar = http.cookiejar.LWPCookieJar(StorageFile)
            if not os.path.exists(StorageFile):
                # Create a new cookies file and set our Session's cookies
                print('Cookies werden erstellt...')
                self.jar.save(ignore_discard=True, ignore_expires=True)
            else:
                # Load saved cookies from the file and use them in a request
                print('Lade Cookies von Datei...')
                try:
                    self.jar.load(ignore_discard=True)
                except http.cookiejar.LoadError:
                    print("Recreating cookieFile...")
                    self.jar.save(ignore_discard=True, ignore_expires=True)
                    self.jar.load(ignore_discard=True, ignore_expires=True)
                print("Geladen!")

    _err = 0

    def request(self, url, post, raw=False):
        headers = {"Content-type": "application/x-www-form-urlencoded",
                   "Accept": "text/plain,text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                   "User-Agent": "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36"}

        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self.jar))

        ipost = None

        if post is not None:
            ipost = urllib.parse.urlencode(post).encode('utf-8')

        request = urllib.request.Request(url, ipost, headers)
        page = None
        try:
            page = opener.open(request)
        except urllib.error.HTTPError as e:

            if isinstance(e, NotConnected):
                raise

            if isinstance(e, urllib.error.URLError):
                if self.__checkUrlErr:
                    raise
            if self.UrlOpenErr(e):
                raise
            else:
                raise NotConnected()
            sleep(2)
            self._err += 1
            if self._err > 10:
                raise
            return self.request(url, post, raw)
        try:
            try:
                os.remove(self.storFile + ".bak")
            except EnvironmentError:
                pass
            os.rename(self.storFile, self.storFile + ".bak")
            self.jar.save(ignore_discard=True, ignore_expires=True)
        except AttributeError:
            print("Nicht gespeichert! Backup wird gespeichert!")
            os.rename(self.storFile + ".bak", self.storFile)
            pass
        if not raw:
            return page.fp.read().decode("utf-8")
        else:
            return page.fp.read()

    def UrlOpenErr(self, error):
        self.__checkUrlErr = True
        try:
            self.request("http://google.at/", None)
            self.__checkUrlErr = False
            return True
        except urllib.error.URLError:
            self.__checkUrlErr = False
            return False
        except NotConnected:
            self.__checkUrlErr = False
            return False


class ProxerRequest(OldProxerRequest):

    def getRequestMethod(self):
        if __WebFallBack__:
            return "urllib2"
        else:
            return "Requests lib"

    def set_fsk_handler(self, handler):
        self.__fsk_handler = handler

    def __init__(self, StoragePath=None, cloudflareEvent=None, timeout=10, exception_callback=None):
        super(ProxerRequest, self).__init__(StoragePath)
        self.__timeout = timeout
        self.exception_callback = exception_callback
        self.cloudflare_event = cloudflareEvent
        if not __WebFallBack__:
            self.session = requests.session()
            if not __noScrape__:
                adapter = libs.cfscrapeEv.cloudFlareEvented()
                self.session.mount("http://", adapter)
                self.session.mount("https://", adapter)
                adapter.setEvent(self.cloudflare_event)
            self.CookieFile = StoragePath
            self.load()
            self.__fsk_handler = None
            if self.exception_callback is None:
                print("[WARN] ProxerRequest: No request error callback!")

    def request(self, url, post=None, Json=None, raw=False, retry=True, retry_count=-10, in_retry=False, headers=None):
        if url is None or url == "":
            return
        if __force_no_https__:
            url = url.replace("https", "http")
        try:
            if __WebFallBack__:
                super(ProxerRequest, self).request(url, post)

            if raw:
                return super(ProxerRequest, self).request(url, post, raw)

            if post is None and Json is None:
                r = self.session.get(url, timeout=self.__timeout, headers=headers)
            else:
                r = self.session.post(url, data=post, json=Json, timeout=self.__timeout, headers=headers)

            text = parse.unquote(r.text)
            if "class=\"cf-subheadline\"><span data-translate=\"complete_sec_check\"" in text:
                raise Exception("Cloudflare protection active!")

            val = self.checkFSK(text)
            if val is 0:
                return text, r
            elif val is 1:
                if post is None:
                    r = self.session.get(url)
                else:
                    r = self.session.post(url, data=post)
                return r.text, r
            elif val is 2:
                return "Du musst 18+ sein um das zu sehen!", None
        except Exception as e:
            if not __WebFallBack__:
                from requests.exceptions import MissingSchema
                if isinstance(e, MissingSchema):
                    print("MissingSchema not a valid url!")
                    raise e
            if self.exception_callback is not None and retry:
                if retry_count > 0:
                    retry_count -= 1
                elif retry_count != -10:
                    return ("Reached Limit of retries", None)
                try:
                    print("Trying to handle the connection error...")
                    print("Requested URL was " + url)
                    do_retry = self.exception_callback(e)
                    r = None
                    val = None
                    text = None
                    if "Max retries exceeded with url" in str(e):
                        print("Recreating requests object...")
                        self.save()
                        if not __WebFallBack__:
                            self.session = requests.session()
                            if not __noScrape__:
                                adapter = libs.cfscrapeEv.cloudFlareEvented()
                                self.session.mount("http://", adapter)
                                self.session.mount("https://", adapter)
                                adapter.setEvent(self.cloudflare_event)
                            self.load()
                            self.__fsk_handler = None
                            if self.exception_callback is None:
                                print("[WARN] ProxerRequest: No request error callback!")
                    if in_retry:
                        return ("retry", "retry")
                    else:
                        """ Prevent stack overflow """
                        do_retrying = True
                        while do_retrying:
                            val, val2 = self.request(url, post, Json, raw, do_retry, retry_count, in_retry=True)
                            if retry_count != -10:
                                retry_count -= 1
                            elif retry_count < 1:
                                return ("Reached Limit of retries", None)
                            if "retry" in val and "retry" in val2:
                                do_retrying = True
                            else:
                                return val, val2
                            sleep(0.5)
                except Exception as ee:
                    print(ee)
                    print("During handling a connection exception this came by!")
            else:
                print("[ERROR] ProxerRequest: Rethrowing exception cauze it can´t be handled in here!")
            raise e

    def save(self):
        if self.CookieFile is None:
            return
        if __WebFallBack__:
            return
        try:
            try:
                os.remove(self.CookieFile + ".bak")
            except EnvironmentError:
                pass
            if os.path.exists(self.CookieFile):
                os.rename(self.storFile, self.storFile + ".bak")
            self.session.cookies.save(ignore_discard=True, ignore_expires=True)
        except AttributeError:
            print("Cookies: Nicht gespeichert! Backup wird gespeichert!")
            os.rename(self.CookieFile + ".bak", self.CookieFile)
            pass

    def load(self):
        if __WebFallBack__:
            return
        if self.CookieFile is None:
            return
        self.session.cookies = http.cookiejar.LWPCookieJar(self.CookieFile)
        if not os.path.exists(self.CookieFile):
            print("Cookies: Keine Datei! Erstelle neue..")
            self.session.cookies.save()
        self.session.cookies.load(ignore_discard=True, ignore_expires=True)

    def checkFSK(self, pageData):
        if "<h3>Dieser Bereich darf nur von Benutzern &uuml;ber 18 Jahren betreten werden! Eltern haften f&uuml;r ihre Kinder. Proxer.Me &uuml;bernimmt keinerlei Haftung!</h3>" in pageData \
                or "<h3>Dieser Bereich darf nur von Benutzern über 18 Jahren betreten werden! Eltern haften für ihre Kinder. Proxer.Me übernimmt keinerlei Haftung!</h3>" in pageData:
            if self.__fsk_handler is not None:
                value = self.__fsk_handler()
                if value is not None:
                    if value:
                        return 1
                    else:
                        return 2
        else:
            return 0

    def setCookie(self, cookie):
        if __WebFallBack__:
            self.jar.set_cookie(cookie)
        else:
            try:
                self.session.cookies.set_cookie(cookie)
            except Exception as e:
                print(e)

    def is_online(self, url):
        r = self.session.head(url, timeout=5)
        if r.status_code == 404:
            return False
        elif r.status_code == 200:
            return True
        else:
            print("Got status code: " + r.status_code + " don´t know if its online!")
            return False