# -*- coding: utf-8 -*-
__author__ = 'marcel'

import re

import lib.ProxerRequest
import lib.ProxerSerie

__scriptFinder__ = re.compile("<script type=\".*var pages.*serverurl.*;", re.DOTALL)
__pagesFinder__ = re.compile("var pages = .*?;", re.DOTALL)
__serverurlFinder__ = re.compile("var serverurl.*?;", re.DOTALL)
__jpgFinder__ = re.compile("\".*?\..*?\"")


class Chapter:
    @staticmethod
    def getPictUrls(base: lib.ProxerSerie.Episode, language, retry=True):
        if language is None or language == "":
            url = None
        else:
            url = base.get_subtypes()[language]

        if url is None and retry:
            for u in base.get_subtypes().keys():
                print("Using alternative lang: " + u)
                var = Chapter.getPictUrls(base, u, False)
                if var is not None:
                    return var
            return None
        elif url is not None:
            url = url.replace("#top", "/1").replace("chapter", "read")
        else:
            return None

        strpage, r = base.pxreq.request(url, None)

        not_released = False
        if "Diese Seite wurde nicht gefunden oder wurde verschoben" in strpage:
            sspage, rr = base.pxreq.request(url.replace("read", "chapter"))
            not_released = "Dieses Kapitel ist leider noch nicht verf" in sspage

        try:
            script = __scriptFinder__.search(strpage).group(0)
            pages = __pagesFinder__.search(script).group(0)
            pages = __jpgFinder__.findall(pages)
            surl = __serverurlFinder__.search(script).group(0)
            surl = "http:" + surl.replace("var serverurl", "").replace("\\", "")

            urls = []

            for p in pages:
                pp = surl + p
                urls.append(
                    pp.replace(";", "").replace("\\", "").replace("'", "").replace(" ", "").replace("=", "").replace("\"",
                                                                                                                     ""))

            return urls
        except AttributeError as e:
            if not_released:
                raise lib.ProxerSerie.EpisodeNotAvaliableException(base)
            else:
                print(e)
        if not_released:
            raise lib.ProxerSerie.EpisodeNotAvaliableException(base)
