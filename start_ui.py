#!/bin/python3

import getopt
import sys

import gui.ProxerGTK
import code, traceback, signal

from gi.repository import Gtk, Gdk

def set_proc_name(newname):
    from ctypes import cdll, byref, create_string_buffer
    libc = cdll.LoadLibrary('libc.so.6')
    buff = create_string_buffer(len(newname)+1)
    buff.value = newname
    libc.prctl(15, byref(buff), 0, 0, 0)

def get_proc_name():
    from ctypes import cdll, byref, create_string_buffer
    libc = cdll.LoadLibrary('libc.so.6')
    buff = create_string_buffer(128)
    # 16 == PR_GET_NAME from <linux/prctl.h>
    libc.prctl(16, byref(buff), 0, 0, 0)
    return buff.value

def debug(sig, frame):
    """Interrupt running process, and provide a python prompt for
    interactive debugging."""
    d = {'_frame': frame}  # Allow access to frame object.
    d.update(frame.f_globals)  # Unless shadowed by global
    d.update(frame.f_locals)

    i = code.InteractiveConsole(d)
    message = "Signal received : entering python shell.\nTraceback:\n"
    message += ''.join(traceback.format_stack(frame))
    i.interact(message)


def listen():
    signal.signal(signal.SIGUSR1, debug)  # Register handler


if __name__ == "__main__":

    args = sys.argv[1:]

    try:
        set_proc_name("proxerMe".encode('utf-8'))
    except Exception as e:
        print(e)
    try:
        print("process name: " + str(get_proc_name()))
    except:
        print("")

    win = gui.ProxerGTK.HeaderBarWindow()
    force_suche = False
    start_page = None
    try:
        opts, args = getopt.getopt(args, "hp:s:", ["page=suche="])
        for opt, arg in opts:
            if opt in ("-p", "--page"):
                if not force_suche:
                    start_page = arg
            elif opt == "-h":
                print("proxer.Me Application")
                print(
                    "-h Dieses anzeigen.\n-p oder --page eine Hauptseite wählen\n --> Zur Auswahl stehen start, anime, manga, suche")
                print("-s oder --suche mit Suchwort öffnet die Suchseite mit den gefundenen Medien")
                exit(0)
            elif opt in ("-s", "--suche"):
                force_suche = True
                win.suche(arg)
    except getopt.GetoptError:
        pass
    listen()
    signal.signal(signal.SIGUSR2, lambda x, y: win.get_player().timer.toggle())
    win.start_app("proxer.me", start_page)
