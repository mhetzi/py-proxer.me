import lib.ProxerSerie
import lib.ProxerRequest

req = lib.ProxerRequest.ProxerRequest()
s = lib.ProxerSerie.Serie(req)
s._MainURL = "https://proxer.me/info/53"
s.Gesehen = 0

tup = s.check_new_eps()
if tup[0]:
    print("Neue Folge verfügbar " + str(tup[1]))
else:
    print("Keine neue Folge!")