__author__ = 'marcel'
__package__ = "libs"

class Type:
    NONE = -1
    POPULARS = -2
    RANDOMS = -3
    RECENTS = -4
    PLAYLISTS = -5
    RADIOS = -6
    EXTERNALS = -7
    ALL = -8
    MPD = -996
    LOVED = -997
    NEVER = -998
    DEVICES = -1000
    SEPARATOR = -2000
    COMPILATIONS = -2001


class ArtSize:
    SMALL = 33
    MEDIUM = 48  # If changed, adapt width request in AlbumRow.ui
    BIG = 200
    MONSTER = 500


