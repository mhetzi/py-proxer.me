# -*- coding: utf-8 -*-

__author__ = "marcel"

import lib.ProxerRequest


class Kodi:
    def buildJSON(self, method: str, params: dict):
        json = []
        jsonData = {"jsonrpc": "2.0", "method": method, "id": 1}
        json.append(jsonData)
        if params is not None:
            jsonData["params"] = params
        return json

    def sendJson(self, data_liste: list):
        text, r = self.__pxRequest.request(url=self.__host, Json=data_liste)
        json = r.json()
        return json

    def set_host(self, host: str):
        if "/jsonrpc" in host:
            self.__host = host
        else:
            self.__host = host + "/jsonrpc"
            self.__host = self.__host.replace("//", "").replace(":/", "://")

        if "http://" not in self.__host:
            self.__host = "http://" + self.__host

    def __init__(self, px_request: lib.ProxerRequest.ProxerRequest, host):
        self.__pxRequest = px_request
        self.__host = None
        self.set_host(host)
        self.__default_video_playlist_id = 1

    def get_play_lists(self):
        j = self.buildJSON("Playlist.GetPlaylists", None)
        r = self.sendJson(j)
        return r[0]["result"]

    def get_video_playlist_id(self):
        ar = self.get_play_lists()
        for pi in ar:
            if pi.get("type", "none") == "video":
                self.__default_video_playlist_id = pi.get("playlistid", 1)
                return pi.get("playlistid", 1)
        return -1

    def clear_playlist(self, playlist_id):
        j = self.buildJSON("Playlist.Clear", {"playlistid": playlist_id})
        r = self.sendJson(j)
        try:
            if r[0]["result"].lower() == "OK".lower():
                return True
        except Exception as e:
            print("Excption on checking if file was added")
        return False

    def add_to_playlist(self, url, playlist_id: int, instant_play=False):
        params = {}
        jsonItem = {"file": url}
        params["playlistid"] = playlist_id
        params["item"] = jsonItem

        json = self.buildJSON("Playlist.Add", params)

        r = self.sendJson(json)
        done = False
        try:
            if r[0]["result"].lower() == "OK".lower():
                done = True
        except Exception as e:
            print("Excption on checking if file was added")

        if done and instant_play:
            return self.play_item(playlist_id)
        else:
            return done

    def play_item(self, playlistid: int, position=0):
        params = {}
        jsonItem = {"playlistid": playlistid, "position": position}
        params["item"] = jsonItem

        json = self.buildJSON("Player.Open", params)

        r = self.sendJson(json)
        done = False
        try:
            if r[0]["result"].lower() == "OK".lower():
                done = True
        except Exception as e:
            print("Excption on checking if file is playing")
        return done

    def get_playing_infos(self, playerid):
        json = [{"jsonrpc": "2.0", "id": 2, "method": "Player.GetProperties", "params": {"playerid": playerid,
                                                                                         "properties": ["audiostreams",
                                                                                                        "canseek",
                                                                                                        "currentaudiostream",
                                                                                                        "currentsubtitle",
                                                                                                        "partymode",
                                                                                                        "playlistid",
                                                                                                        "position",
                                                                                                        "repeat",
                                                                                                        "shuffled",
                                                                                                        "speed",
                                                                                                        "subtitleenabled",
                                                                                                        "subtitles",
                                                                                                        "time",
                                                                                                        "totaltime",
                                                                                                        "type"]}},
                {"jsonrpc": "2.0", "id": 3, "method": "Player.GetItem", "params": {"playerid": playerid,
                                                                                   "properties": ["album",
                                                                                                  "albumartist",
                                                                                                  "artist", "director",
                                                                                                  "episode", "fanart",
                                                                                                  "file", "genre",
                                                                                                  "plot", "rating",
                                                                                                  "season", "showtitle",
                                                                                                  "studio",
                                                                                                  "imdbnumber",
                                                                                                  "tagline",
                                                                                                  "thumbnail", "title",
                                                                                                  "track", "writer",
                                                                                                  "year",
                                                                                                  "streamdetails",
                                                                                                  "originaltitle",
                                                                                                  "cast",
                                                                                                  "playcount"]}}]
        ret = {}
        rjson = self.sendJson(json)
        for obj in rjson:
            if obj.get("id", 0) is 2:
                robj = obj.get("result", {})
                ret["time"] = robj.get("time", {})
                ret["ttime"] = robj.get("totaltime", {})
            elif obj.get("id", 0) is 3:
                robj = obj.get("result", {})
                ret["media"] = robj.get("item", {})
        return ret

    def get_active_players(self, default_id=1):
        j = self.buildJSON("Player.GetActivePlayers", None)
        r = self.sendJson(j)
        try:
            return r[0]["result"][0].get("playerid", default_id)
        except IndexError as e:
            return default_id

    def play_pause(self, playerid, toggle=True, do_play=False):
        par = {}
        par["playerid"] = playerid
        if toggle:
            par["play"] = "toggle"
        else:
            par["play"] = do_play

        json = self.buildJSON("Player.PlayPause", par)
        r = self.sendJson(json)
        try:
            return r[0]["result"]
        except IndexError as inde:
            return {"Failure": True}

    def __seek(self, seek_obj, playerid):
        j = self.buildJSON("Player.Seek", {"value": seek_obj, "playerid": playerid})
        r = self.sendJson(j)
        pass

    def seek_by_time(self, playerid, hours=0, mins=0, secs=0, millisecs=0):
        self.__seek({"hours": hours, "milliseconds": millisecs, "seconds": secs, "minutes": mins}, playerid)

    def seek_by_percent(self, percent, playerid):
        self.__seek({"percentage": percent}, playerid)

    def playlist_get_items(self, playlistid):
        j = self.buildJSON("Playlist.GetItems", {"playlistid": playlistid, "properties": ["file"]})
        r = self.sendJson(j)
        return r

    def get_playlist_item_count(self, playlistid):
        j = self.playlist_get_items(playlistid)
        try:
            return j[0].get("result", {}).get("limits", {}).get("total", 0)
        except IndexError as e:
            return 0

    def get_video_playlist_id_default(self):
        return self.__default_video_playlist_id
