import weakref

__author__ = 'marcel'

from threading import Timer


class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.do_stop = False
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.kwargs["RepeatedTimerRef"] = weakref.ref(self)
        self.is_running = False
        self.oldRunns = False
        self.start()

    def _run(self):
        if self.do_stop:
            return
        # if self.oldRunns:
        #     self.is_running = False
        #     self.__start()
        #     return
        self.is_running = False
        self.oldRunns = True
        if isinstance(self.function, weakref.WeakMethod):
            # print("Calling weakreaf method...")
            t = self.function()
            if t is None:
                print("Weakref is dead!")
                self.stop()
            else:
                t()
        else:
            self.function(*self.args, **self.kwargs)
        self.__start()
        self.oldRunns = False

    def __start(self):
        if self.do_stop:
            return
        self.start()

    def start(self):
        if self.do_stop:
            self.do_stop = False
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.setDaemon(True)
            self._timer.setName("Playerwindow_Status_Updater")
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False
        self.do_stop = True

    def toggle(self):
        if self.do_stop:
            self.start()
            print("RepeatedTimer started!")
        else:
            self.stop()
            print("RepeatedTimer stopped!")

    def destroy(self):
        self.stop()
        self._timer.cancel()
        del self._timer
        del self.interval
        del self.function
