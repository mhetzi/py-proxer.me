
class Pretty:

    @staticmethod
    def human_size(nbytes):
        suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
        import math
        rank = int((math.log10(nbytes)) / 3)
        rank = min(rank, len(suffixes) - 1)
        human = nbytes / (1024.0 ** rank)
        f = ('%.2f' % human).rstrip('0').rstrip('.')
        return '%s %s' % (f, suffixes[rank])

    @staticmethod
    def GetHumanReadable(size, precision=2):
        size = int(size)
        suffixes = ['B', 'KB', 'MB', 'GB', 'TB', "EB"]
        suffixIndex = 0
        while size > 1024:
            suffixIndex += 1 #increment the index of the suffix
            size /= 1024.0  #apply the division
        return "%.*f %d" % (precision, size, suffixes[suffixIndex])

    @staticmethod
    def list_to_string(liste: list, separator=", "):
        str = ""
        for l in liste:
            if str == "":
                str = str + l
            else:
                str = str + separator + l
        return str