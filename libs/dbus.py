# Copyright (c) 2014-2015 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# Copyright (c) 2013 Arnel A. Borja <kyoushuu@yahoo.com>
# Copyright (c) 2013 Vadim Rutkovsky <vrutkovs@redhat.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
from random import randint

from gi.repository import Gst, GLib, Gio

import gui.ProxerPlayer
from libs import Type, ArtSize


class MPRIS(dbus.service.Object):
    MPRIS_IFACE = 'org.mpris.MediaPlayer2'
    MPRIS_PLAYER_IFACE = 'org.mpris.MediaPlayer2.Player'
    MPRIS_NAME = 'org.mpris.MediaPlayer2.'
    MPRIS_PATH = '/org/mpris/MediaPlayer2'
    #:gui.ProxerPlayer.PxPlayerGTK

    def __init__(self, player, Playername):
        self.Lp = Gio.Application.get_default
        DBusGMainLoop(set_as_default=True)
        self.busname = dbus.service.BusName(self.MPRIS_NAME + Playername, dbus.SessionBus(), allow_replacement=True, replace_existing=True)
        dbus.service.Object.__init__(self, object_path=self.MPRIS_PATH, bus_name=self.busname)
        self._app = player
        self.player = player
        self._metadata = {}
        self.player.connect_player('current-changed', self._on_current_changed)
        self.player.connect_player('seeked', self._on_seeked)
        self.player.connect_player('status-changed', self._on_status_changed)
        self.player.connect_player('volume-changed', self._on_volume_changed)
        self._name = self.busname
        self._playername = Playername
        self.closed = False

    def __del__(self):
        if not self.closed:
            print("Cleaning dbus up...")
            self.close()

    def close(self):
        self.remove_from_connection(None, self.MPRIS_PATH)

    @dbus.service.method(dbus_interface=MPRIS_IFACE)
    def Raise(self):
        self._app.do_activate()

    @dbus.service.method(dbus_interface=MPRIS_IFACE)
    def Quit(self):
        self._app.destroy()

    @dbus.service.method(dbus_interface=MPRIS_PLAYER_IFACE)
    def Next(self):
        self.player.nextVideo()

    @dbus.service.method(dbus_interface=MPRIS_PLAYER_IFACE)
    def Previous(self):
        self.player.prev()

    @dbus.service.method(dbus_interface=MPRIS_PLAYER_IFACE)
    def Pause(self):
        self.player.pause()

    @dbus.service.method(dbus_interface=MPRIS_PLAYER_IFACE)
    def PlayPause(self):
        self.player.play_pause()

    @dbus.service.method(dbus_interface=MPRIS_PLAYER_IFACE)
    def Stop(self):
        self.player.start_stop("stop")

    @dbus.service.method(dbus_interface=MPRIS_PLAYER_IFACE)
    def Play(self):
        self.player.play()

    @dbus.service.method(dbus_interface=MPRIS_PLAYER_IFACE,
                         in_signature='ox')
    def SetPosition(self, track_id, position):
        self.player.seek(position/1000000)

    @dbus.service.method(dbus_interface=MPRIS_PLAYER_IFACE,
                         in_signature='s')
    def OpenUri(self, uri):
        pass

    @dbus.service.signal(dbus_interface=MPRIS_PLAYER_IFACE,
                         signature='x')
    def Seeked(self, position):
        pass

    @dbus.service.method(dbus_interface=dbus.PROPERTIES_IFACE,
                         in_signature='ss', out_signature='v')
    def Get(self, interface, property_name):
        return self.GetAll(interface)[property_name]

    @dbus.service.method(dbus_interface=dbus.PROPERTIES_IFACE,
                         in_signature='s', out_signature='a{sv}')
    def GetAll(self, interface):
        if interface == self.MPRIS_IFACE:
            return {
                'CanQuit': True,
                'CanRaise': True,
                'HasTrackList': False,
                'Identity': self._name
            }
        elif interface == self.MPRIS_PLAYER_IFACE:
            return {
                'PlaybackStatus': self._get_status(),
                'LoopStatus': 'None',
                'Rate': dbus.Double(1.0),
                'Shuffle': False,
                'Metadata': dbus.Dictionary(self._metadata, signature='sv'),
                'Volume': dbus.Double(self.player.get_volume()),
                'Position': dbus.Int64(
                    self.player.get_position()),
                'MinimumRate': dbus.Double(1.0),
                'MaximumRate': dbus.Double(1.0),
                'CanGoNext': self.player.have_next_video(),
                'CanGoPrevious': False,
                'CanPlay': True,
                'CanPause': self.player.is_playing(),
                'CanSeek': True,
                'CanControl': True,
            }
        else:
            raise dbus.exceptions.DBusException(
                self.MPRIS_NAME,
                "MPRIS_Implementation doesn't handle %s interface"
                % interface)

    @dbus.service.method(dbus_interface=dbus.PROPERTIES_IFACE,
                         in_signature='ssv')
    def Set(self, interface, property_name, new_value):
        if property_name == 'Volume':
            self.player.set_volume(new_value)

    @dbus.service.signal(dbus_interface=dbus.PROPERTIES_IFACE,
                         signature='sa{sv}as')
    def PropertiesChanged(self, interface, changed_properties,
                          invalidated_properties):
        pass

    #######################
    # PRIVATE             #
    #######################

    def _get_status(self):
        state = self.player.get_status()
        if state == Gst.State.PLAYING:
            return 'Playing'
        elif state == Gst.State.PAUSED:
            return 'Paused'
        elif state == Gst.State.READY:
            return 'Ready'
        else:
            return 'Stopped'

    def _update_metadata(self):
        if self._get_status() == 'Stopped':
            self._metadata = {}
        else:
            # MPRIS SUX
            track_id = randint(10000000, 90000000)
            trpath = '/org/' + self._playername + '/' + str(track_id)
            self._metadata['mpris:trackid'] = dbus.ObjectPath(trpath)

            track_number = self.player.get_ep_num()
            if track_number is None:
                track_number = 1
            self._metadata['xesam:trackNumber'] = track_number
            self._metadata['xesam:title'] = self.player.get_current_serie().getName()
            self._metadata['xesam:album'] = ""
            self._metadata['xesam:artist'] = [""]
            self._metadata['xesam:albumArtist'] = [""]
            #                                                                       microseconds
            self._metadata['mpris:length'] = dbus.Int64(self.player.get_duration() * 1000000)
            self._metadata['xesam:genre'] = self.player.get_current_serie().Genre
            self._metadata['xesam:url'] = self.player.get_last_url()
            self._metadata["xesam:userRating"] = self.player.get_current_serie().Rating / 5
            cover_path = self.player.get_current_serie().coverUrl
            if cover_path is not None:
                self._metadata['mpris:artUrl'] = cover_path
            elif 'mpris:artUrl' in self._metadata:
                self._metadata['mpris:artUrl'] = ''

    def _on_seeked(self, player, position):
        self.Seeked(position * 1000000)

    def _on_volume_changed(self, player, data=None):
        self.PropertiesChanged(self.MPRIS_PLAYER_IFACE,
                               {'Volume': dbus.Double(
                                   self.player.get_volume()), },
                               [])

    def _on_current_changed(self, player):
        self._update_metadata()
        properties = {'Metadata': dbus.Dictionary(self._metadata,
                                                  signature='sv')}
        try:
            self.PropertiesChanged(self.MPRIS_PLAYER_IFACE, properties, [])
        except Exception as e:
            print("MPRIS::_on_current_changed(): %s" % e)

    def _on_status_changed(self, data=None):
        properties = {'PlaybackStatus': self._get_status()}
        self.PropertiesChanged(self.MPRIS_PLAYER_IFACE, properties, [])