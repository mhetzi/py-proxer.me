__author__ = 'marcel'
import cfscrape


class cloudFlareEvented(cfscrape.CloudflareAdapter):
    __OnCfEvent__ = None

    def setEvent(self, event):
        self.__OnCfEvent__ = event

    def solve_cf_challenge(self, resp, headers, cookies,  **kwargss):
        try:
            self.__OnCfEvent__(False)
        except Exception as e:
            print("Cloudflare Event couldn´t be fired!")
            print("Continuing with the scraping...")

        var = super(cloudFlareEvented, self).solve_cf_challenge(resp, headers, cookies, **kwargss)

        try:
            self.__OnCfEvent__(False)
        except Exception as e:
            print("Cloudflare Event couldn´t be fired!")
            print("Continuing with the scraping...")

        return var
