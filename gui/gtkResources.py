import os

from gi.repository import GObject
from gi.repository import Gtk, Gio
from gi.repository.GdkPixbuf import Pixbuf

try:
    from StringIO import StringIO
except ImportError:
    import io

__author__ = 'marcel'

import base64
import lib.ProxerRequest
import lib

__bellBase64__ = """iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAAXNJREFUSInN1U1LVVEUxvFf2lVBA18QMQc5sFAHDXwBRcKpUEF9iYi+
RDoQBw79DOI30IkzQYQCFQeCjsSXfCknBSYVt8HewoW8x7NPCj6wR3ut53/OXmvtzT1SL/axh568
SbUJgHf4FiGtWE35uiwNYQEnKMd1jHkM/K/5W3zBezxBKa5ufIh7r4qa1+EQwxkxIziI0GSNy3fO
nzBWbbMmI7EPWzkAm+gvAmgTuuYmfY2xyYASfuUAXKK+CKAGf3IAyjLmKQvQiIscgAs0FAF0CX1+
k45jbJJKOMKzHLH9wvXxMAUwjaWE+GVM5Ql8EM230ZEA6MQOPkaPquZz+Iz2BPMrdWADs9UgM8LY
Nxcwv1Ir1jF53ealjJFP0HMV7V1Z+SmsCMVdE870COf4gZ/4XZHXgEdoEdr0KUYxIdQC/57VY7zE
oPAsdgq/3RRXuSLvewSfC/OyK9RvUZiNZJ0J9/8L4Z24db3BqfB0vr4LQCH9BcDURXCLyaFSAAAA
AElFTkSuQmCC
"""
__bellAuthorLink__ = "https://www.iconfinder.com/Umar"
__bellLicense__ = "Free for commercial use"

__addFriendsBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAAhVJREFUWIXF179rFEEUB/DPxcOcRBRsguCPf0BQLLSwF8xhKQixstFC
bCwsxNLCRkjhf6ASA4pBC1NIUBRFFBQMaLBSUAiaCBEVjHcWMyebzd3e5G45v/DY2dk3733n7Zs3
M6zGGKaxgAaaJcpX3MNRbVDDZMkOi2Qq+vyHQTpvyc1s2AftvCX1Kk61+ycdsBij9Tq+H8BxjKzD
RhanCQmXwvY2trYxsh2PE23k5Qtp2f4Q1YKZbMZ8DwQaEhX3Fzhv4ViPUeiq8DHBOWzCynoJDCUY
/pxI4Ce+5fpu4Sy+Fw3sxvJDIoGatRHYHb91rDMpEdiJvQl6Y9iQ6/sTnyudBhVldhZXcDhjMI8R
XIrtB3gT28vxeV9Ychsxji3ZwakJMykstzxGMRt1lqyNQh4XsnYrsZGKBVwXKmFFqITZGTVxEq/i
+xx+Yxe2YRgTOJg1mjL7CezDRdzBczwTdrVz2CNkfH7cjujjWoHtrpXqTGJ0KrhcNoHzic6zuJoZ
Py7sH3O9EJjtwTkh099aHcWeSvGhHgkQZp66utp2vuvDOaEqLqcQ6FQJZ/ok8AuPUhQ7EXjRJwF4
2Q+B+RIIvO+HQOoWXIRP/RBYKoFA/mzQDs0h4aSbx48SCKTYWKziKeq5DydKIDCaoPOEcFdLLRpl
y5EWk6n/4PxGNhQ14a42SOfD7f5JHXeF41OZ1/NGtDmdDTv8Ba03+c9pDbIhAAAAAElFTkSuQmCC
"""
__addFriendsAuthorLink__ = "https://www.iconfinder.com/zapolzun"
__addFriendsLicense__ = "Free for commercial use (Include link to authors website)"

__newMessageBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAA01JREFUWIXtll9sU2UYxp/361YrEmNkgCGoS9R4QVuoJUh7Nmq8UGMM
DsyqMVFjQthaiUhmxJiYZQkJF5rwZ3JK1WReKiQgMUQJwQjsdDOhmWu3hSxEjIELIOAYyPr3e7ix
Sx3tdgq90j1353uf7/09ec97kgPMa17/d0n5g9+/uTG74EoYwidJSrVLdwUSIYW/u24u/TaZ/DJf
OlflpqkHLh2m8N16wwGApIDyTub+y99XNHhDG9zu1vXngG5V0VAXdSt36/pznnWvekon07AidJMW
XAB69LrY6FMt+1Jt9cIa5vCba78aeRjo0VpwQQsW3RGgXFnk8gQ/N8zUp/dE7qYyzPReUG3L60yx
kqVigF8jvj+kUAxokZcNM93nj59prJXtj59ZYCwdOaTB5kxDNpTsWH3ddgAA6N/6zBXeuvE8wIUu
7fwptGvoIbvwgPnbEpdu/IXgxeVNZzckO1bfquaddeEGu4JTVsQTBpDMOZX1bGyoeS540Bx+WlES
pDqYiHrfOxgOVxy9rQAAABFaEe9H4pDeBjistbGRNbPAW0XkZyj5JBH1fDZnb1sB/lGi07sf5CYH
+IOxP71xZj1gpt8g8R2Uet2KeA/Y7VvbNy9SEIUPNfVuw0x1lY6NfcPbFfUOpeQDFIuFWlo22DUG
zfQmgD0s6tcoOqCpjhqx1BOgCET7NRkElU+JHDFiqU4r4j1sp+/cEyAlGEvvFKJLiFYrunJwMOK7
6KSjBZDHIFyWUfnnBqKrLg9EPcc01UvQ0hswU+/bCTDrBEJ9512FWOobinpEhMH+qPevUu3kFvdN
AK/MvDMQXTFkxEeCKOgfDTPVbEU8XRBhNUbVCYTiZ5sKmckTFOQmFskL5fC5ZHW4/3TmtKFJnxFL
Hwj1nXfVFCDwxejj+UIuAajjicjKt8fC7pxdeEknt/kmri92vAhKtpCZPLFmz/iDlXwVX4HDmb+P
2vGx1ek5VCu4XGNhdw7kW8F4ekuDKi4EMFk1gBT1NRFZDnSr/s2rxgGM3wt8WiJMAL0A0N7e7hi9
lH1UEVeny+XeFS1tR0XRReJ0XeAzs0BCWvj32Kkj08v7rx1wTS1pg5Y+mWVr7xouQkC+XiwTdfvP
mNe8/hu6DYrjUFPkHHySAAAAAElFTkSuQmCC
"""
__newMessageAuthorLink__ = "https://www.iconfinder.com/DemSt"
__newMessageLicense__ = "Creative Commons (Attribution 3.0 Unported)"

__newsBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAAZtJREFUWIXtlL1qG0EUhb97I1iDi3Ul/wQMAhWpBNIUq3cR+AVc+zFc
uFaVNwipQ8pgRRmNyuDKYEIMLgxCpXTXjQzyRpK1K63S6DQ7s2d2ztl77oyQQb1ej+I4vgY6wFGW
3wbMbCQin0ej0dWHLFmr1W6AS+CgDHEAEYlEJImiKK4s4DuzZ+K9/1mGgVarlYjIrYhc6AL+CKAs
cYDBYNCbDRdW4A2cc78At4w3s14IoV3UTGWNplsqDqCqSVFxgMpM/PK9hd57yb5zzqXz82az+VdV
T1Zsc++9r70xQKbpspvmQQjhNO83yg6abhXebcI8KBrB1lAkgrUNbNIbGxkws96qo5am6Y/XcSkR
5Llkip6C/4p1ruI/wNk2xMzsIYRwnsuA9/7jNsSXYR/BPoKNI5gva561axvIE0GRuPYR7CP4pwJm
1hOR6eu87AjEOTcGDqfTaXU4HD4V+IncSJLkeDKZPAJjBb4DqGq30WhUdyTeBTCzb+Kc+wTcAnHZ
4hk8q2pbvfe/gXaapl+B8Q6Ex2b2RVXb/X7/7gXvQdCSRpSeqgAAAABJRU5ErkJggg==
"""
__newsAuthorLink__ = "https://www.iconfinder.com/designmodo"
__newsLicense__ = "Creative Commons (Attribution 3.0 Unported)"

__newBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAABDxJREFUWIXtl11MW3UYxp/3f05ZxGE2YZtx6jQs8tFCjTgX+jFcMmM0
zGUTcJkmYuIYKySLhuiFQSBERRbNdJQiM1k0xESdiZjh1MQoa2FMx8Ja6HbhV9Ao2dBpIAItfV9v
YLalp9CNxJs9l0+f8z6/c/7/03MOcF3/syjVA2wdgWwIOwGVQyKZACBEf5DQBQJ7fa7CH5cdIP/D
4bSbx7mSiWoIkiWgbyAYUZBxAGBQFghmgjwAyEUR5f4ri94LVlhC1wzgcPtLGDgCwg8CdfDUfvPX
IJKEYREq9oxsVcTPA3SnCO/td1m9Vw1g9wTqmPk5jVSVz1VwfDHYmGM7Ats5wm8rUq19roJDKQPY
2/31LLI7YsJD31ZZf02lfF73d567TQvjC6Woq29/4atLBrB7/BUs1JLGZOuttYwlHt+gLE7/wyCW
4ZP3fA40caJUSdvwLbMkp4RQ1+8q+Dj+dxVvbD7iX8egNl1xmXE5YN5yrpGV/E0ik5YtQ01Gud5a
y5hoUk6EdsebZ9csCqCFqImE3/VWW88aDQUAiEiw9xNfwPvpSYHBppxTf3XhGQi6OE1vSAqw6XAw
UxQqTNqK15KWzyFEwSS8/NGicLhFQHsc7f7VhgBp+mwZASd69+WOLwEgJfkO3HtJCX/JpMoMASDY
Bshny10+L4LqEZZt0Z4e0y+wiob6aK+oqDR9Kt30NIEXbKBomZ07GuPaLmaETEcHBo5N/efRoCJ5
0RCAQOtnMDMa7U2na80EPjTi7f7FqHzE290Y7+XbS++Y0KkZQN28p0UwGtJ4fXQuZgkYWDH4W9F0
3KzJZOVGCvYdH4XCP9He2rXBKbC6wRBAAZNFtw5mxEwSujFv664NqQIUOrffRUBM2e/juTdBYSLa
i1kCBv+cLqZsAFf+AzJm9foJbbbS7NyxLr5k/tIvWH8ADBpbGdJfivEUspXIT4YAIJxhIVs0wNwm
8sQXxJcm2gfxEoYNRN9Fe7FLQNQDYOdig65aIjsBjrnNYwAuZ6oegeQ7PMP5y91d3D5kAZAzo8LG
AMEKS4igXhdwwkfntUiR1kJKOzi4776wIQAAXM6kt5hlo83tfzLZQBKsyi8pX5ljezSDBKuSZe3t
w08B2DCjptsWgMUbwQpLSDTsFkVvFLsDNsMziqhXFIee1TV1QIfpZaOcwxOwg6SVQI/Hnz2Q7I3I
438QoC4IV/a5rCeMcslk8/gfIdBRIt7jq7Z+lSiT9J2w2B2wKcIHgHwkaaaG/mfyJpLlrxS/cz4D
oXCzkOzSSC/3VZtPG2UXfSvedDiYadIjrQSUCqQzwvT+6ZqC84mym92BPJ3kCSaqUpDuiK5eGNhr
+TPZ/CV/mDg6h+6WWVXNIo8BMBGpIJFcAgARWgNiMwEzAnWMI9QxUGv5filzU/4yAgCnO3A7K8kl
qCwAEPC4YrrgrSlI+aF1Xf8C/Iqm1j1b4S4AAAAASUVORK5CYII=
"""
__newAuthorLink__ = "https://www.iconfinder.com/DemSt"
__newLicense__ = "Creative Commons (Attribution 3.0 Unported)"

__errorBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAARCQAAEQkBwOWiGAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAMUSURB
VFiFxZc/iBxlGMZ/75dlb0ckKAEPTi5EvRjwjgu7s3MsmxC7dGlSLPEqG7HTRrRJI8TmGkmRysLO
Pk16TWPOmV1ynIIYzsNAYgTRgOjcMfO9Nt8ue5v5u0nIU828f57n4f2+mflGVJUXica8jZ1OZ8kY
swlgrf1mOBw+mIdH5pnAxsbGsrX2e2DJhR4YY3rb29v363KZug2DweCYtfbmlDjAkrX25mAwOPbc
Dezt7X0ItDNSbZerhVpLsL6+/mqz2fwFOJFT8ufh4eHpnZ2dv6py1ppAs9n8vEAc4ISrqYzKE+h0
Ou8YY+5S/uQk1tqzw+Hwpyq8lSdgjPkyQ/wR8PtMrOFqq/FWKfJ9/xJwcSb8SRzHJ4GTwEczuYuu
pxSlS7C2ttZstVo/AitT4ftRFJ1SVQsgIsb3/V+dmTHuxXG8uru7e1jEXzqBhYWFj2fEAcKxOIC7
jmZqVlxvIQoN9Hq9RRG5mpE6nRF7azYgIld7vd7i3AaSJPkCOJ6RWhGRSa+ISI6p444jF7l7oNvt
doAf8kymafrGaDTah8m34bccDQsEYRgOs5JFE7helDfGnBlfJ0nydgGPcVy5yScQBMEV4HwBKSIy
MWCMKTIAcN5xlhvo9/ueqm6VEAJMDIhImQFUdavf73ulBg4ODj4FlssIpyegqqUGgGXHfZRnehO6
zfQz8ITTDPyhqp85M9eA1yv0/GeMOTN9cDnybk/TdEtEqogDvCYiX1esHcNL03QLeG8cmCxBEATn
RCRzo+TglqpeUNULwK2qTSJyJQiCc0cMiIhR1dxHJQP/AJtRFN2Ooug2sOlilaCq18cvMgPQ7Xbf
B/yqBCJyJwzDx+P7MAwfi8idqv2A7zSR1dXVl1ut1j2g8J09g8QY8+Z4M7nNu0e9Y/6jOI5XGp7n
XVbVOuIADWvtd77vfwUgIh/UFAdY9DzvckNVKx0cMnBKRAo/NGVQ1UsGePg0JE+Jh0ZEbgD7L0B8
X0RuiKrSbrdfaTQa76rqWeCl5yz8r4jcTZLk29Fo9Pdc/4bPErV/zZ41/gd+CBcvlyjhCgAAAABJ
RU5ErkJggg==
"""
__errorAuthorLink__ = "https://icons8.com/"
__errorLicense__ = "Free for commercial use (Include link to authors website)"

__pageBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAAVRJREFUWIXtl7FKA0EURc97iamUVGohWOsHCIJisT+wKinyBZJCU0Zi
I1aJtQpCvkAQk8pKUimIP6D4CSqIkDQa39hkkkDAFNnFIHur5c6w98yby8IKA9reLS+pUsUkQJkh
QpnRVrWmmZSuTitP3pfBcFTuFbJRBg+BwAfmVj1E2i+oUgWyzlzdOplC/fzoJcrg3F55FqGmSOhE
joEQQPtoEgDEEQ5weVJ5/f7M7AA4scD7fYDunccR7uXfrej0MMAfKT1qQ6N558YJ2AzW5Lf1yZ/A
qBOMq8mfQNKBpANJB5IOjOrAuBOa/An8+w4kAAlAAtADMKMN3T+YmJQvlucBDGsNAahaEwChtlU4
nIsjvIOrAajTG+/3voRmUkLZUCRMZb7CXPEgUoAOAILB+5TZvvdT/uHx4fZteWW9AbLoxBYEyUQJ
YFhLnFynzfIXZ9Vn7/8AgHV3fyl8XRgAAAAASUVORK5CYII=
"""
__pageAuthorLink__ = "https://www.iconfinder.com/recepkutuk"
__pageLicense__ = "Free for commercial use"

__pagesBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAAuRJREFUWIXtlktIVGEUx3/nzIxlKdEbN7oJktoUFQlR0bSJHmom1CKK
FoWLmlUPRqiIIMegTQ8ohIJoU5QOQqtqIsqIHrQqLNpE0EOLqDTwcc9pMZOYOeNkQUT+4Fvc7/tz
zv+ce7j3gzH+dyTbQWvrwwk2sWcjeCXIAnNKVBGc1y48wEmO6/l0qbCwUD57eGOAVKn5IlRLADB7
4+gjQp4MfRl/qbJy4de8DSRvtG1DLAE6I6d94w0ASknuOq0D13j1yiVnsxqo2RkvVyWBSXRzzZri
dBHBQwnpGTduTta+lwAfLVKmEHXx7YIuyiS47yJNI+nON1/tVrWUmextPtnQPmCgZme8HJV7CpMA
NlevAaA6uiTrKwJIptr8V3QXklfTduET5hXNJxvawwCqJIBJbt5i/QV1wLvBAR63v/DBz/PLZw2b
cESd+wyEJkWqXKQRqNK0JYkCWH9BXcvpQx25qvkdLp9o6Ax6C3YAuFgUIAyAUgyQLXm2ikejazl9
qKM2Vo+iRenUf5lwPqI/NgPD8G904E/OwFD+jQ6MzcD/OwPJG3f2ILLCYC7YtPSuvhf3pwKp77qW
1J29Yh5FdI6JTQVQ6AR94s7NURtA5GgmGIOaVYpIKbBqQIY0opJRDejKgDIRVudKoQBmdAPU7opP
T2/bBzADu4uz38SWhiM2pVd6i3Evx2WrQSsQAAHuSXe2WBDMLuzrKgpHbIqbL3PngBv3zMwxOgE2
xeIzAQzrGuiAqqVA1yE0ra87uCOQyLxxSP/aFYvfDmP6WWadb712uzQoMF+/fPmrIZpu4HZmHb5y
61ZJqC+km2Lxmf14Ewjqej3dPX6+kOTL5eNHBKA2Vu8jaQdj8DESWMXFU4nnP1zJRKTRxaLff5Uj
4WIVWCgk4m35JbYudb0eMtt38VTiOeS4FediQ6z+uMCuH8w4x66cOLL7V2Pl9SEaSrd/2VMkRW7I
VjVzRM9N7XkfH02sMcb4BkYxQckqVwS8AAAAAElFTkSuQmCC
"""
__pagesAuthorLink__ = "https://www.iconfinder.com/recepkutuk"
__pagesLicense__ = "Free for commercial use"

__FullscreenBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAAJxJREFUWIXt1tEKgCAMBdBr+OH9uT30YupM07sVeSEIknacEQNW/h4H
IFgCNsviAOCje6dcOwAv6AADsOPcnXRlER+QEFldBqCGUANICFVACaEOSBEmgBhxAfjyWhqgmLQD
4sLJEY8gKCGqAA3ELYCNaAIwEc0AFqILwEB0A2YjxB+R6mRkPhGtqXjkvNPOPXrXJzow+o1Ua5h3
4ACp+nyuERypSwAAAABJRU5ErkJggg==
"""
__FullscreenAuthorLink__ = "http://www.picol.org/"
__FullscreenLicense__ = " Creative Commons (Attribution-Share Alike 3.0 Unported)"

__nextBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAAZdJREFUWIXdl8FKAzEQhv9MqkKL2IJHW7GIR08+wC4i+CZabFHBxxCK
HgTPPoJHkfoAXjzb1q7W3qRF8FC3STyItoKx2d3Eiv9xdzb/x2QmOwEmLGYStHL6PB8KnHGS5fp2
tmETgEzM+0JdAmwzlKxWOOkVfxUgDHFIoNX3YMozsCubEGMBZCj2pFLXww8ozyTVCsfdJRsARjWw
WO1mMc0viLG14VMVSCa8oJRrOQdwCWEMoIWQaClwr1VOB84BtBDAHePca26l750D2IaIBaCFkGgS
415jJ/1gus7YNtQp2M/18Co2RlsUhKKAqC0fvSyYrhM7Ax/6LhNKoZEakFffzbSdA+ggJFAXjLx2
KfPoHEALIXErFXkPlUzHOUBciNhFaCyC+un1/9iCiRZhEnMgYQ3ozoCpkHwTc8DyURzlAEoEYOs/
EAvApnlkANuzQCQAF+aAYRdoRzHJ/STmgEEGXMyBo0qNC6CZVBXA10mYCz8ozSY2Bwy2oC/kAZS8
+TS3cBeIBNCpzD31JdYlcC4Gwrdp/if0Btya+O75fiGNAAAAAElFTkSuQmCC
"""
__nextAuthorLink__ = "https://www.iconfinder.com/samatodedara"
__nextLicense__ = "Free for commercial use (Include link to authors website)"

__previousBase64__ = """iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAAhBJREFUWIXNlrtuE1EQhv+ZXStSKHIpHXvXXm9oqSLaRIjwFNQIFJAi
XgMpJHHiWLwFJRQ+LoEC5QES4hDowGshr2StzxmqXIyAvdow5Ugz369zmX+AfxxUdMP6/uA2iF6O
RB5+fbrwbaYCvNZgVQQK4DLEHI8M7sWJ4KLgjWbgi0YH4DIAgPjOnMUv4ursIuD+UdCIDHUYvHKZ
M5APGOntuNrcV+AcBh6BFIOrv8A3e9tLwVQF/AluWeP7p4+WB0l6ZH4Dzl6/ToY7eeBAxhNwW/0a
i6UAcvPAAcBKC681Q5eFFIhqV3CD95Y93kwLTy3Aa4cOoBUI9Ql4KcoETyWgcRBWRXQXKA6eWIC/
O6wYMl0QvBvpd2xHD/LAgQS/wN8dVsa2UeBJOFn54UDML6i0hiuWGMWAPw34XwVU94dlJqOYsTot
OFCgGWWN//cKZiUi9gouHt/6Uop4QwQnN9J3dVR647W/L+QVkNgLGgdh1Yie+I5FDKJUZjQNEand
0GuHjmitUNBIzmTHtWboErQCo3YlIqMdZ5oDZ1vzPYG1DkjvuhGtaW2/TfswMw+is635niH9WxHu
Tn8xaZ/8S+lev05sKWZyLnMzW0onRNjczbIZF+IF58+WPglk3cB8vm5Mazxn78TVFmZG508WT0ss
GwbmAgBgzMeopJ8X1T9x+EdBwz388bryKlieOTxL/AQOYhslqY19qwAAAABJRU5ErkJggg==
"""
__previousAuthorLink__ = "https://www.iconfinder.com/samatodedara"
__previousLicense__ = "Free for commercial use (Include link to authors website)"

__playBase64__ = """iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAABxtJREFUeJzt3Ftsm2cZwPH/+7pNaJLRUn1p4ha2JNMGqNS0DS1lZHP8
ZQP1AgETIA0xto7DOO2OC644rYdJ3CExTh2oE5o2bkBsGkxq7KZtDr2YSFohVKSRZknbLCWJ2jWl
JfH7cmG7iRM7sVM7n/19z+/GseNPeaTnn9eO1BqEEEIIIYQQQgghhBBCCCGEEEIIIYQQQviE8nqA
QkWjUWed1gewdj9atxprrbZ2zGo9YK19JZFIvOn1jNWoGgJQXZ2dz6D1IaAh35MsvGGt/W4ikXhr
DWerehUdQHt7+/pNGzceAx7LPHajfhs3Gu4GLHXXx6ibGVt4yXULX47H46+u9azVqpIDUF2uewx4
HODapg9xfvv3uLb5w+nvpm7qZsZoO3+MprE4AMYYo5U60J1IvOjF0NUm5PUA+biu+6SCHwJMhKMM
7T3Mzbqm+WTTt7O172Vi20PcrAvjvHMGjVUo9bm2trYLw8PDQx6NXzUqMoB9+/ZtqAmFXkOphut3
tTK49whGr08tfVEAmceub7yXa5vuZ8ulkyhrFPBZiWBl2usBcqmvr38UrZsA3rr/KYyuKei6yeaP
c3bfs6lYQGHt713XfbJ8k1a/igwAa7sAkrqWyca9RV062bSHs5+Yj0DB7ySC/CozANgGcGvDFqxe
V/TFk017OPuARFCIigzApt+bWL36tyi5IojFYgdKNKJvVGQApTLZtIezn5yPQCv1gkSQzdcBgESw
Et8HADDZvDSCrljsKa/nqgSBCADSEXQs+BNRqaMSQYACAIkgl0AFAKkIhh48KBGkBS4AgKnmjzH0
0EFMqAYCHkEgA4B0BA8+uzCCF1zX/ZrXc621wAYASyJAwdGgRRDoAACmwlkvB4GLIPABAEw1tzMU
XRLB1z0ea01IAGlTze0MdWZF8NsgRCABLJCK4FCgIpAAFplq3s1QLDuCh2Oxb3g8VtlIADksjsAq
9Ru/RiAB5JErgq5Y7Jsej1VyEsAypsK7GXLnI0CpX/stAglgBbcjWFebesBnEUgABUhFcDArAtd1
n/Z2qtKQAAq0OAIFv/JDBBJAEabCuxnsOuSrCCSAIk2HdzH48JIIvuXxWKsmAaxCjgh+Wa0RSACr
NB3exeAj1R+BBHAHpsO7GPzU4awIumKxb3s8VlEkgDs0Hd6ZFQFKPV9NEUgAJTAd3sngp49kR9DZ
+R1vpyqMBFAi0+GPZkeg9S+q4U9ECaCEUhEcxoTWA7ffGH7e47GWJQGU2PTWnQzuP4JJ/bd2peBY
LBa7x+u58pEAymB66y7+Gf1+5u5dWqmfeTnPciSAMhm/7xH+0/JA5u4XKvUUkADKaCTyxcyXSin1
GS9nyUcCKKOrTdvn/22htTs9HicnCaCMrA5xq25z6mulmj0eJycJYI0oY4zXM+QiAZSRMklqb0wC
YLUe93icnCSAMto0fg6dnM3crciPs5cAysVaWv7+Uube3PrZ2b94OU4+EkCZfOAff2LzWOqX3sKL
b5w6ddnjkXIq/mM4xYq2/Psk9/U9D4AxZqJ2bu4HHo+Ul5wAJdZ4oZePxA+isACzIfjS306fvuL1
XPlIACXUONLLju6fokwSY4xR1n7l+IkTPV7PtRwJoETmlz+XWr7WTxxPJP7o9VwrkfcAJdA40suO
ePby4/H4H7yeqxByAtyhal4+yAlwRxpHetmRqN7lg5wAq9b4dvUvHySAVfHL8kFeAormvN3HjhP+
WD7ICVAUZ7SPyImf+Gb5IAEUzI/LB3kJKIgz2kekx3/LBzkBVuTn5YOcAMtyRvuInPTv8kFOgLyc
Mf8vHySAnIKyfJAAlnAu9gdm+SABZEkt/8eBWT7Im8DbnIv9RE4Fa/kgAQDp5Z8O3vJBXgICvXwI
+AngXOwn0hvc5UOATwBZfkogA3AuyfIzAvcS4FzqJ9Iny88I1AngXB6Q5S8SmBMgtfwfyfIXCcQJ
IMvPz/cngHN5gEi/LD8fX58AzrgsfyUVGYCCJKQ+YmW1ZPmFqcgAsHYUoPa/EygzV/TlsvzCVWQA
VqnjACFzC+fKmaKudcYHiAzI8gtVkQEkk8k/W7gMcO/5o+jkrYKua7zUS+SMLL8YIa8HyGVkZGSu
tbX1HQWP1vzvKg3vXuBKcwdWh0Cln7TwVsHWkdfZ/uZzKJuU5RehIgMAGB4ePtfW0vJ+lNpdPzOK
MzHATMM93NywJfUh7OkAGt4d5oPnfk7Lv15GYTHGJFHq8UQi8dLyP0HA/O9RRYpGo+u01ke1Uk9k
Hru5oYmZhrtRGOpmLvKeG/Ofv2iMuaq0fiwej//Vk4GrUEUHkKZc133aGvOc1npjvicZa18zxjzT
09NzYQ1nq3rVEAAAHR0d76upqfkqsN9a26aVMhbGlLUDaP1Kd3f3Oa9nFEIIIYQQQgghhBBCCCGE
EEIIIYQQQgghPPR/3NV8HgFQv9UAAAAASUVORK5CYII=
"""
__playAuthorLink__ = "https://www.iconfinder.com/snipicons"
__playLicense__ = "Creative Commons (Attribution-Noncommercial 3.0 Unported)"

__pauseBase64__ = """iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAABcJJREFUeJzt201sFGUYB/D/885sq9sqljYxdFuLFgJUjRg9kIhZC+VL
IRwUo0RJPMvFxHuvHryYeNB4EtNEbQJGKxpsGFYwagLqQT4k9sNCQZNaiv2A3e68jwdY2mhbSGdm
YXb+v4Rks4fnP2945nlmCAsQERERERERERERERERERERERFRhZByhu3atatqdGTkJRXZKdYuhzHO
YmtZawsQOSUiXYcPH+4FoCFe6q2QjRs3blHVl9XaNcaYqkVXstZXYwZE5MDY2NinJ06cmA7xOhdU
tgZob29/GEC3EVkTQfmDjuu+eujQodEIav9PNpttcB2nC8DmsGsr8KuqvuB53m9h155LWRpgcza7
elrkO2PMUgAoVN2HqZomqCx6AMCxBdSMD8DxrwIAFPjF9/2nc7ncRDhXPbetW7fem8/njxmRRwHA
d9OYuGc5rKQWXVPUR83kOaQKl0tfjVjVdZ7n9YVwyQtyow7o7Ow0x3K5LmPMUmtSOPPIG/gzswkq
JnBtx+bx4Nl9aOn/GAKsdY15C8De4Fc9v0Kh8HbpL39gxSsYbN0N61QHritqsez811h98h2ILTZA
9SMATyHi1bb4W/AWGWO2iTFvAsDZttdx4YHtgIQzeFRcjDY8gfTkMGrHBwCRx1dlMu/9PjQ0FUrA
f3R0dDRa398nIuZi0xacbdsLNSHdQyIYX7ISvnMX6keOQ0SaW1tajvYPDg6EEzC34LfhzQJUtwGA
GhcXm7dFkjHcsqP0MZVPpToiCQGgqpvM9QfX4ebtkWRcaH4Oen0z+yLPRhIyS+QrAEAGAPJVdfBD
GJVzuZJeduOziDRHEgJAVTOl2XWltjGSJ6hiKo3p6jpU5UdhIjxLSeQTQI25lhHCzp83Y1ZtoxpZ
kMyqrRE+P5fOo+WY0FEH0J2tHCtgRlQ3TVn/OWtW5u3IDRknQMJxAgTJ5ASguGMDJFz5VkCFjMwb
KuQ8nAAJxwZIOL4FBMnkCqC4YwMkHFdAkEyuAIo7NkDCcQXcibllPA8nQMJxAgTJ5EMgxR0bIOG4
AoJkcgVQ3LEBEo4rIEgmVwDFHRsg4bgCgmRyBVDcsQESjisgSCZXAMVdZfwwhBNg0TgBEo4NkHB8
CAySyRVAcccGSLjKWAG3A1cAVQI2QMJVxgqotLcA/jCEyoUNkHBcAUEy+RZAcccJECSTE4Dijg2Q
cFwBQTK5Aiju2AAJxxUQx9wQcQIkHBsg4fjfwoNkcgVQ3LEBEo5vAUEyuQIo7jgBgmRyAlDcsQES
jisgSCZXAMUdGyDhKmMF3A5cAVQJ2AAJVxkroNLeAvjbQCoXNkDCcQUEyeRbAMUdGyDhuAKCZHIF
3JxYawFA1EaXMau2An5UOSpyI6hM54nsLCWRN4Aacx4Aqq6MwvGvztw5If5J/zM8kydyLqqziOr5
0ue7xy9GchZ3ehKpqUulvMjOUhL9BBA5CABifTSePhhJRubU56WP077v90YSAsAtFr+x1voA0DST
GarM6R4IFACgIl9GEjKLE3XAnj17+gcHB58VkczS4Z9RSNdhor4VkOC95xSmsPLHD9B45qtrX6i+
6x05sj9w4Xn0DQ1NtC5fvgwiT9b+3Q/Hz+Py/W1QJxW4ttgimk59gZU/vA9RhVU95nleZwiXvXBu
1AEAkM1mV7iO8z2ABgAoVtdiakkTrFn8M6hTzKPm0h8wfgEAoMDxdDqd7enpmQrloueRzWZrHcc5
KsBaALBuNSbrWuA7VYuuaWwR6bFzcAuT176w9q+i6rpcLjcYxjUvpGzPse3t7auMyCcAHgu9uLUH
xHVf6+3tvRx67TmsX7++rtp1P4QxO0Ivbu1PVuRFz/P6Qq89h7K+yGSzWdd13eehuhPAQwjwGqrA
VQAnVbXL87xvQ7vIWycbNmx4RlR3Q6QNQHWAWkUF+gB8Vl9fv7+7uzvyp38iIiIiIiIiIiIiIiIi
IiIiIiKqOP8CwzV6XOcTEiwAAAAASUVORK5CYII=
"""
__pauseAuthorLink__ = "https://www.iconfinder.com/snipicons"
__pauseLicense__ = "Creative Commons (Attribution-Noncommercial 3.0 Unported)"

__player_nextBase64__ = """iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAC0ZJREFUeJztnXtwVNUdxz/nZBN2SXgqaAhKwl6Q4gtBKiK62d2ER6so
PvtPZ2pttVZtta3v8Z/+0c4402k7/Qsdq62CFaW1NSKleRAEwVdbGJKQBHkkSIKW8AoJkOw5/WOz
CZsEstnd7N27nM8fmXvunHPub/L97Ln37uTegMFgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPB
YDAYMgSRzMn8fr9XCPGAgIBSKl9KeQal9iLEJqRcXVFRsSeZx0sWPp9vqkvKH2ghSrRSl0kpu4D9
GrZIKd8sLy+vs7vGkSIpAixYsMCT63b/SsFjUsqswfoopZQU4tXTXV1Pbt68+Ugyjpso8+bNyx43
btwLWqmnpZQ55+qntH5LKfV4dXV1ayrrSwUJC+Dz+S6WQqyTUs4Pzyg4McbLqdGXINUZ8o7vZdSp
//X2V0o1CSlvq6ys3JHosRNh4cKFYzxu97tAILKvfXwRnXlTkKqL3ONNuNv78lZKfYUQK6qqqj6y
o96RIiEBFixY4Bk9evQmAdcDfJXvY/esH9I5Or9vdq0Zd7QWq+4lxrftBEApdUxmZS2rqKjYmlj5
8eHz+VxZWVnrBQQBDufPp/G6hzk5flpfJwFj2hrxbn+Fiw5+Gtl7CiFWVFRUrE991SPDoMt1rMyc
OfM3AlYA7Jl5P/WzH6U7Z0xfBwEIwWnPZFovW4JQ3Yxv24kQwg3cV1hYWL1v377mRGqIB2v69OeF
EA8ANF9xJ7U3PssZ9/joj4OAM56LaC0MorLdTGz5HMCllLrH6/V+tnfv3t2prnskiFuAkpKSGSoU
+rMQQhyaEqDhG4+AEAN+iX3bgiOT5qJcbiZ+/TlAjhAi5RL4fL5LpZRrAVfbJXOpXfgcOlLoYLUL
wbHJV3E6dzKTvtyKECKjJJDxDtSh0ENSSqkR7J71YDj8GNjvvY/dsx+MNPOkEP8sLS6+Kd46hku2
lN8D3AC75zzUF/4QHPQuo+6Gn4MQSClHqVDo74FAYNkIlpoS4hZA9Vw8HZ8wm1PuScMau9+6j8bZ
D0WaeUrK9amSQGsdAOjMy+fEBGtYYw9ay6hb0CtBjlbqXadLELcAaF0A0Om5NK7hTda9AyQoKSlZ
FHc9sROuOzc/rsGZJkH8AvRcP2gR/3Vkk3UvjVf+KNLM00p9MOIS9HxPoQf/uiImBpPA7/d/K2k1
ppBEBEgKTd57aLyqT4JQd3eqVoKE6C8BWv/NiRLYLgBEJHgYACllbqi7e30gELjZ5rKG5KC1jNob
f+FoCdJCAIAm791REmilPnCCBC3W0gESBIuLv213XbGSNgLA4BL4/f5bbC5rSPpLoOCvTpEgrQQA
aLKiJUDrdY6RYOGTjpMg7QQAJ0uwZKAEweCtdtd1PtJSAAhL0HD1j4FeCRxyOlhC7U1P9UkQCq1N
ZwnSVgCAZuuusyUYjdYflBQX+2wua0havIupXfTU2ReGabsSpLUA0CPBNY8AYQlCsM5pEgDZ6SpB
2gsA0Oy9k4ZroyUIBoPF9lY1NE6QwBECQI8EZ60EKhR636kSBAKB2+yuK4JjBABothwqgRUtgYC1
6SKBowSAHgmufRTovTB0xukgTSVwnAAAzdaKXgkAD1qvCwQCfjtrioUWazE1Nz8dJYHf719uZ02O
FADCEtTPeSzS9Ah43wkStFqlURJIId6xUwLHCgBwwHvHAAn8fn/gfGPSgVarlJpbnomSIFhcfLsd
tThaAIAD1h3UX/eTSNMjhShzhATekigJkPJtOyRwvAAAB7y3D5CgpLg4aGdNsdBqhSXo+cNUWyTI
CAGgR4K5fRJoKd9zigS1PvskyBgBYOBKoKV0zEpQWxwlQcquCTJKAIADVpQEbkdJ4H82IoErVRJk
nADQI8Hcn0aabi1lWTAYLLGzplho9QYHSBAIBO4YyWNmpAAAB6zlURKg9XtOlEDA2yMpQcYKAGEJ
ds17PNJ0o/V7wBU2lhQTrVbqJMhoAQC+9N4WLYFDOIcEK5J9nIwXAHokuP6JqH3uk4dsqiZ2BpFg
TbIluCAEAPjSe2uUBJ15U2ysJnZarSA1geeiJCjx++9M1vwXjAAQlqD2hqc5OulqmmbdY3c5MXPI
CkRJoIV4K1m3iBeUAAAthYv5PPg7jkyeY3cpw6K/BArWJOOR+gtOACdzyApQE3yu96+Nu2HNokWL
JiQypxHAYRyyAnwx//sASCmn5OTkvJDIfEYAB7J/znc4cfEMALRSD/p8vrx45zICOBAtJE3X3A2E
n5rKysqK+xtOI4BDaSuY27sttI77itYI4FDOeCagpSvSjO9FTRgBHI4O/xRCxTuDEcChjOpsQ6hQ
pBn3S6yNAA7loqZPere11p/FO48RwIFI1c3l29dEmkeUUpVxz5WckgypxPvxy+QebQJAw++rq6tP
xTuXEcBhTN35LpfveAcADbs6OjpeTGQ+I4CDKKgr44otfwBAKXUcuGvbtm2dicx5wQkwbddbLCz7
LpObP7S7lGFRUFfGrA9/G2meyoLllZWVtYnOe0EJUFTzOtb2l/C0H8TavtLucmKmf/galpdv3Fid
jLldQ3fJDIpqXmf6ztd62572FvuKGQYFdWXM2twXvtL69qqqqn8la/4LQoCimteZXvNa1L7D+fPt
KWYYnCP8Dck8RsafAs4OXyl1zN5qYicV4UOGC9A/fJmVVQrU21pUDKQqfMhgAQYLv6Ki4tPzj7Kf
gl2pCx8yVICiWhN+rGScAEV1b5jwh0FG3QUU1b3B9J2vAg4Mf0vqw4cMEiD8yTfhD5eMOAX0D19I
udiEHxuOXwEGC7+ysvKTIYbZTsGuMmZ9ZG/44PAVoGiXCT9RHCuACT85OFIAx4Zfn17hgwMFKKxf
ZcJPIo4SoLB+Fd6aPwIm/GThmLuAfuEfd1T4W9MzfHCIAIX1q/DWRoVfasJPDml/CjDhjyxpLYBj
w29wRviQxgIUNpjwU0FaCmDCTx1pdxFY2LDaueFvc1b4kGYrQGHjary1rwAm/FSRNitA//A1LK4y
4Y84aSHAYOFv3LjxY5vLGpKChjJmfezc8CENBChsXI23zoRvF4lcA4QAhA4N1e+c2BK+Cr9X5azX
qwybTAkfEhBAwgEAT0d8z9jZ9smXMlx3+8G4hmdS+JDICiBEOcDYo3W4O4b3jiJbl32tywE8J1sZ
2za8h4QKGjMrfEhAAAUvKaVCAs2MXStB65jGFTausvWcL12uPymlOgCs/65E6NjesDa14R8ZFz5A
VrwD9+3bd6SoqGicEOLG3Pb9CDRHJl4b+VeoYaI2Q8yoXUlR4yrAvgu+PXv2nPQWFWmECHpOHiL7
9DHaptwQLvaseiPbAk3RzjeY8e/e9wlkTPiQgAAA06ZN2ySkLBEwdULbDsae2E37WIuunHHhDiL8
C5xweDtX/ufXXNLS+06DIwixpKqqypb7/Ovnz996sqPjm0IIa2xbPeO/3kH7eC9nPBPP6qUZd7iO
2VtfZMoX6wFQSnUIKZdXVlYm7fl8uxFDdzk/ixYtmjAqJ6cMWBjZdzJvGp2j88kKnSL3xD5yzhzt
7a+U2oMQy6uqqmoSPXYilJaW5oZCobUClkT2dYyZSueYAoTqIvfYfkZ1Hu7tr6FFKbXCCbeowyFh
AQCWLl06quv06V8ixBNA9mB9lFIhCS8Ll+uZ8vLytHhO3+fzuVxSPoMQz3Pu/yim0XpVdlfXz9Zv
3vx1KutLBUkRIILf758m4X60DiDlFJQ6raXcC2xyuVxvbtiwoTmZx0sWS26+Ob8rO/t+oVQJUl6m
tO4C9kvY0q3UX6qrq3fbXaPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAxD8H86uBt4
WnYDPgAAAABJRU5ErkJggg==
"""
__player_nextAuthorLink__ = "https://www.iconfinder.com/snipicons"
__player_nextLicense__ = "Creative Commons (Attribution-Noncommercial 3.0 Unported)"

__player_fullscreenBase64__ = """iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEwAACxMBAJqcGAAAFWJJREFUeJztXXtwHEV+/rp3diUvtiVZtmWbYBkiDOcHSDZPY3t3Z2X7
yPlSdTkUuBwhIcld/uGRuBI/4MBgDrC5EB6pS1FFgOSOgjO+HFTOBGNLu1rbdzyMkYxtYVMJwndY
lvWWLcmSdqc7f+yMtFrNc3dmH9F+VVvT0zPTv9/0901P78yve4ACCiiggAIKKKCAAgooYGqB6G0M
BAJLKXAvgFLnPCC9IOTlhoaGLxyzkYOora39hiRJf0OBEqdsMKCfEPJKKBRq0dpHUwCBQGApJeQI
gGmOeJcAxtgFwe2uPnDgQKvTtnIBtbW1V3PGPgUw3WlbjLEhlyDcUF9f/7nadqp1oHzlO04+AFBK
Z7JY7O5M2MoFMMb+HBkgHwAopV4uSX+ruV3nWOeafRVwQkYzaS+bIJxHM2uQPKi1SU8AGQNjrA3A
v2fbj0yBE/IqGDufMYOMvae1SbBcGOdPpuXMZHQXxWKv7zt8uNPmcnMWoVDorM/nq3YB3yecl9la
OCEPq+T9Tmt3awLg/MmGcPhH1r0qIBmRSKQdwLN2lxsMBAQQssXs/jlxCyggeygIYIqjIIApjoIA
pjgKApjiKAhgiqMggCmOggCmOKw/CUwBoiiuIMD3ACzmgMtBU92c8/qioqK39u3bN2JnwT6fr1gQ
hDvBeZADs+wsOwkxwvlpBrwRDoePOWgHgMMC8Pl8010u108JcI+SpxuAYAMIIfeMDA8/FgwG725o
aPjAjjJFUVxDgJ+D80rA+XMAIaDAZlEUX/V6vffv3bt3yClTjglAJv8AAW4BAE4ohqdVgBOnTHJ4
RnohxAZBKb2KSVJYFMVvh0KhA+mUKori7ZyxdwilHgCIuadj1FMKp2RAeAzFQ+0g4CDAXw0ODi7e
uHHjBqdE4JgA5Cv/FgA4u3Ajvrz6XrniHIDMBeEMc9oP4doTz8EdvVgE4C1RFJeFQqGzqRTr8/kW
EWA3odQT9ZTg8+Wb0FVxW5waAOA2+Z8Ez2gfqk6/jPlf7wMlZPXQ0NDzAH7ohC1HOoHyPf8eIE7+
qSV/5wz5BOMXIgE4pehY4EPzTbvACQXiMQ3bUy3e5XI9DmAGpwKabnoGnfNWgxMywaYTDcGopxQt
y/4B5y7foJj5wTqfb5n9lhwSgNzhAycUX159L0BsrqUk4pPTF8quQccCv5J1V11dneWOZ11dnYcA
dQDQvkDExdIqQ7u2ghD87+K/Hqs7RuldNlsA4NzfwMUAMDytwt4r3wwBighKr1GOmtHR0THfqqm+
vr7LIYfEXSxdbMm2XRgpLsdw0WwAACfkWvtKHocjfQAOuAhgX4ePWEwj3plKQAwWwTmXxsuSxjco
drjJdJrg1K0kHeEq9x8EWbnqyPivvOMTAABjrNvv93dYNRuNRtsYYxcAYFbnJxPKtuRPjiO3BZBc
kSYrfd7ZepR1NcVXKX3j8ccfZ1ZNRyKRGKH0FwBQ3nEEFecaUyM/x0WQmwIgUL/ioJKXlC7rbMI3
mv5JKakXQDoxjDuUVmDJp7tQ1nXMOvnJ55Fj0BYAIb0qud3OuaLYVVmaFEFZ9zFc/9HDoCwKAJwD
fxEKhVKOvg2FQmddhNwLAFQaxfUfPoTSnuO6PmS7NeAWedMTwMuK+oF46LY7Gn09PfcMoFVhWi1C
Qrqs+xiu/2AbXNKI4vB9oVDo1+m6VB8O/wqMPQgALmkY1b/dGheBQR9E91wchDsa/VlSyHkvFYR/
09pfUwANDQ1fCG53NTh/lANbCaUrHA3dNqowvWY/mXzOH2hobPxXu1xraGx8EZxvAlREYOaKz6AI
3j906JwgSTWE823g/BFCqe6QO92/FvKBT9juZTKsVlxiBRJg2cc7Esl/qCEc/he7XWwIh58LBgLT
QcgOlzSMZUd+jMPf3K3+F9DK0gG8f+jQOQA7zeyb/U5gquQnXHGDM69MKI980+fz2T7ubtWqVTM4
IRuU9QHFZipXfg79Q8i+AIC0RfDZrTvQX75EKW2t4HK9a6cIVq1aNaO4uPg9AtwGAH3ly3D8pkfz
nnwg2wIgKukUljG3F82rdzkiAjXym297GpLHaw/5WRZC9gSgVxlG/wK0RLDWXhFMIn/2MjSv0SHf
6r+AHGgNcqMFMKowo8qW82KCF80+e0SgSb7ba+hHSueUJWRHAFpNoJWrRaMyY4IXzf70RKBKvm+n
Ovmp+q2XziAyLwCzFWGlotVEEEhNBKrk+3dCEqal5ZPueWaxNchuC6B18lZbAJWyxkQwe6lSgqEI
JpE/ZzmaAymSb+VcktMZRGYFYPaqT84zW+FJ9+SY24vmwE5TIlAn/+lx8vXu9Xq+WDnX5HQGkL1b
gFbaav/AgIAxEczRFsEk8ueqkJ+KDyr+mDrHDCJzAjBz0lavHrMi8HjRpCGCyeRfFyffbTP5euer
l3YYuXUL0ErbIALJ40WTOFEElNKDxcXFB8fIr7gOzeJTU4Z8IFeeA5hJG4nA6D6tiCA4LgJKSA0B
qgGT5JvpCxjl6aWzgMwIQO+E5fWyrmNY8953EXw7CCE6aP7WYDFP8njRVDuhJTBPfip5clqIDmDF
oU0IvhPEH7T+l25dqG5zCA4LIOF9p87JKe/zPSN9AICVBx+EEBuyt1VIyJM8XjSt24n2q2px7g/X
ozn4FCSPCvk2Xe1CdAA1h7fEQ8oAXHPshfHIIr3zA+DYO2MZTgmgGwA8I70gPCEeU+UkJwVzAJje
34rqQ1vsEYGyTNpP8nhxcu02tKzZMk4+VPa3ifyZvaeQiOrfbkVp93HVY8aSXIJnpFdOc0eCcRwR
AOe8HgCE2CDmnD+kuV9ZV1NyJM9DAA4CQElPizMiMNFC2Ef+IGoObcHMHpl8xsIc+BEgRxZ9sBWl
PZ9BCxXtjXBJw0pxaQ1y1YIjAigqKnqLMfYlAFx74jnM7Ds9vlGupHlf16P6w6QwrnD46ZgkfQuK
CLpbUH1wq3kRGBGXTHAizJKvc59PTAvRQdQc3DyBfOp2fzsUCj2ZGF5W88EWVJwNTyqjpLcF15x8
UVk93XvhwtsqXqcNx7oawWDwViZJYUppEScUHfP9uFB2DQiLobzzk7G4fQAcjN2XGMPn8/mmCy7X
uwDWAkD/7KVo9u1EzO0d91qLEDME6oGbWBqkhdFB1DROJv/AgQODY/Xj9z8ASl9Q1ntmr0DPnJXg
VMDM3s8xty0CEi/wEpGktfWRyCcGnqcER/uaoiiuI8Bb0J55vFcO3Z4UvasqAr/DIrCL/LA++QpE
UfwOZ+w1SqnqRyMYYz2E0jtCoVBYw+O04eR0LWhtbf3yyiuvfB3ADBIfMFoExIdrgZBXANwZCoU+
VTv2zJkzowsXLvwlpXQ1gMrioU6UdXyG8wt94C639v1bLa22TN5Pb1+j248MIWp85SeitbX1VFVV
1c8459MIUAV5MCpj7ALh/DW3JN1ZH4kcVzvWLjjaAiSirq7OJY/Sjfn9/g6zw7XUWoKmQMK7ebMi
SOUWYDbNZfJDWzCzW/4whwH5ydi+fTv9MBSqiLrdtLy8vH3Pnj2S8VHpI2MCSAeqIhCTRABYbxWS
YYHwxPUx8rtSIz+bcPQWYBdM3w4A662CyV79BCSs5zP5QJ4IAFAXQWnncXToiSAxbaVV0GsXk8iv
Dm9FSZ6SD+SRAAAVEQx2xEVQmSACQJt0NeKhk1ZblyFEB1Ed2oqSTvmLbHlIPpBnAgA0RNCRIALA
vACShWASQnQQ1Q35Tz6QhwIAxkUAQm4nhMwvHuxA8VAnOivXxHew2gJYxJLDu1DedgQAwBg74nK7
N+Qj+UC24wHSQDQaJYSMTz8mTws3Eckv0nhSOrlXb3SMggRbhFIyOjqaF/+m1JCXLUBCGNdNQPx9
/snVD8UnVDLq9CVCWdd68qcGDnQvuAFl5z9D8VAnCLCAUrp64cKFvzxz5kzeffsw7wSgGsMXfGpy
9C5U0mpLBXoiSBIEp26cr/SNiQBAZb6KIK8EoEq+aAP5ClIRQUd+iyBvBKAaty/K0btA+uQrsCIC
lxvnF+Z3S5AXAtActKFFPpLSZh72qBGt9Tg4YT/ucqNjYf62BJl8GeTp6+u7nHMuRaPRtkgkYmr2
TlXy/Qbk67UAyelEWBXBhNfAQ6hu3IqSrpNKCQdjkvStSCQyYHCKAACfzye43e4F0WiUDgwMnD16
9GhGPjDtuAB8Pt8il8v1OIlPvDz+ujM+CeMOvancVcn3OUS+gnREEB1CdXgLSrpblBIMRbB+/for
pGj0URByF+RPyjPGhiilu12CsH3//v2/N/A4LTgqAFEUbyfAbgAz1LYzxi64CLm3Phz+VfI27SHa
CeQrS7vIV5CuCBrNiSAYDN4Jzl+Gdv30E0rr0v3ohR4c6wOIoriGM/bfhBAvpwLaL6/FuStuR8+c
lQAh8A62gRBSBELuvKqysrf1q68+Uo5VJX+tCfKByduT87TWjR4aKUsDQTDqRscVfpR1HEPxJe0+
QTAY3CSTXwQA3XNvxtnKP0b33FsQ9czEZQNnQAmKOWN/WlVZWf/lmTNfwwE40gL4fL5iweU6BaAy
6ilB003P4OLMhPn2CVDR1oglzbtAJblOON/UEA4/pzonj1nyzfT8070FaC2T8oToEKoPqrcEwUDg
H0HIMwAguYrQUrMNHfPXTHgyOaP/C9R8vBnu6EUA+J9Z5eVL9+zZY3un0pEWoKqq6vsA/hIATl7/
EHrLq+MbEkgbnLkI/bOWY25bBJTHAEI2XLVoERPc7u0TyF8jj88HnCdfQSoiSMpjRL0luGrRokUg
5EkAkIRpaL51F7rn3DipBRotLsewdx7mth8EgFlDly61tLa2njB5BqbhzLsAzoNA/ANLXRW3af6X
7i2/Hs237oTkKo5nELKDJM/G5ZqmemzKV6eZn9pxZpZJ6ZjLi+Y1E2cqASHbgTj5TbfsQl/ZMtVj
wYGOijVj4iec18IBODMwRP6u3qinFDzxslOpqL5ZyyeKADL5q56GJHh1j02ZfMRf6S479ARu2Hc/
3CP9qvvYJoLbdqF/1pgI4uTfugv9s5bqHsuJa+yLK5yQ2XAADr8N1CdfgSKCkeLZ6J57ozb5VgnR
In80/j6/4qtGlHS2YO2e78I1OqS6b1o2ZSgi6Jm7EiPT5sTJLxsfnDqpTibUj7P/1DPy5dAJ4Jj8
Fo7ERXB4w2713jlROY4YLKGShkx+aCtKusY6ZwDnqA5tk58ueif6ppU22RdQEHN50bTqGW1xJZeR
IWQ2HsCoUrXSVpt/jXQy+YyxIxz4BABKO0+gOrwt3hKYKEt3aZRnNp0BZCcgJBXyjQgwaLqF0Xj0
7tiVz1hYcLsDkiQFII9FLO08gZrwVmMRmPFFbZvZdAaReQGYIdxq5RrkxYdrqQ/aiEQiAxMGpHad
RE2jgQis5mlt00tnCNkLCUunqTe6EpPJb9QfsaMqgogFEZhpFcycaxaQO7cAq/d5rWUy+RFzw7VU
RXBwK4SoCRFYWRrlZRjZDQp1nPzNlsbqqYmgOpMiyAKyfwuwq2KTyVeZnMFM6HZGRKAnjAwjv1sA
lXuvMDqAmkOpka9gkgi6T8anq1ETQSpi0MrLArI/LiDVq0olTxiVJ2RKg3wFk0XQoi4CO3zPIrIv
AMB6C+Aw+QomiaCnBdWHbRBBcjqLyA0BAMYVqNPcCqMDqPlNwlRsNo7VUxXBbwxEYOZccgS5IwBg
csWZrMzrPnrUEfIVqIlg+cePpSbaHCIfyDUBANY6VnL6sotnxg+ntMGJgZqRSGQAnIeV9csu/m6S
H6bEkGPQEwARRXFHUBR7An7/+WAgcH/GvLJ4VR2/8bGxeAIC/DgYCPy93S7JYVzbgfj7/OMrH0nt
H0EGEAwGN4mi2BHw+7uDgcAj0HmnrCmAYDD4ZwR4BEAZpXQuCHkxGAz6HfBXHWY7WVwlqISQfw76
/Q/Y5UowGNw0FsMnR/L0z1qq65PqOWQAgUBgPTh/lgBzKKWzQMgOURTv0NpfUwCc87WTMhkL2OSn
OZggX0n3lS1H8y0JIqD0BVEUv5OuC3Lo9rOATP7NcjCHBd8yCQr4VLL9Ovurg3CuFjCa+aFkZipY
TvfNWo7mm58Go574JsZeq62tXZCq6fXr118hh25DchWh+eadE8O4zIogs5jEkQaXAHKxE6gFA/LH
bwfXoaVmMwCAUlrCGHs4VZNSNPoo5EEbLTXb4gGcBvYn+JoHyB8BAMaVL6+fnx9Az+wVAAACfG/7
9u2Wz9Pn8wnycC10z70ZHfPWqNrK9b95RsgvASjQIV9J98xeqexd9mEoVGHVhNvtXgB5rF7P7BWm
bOYjnBJADAAINzUAOHVoEcEBTsfjXaNut+XzjEajY8dw4tK15SQIGxsk7EhlOiIAwvlpACgeaodn
tM8JE5ORRMjM3ngcAGOsv7y8vN1qcQMDA2cZY0MAMLPvlKoNp1E03IXikS5l9XMnbDgiAAa8AQAE
HFWnXwZ4ZtvHkp4WzD0XAQBQ4M1UJl4+evRolFK6GwAq2sKY0feFzV4agE+oOw7gTSfMOCKAcDh8
jAOvAsD8r/dhyfGfoGi42wlTE0C4hHltDag+ug0EHIyxHkGSdqRanksQtjPG+gmXUHNkMyraG0G4
85N4Fw13YelnOzGvrR4AwIGXQqFQi8FhKcGxgSFer/f+wcHBxZSQ1fPPvo/5bfsxXDQ7PpWbI+Dw
jPSOfWMHwCVC6R3vNzaeS7XE/fv3/14UxTrG2K/d0YtFy5qegCRMk4drOTNih7BovNmXW03GeSNj
bJMjxuCgAPbu3Tu0cePGDUNDQ88T4AfgHMXDjnz4Sg2niSTd3WDDZ1ZCodCBWp/Pz4GfA6hyxS5h
WuySDS4agnPgJcbYpkgkMmy8e2pwdGjY3r17hwD8cJ3P9yKj9C5OyLVO2iScdxLgQO+FC2/bOcdO
fSTyYV1d3dLu7u4/IZzXOjVQU0YM8Q7fm041+4nIyNjAA5HICcifS8tXyJMz/EL+/b9Bfj4IKsA2
FAQwxVEQwBRHQQBTHAUBTHEUBDDFURDAFEdBAFMc1h4EEfJwwO+/z04HKKW9YOwniV8PnwoQRfFB
Hn/Gr/rh6JRBiKXyLD8J1PrSdRooAaU/DQaDXzQ0NNTbXHZOIhAI/BEBnic0+w1w9j2QweXZRacC
KCBm1CDXDsjQHhdAiO3z0uqBcO5IxEsughNyKsMmNT9BrykASZJe4sC7zvgzCW/GGHsjQ7ayjv7+
/v/gwH9mxBjn77iLi1/R2mwY1bBu3borMTpq931/3IGiom6nv4qRqwgEApUCY2VOlT8K9EUika+c
Kr+AAgoooIACCiiggAIKyEf8H29sPuhSF8P+AAAAAElFTkSuQmCC
"""
__player_fullscreenAuthorLink__ = "https://www.iconfinder.com/snipicons"
__player_fullscreenLicense__ = "Creative Commons (Attribution-Noncommercial 3.0 Unported)"



def getBellIO():
    dec = base64.b64decode(__bellBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getFriendsIO():
    dec = base64.b64decode(__addFriendsBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getMessageIO():
    dec = base64.b64decode(__newMessageBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getNewsIO():
    dec = base64.b64decode(__newsBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getNewIO():
    dec = base64.b64decode(__newBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getErrorIO():
    dec = base64.b64decode(__errorBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getPageIO():
    dec = base64.b64decode(__pageBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getPagesIO():
    dec = base64.b64decode(__pagesBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getFullscreenIO():
    dec = base64.b64decode(__FullscreenBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getNextIO():
    dec = base64.b64decode(__nextBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def getPriviousIO():
    dec = base64.b64decode(__previousBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)

def get_player_play():
    dec = base64.b64decode(__playBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)

def get_player_pause():
    dec = base64.b64decode(__pauseBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)

def get_player_next():
    dec = base64.b64decode(__player_nextBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)

def get_player_fullscreen():
    dec = base64.b64decode(__player_fullscreenBase64__)
    return Gio.MemoryInputStream.new_from_data(dec)


def __loadFromCache(path: str, newWidth=None, newHeight=None, preserve_aspect_ratio=False):
    try:
        if not os.path.exists(path):
            return None
        if newHeight is None and newWidth is None:
            pixbuf = Pixbuf.new_from_file(path)
        else:
            pixbuf = Pixbuf.new_from_file_at_scale(path, width=newWidth, height=newHeight,
                                                   preserve_aspect_ratio=preserve_aspect_ratio)
        return pixbuf
    except Exception as e:
        return None


def __getFilenameFromURL(url: str):
    alleTeile = url.split("/")
    return alleTeile[len(alleTeile) - 1]


def setImages(cacheDir: str, pxRequest: lib.ProxerRequest.ProxerRequest, args: list, newWidth=None, newHeight=None,
              preserve_aspect_ratio=False, redownload=False):
    if not os.path.exists(cacheDir):
        try:
            os.makedirs(cacheDir)
        except Exception as e:
            print(e)
        redownload = True

    toDownload = []
    downloaded = []
    if redownload:
        toDownload = args
    else:
        for i in args:
            if i.get("URL", None) is not None:
                image = __loadFromCache(cacheDir + os.path.sep + __getFilenameFromURL(i["URL"]), newWidth, newHeight,
                                        preserve_aspect_ratio)
                if image is None:
                    print("Resourcemanager: Downloading " + i["URL"])
                    toDownload.append(i)
                else:
                    i["pixb"] = image
                    print("Resourcemanager: Loading from File: " + i["URL"])
                    downloaded.append(i)

    GObject.idle_add(__buildImage, downloaded, None)

    tmp = __downloadImages(pxRequest, toDownload, newWidth, newHeight, preserve_aspect_ratio)
    options = {}
    for do in tmp["downloaded"]:
        pb = do.get("pixbFile", None)
        if pb is None:
            pb = do.get("pixb", None)
        if pb is not None:
            try:
                if ".png" in do["URL"]:
                    picf = "png"
                elif ".jpg" in do["URL"]:
                    picf = "bmp"
                else:
                    picf = "bmp"
                pb.savev(cacheDir + os.path.sep + __getFilenameFromURL(do["URL"]), picf, [], [])
            except Exception as e:
                print(e)


def __downloadImages(pxRequest: lib.ProxerRequest.ProxerRequest, args: list, newWidth=None, newHeight=None,
                     preserve_aspect_ratio=False):
    import time
    noErr = True
    imageList = args

    errorImages = []
    toSet = []


    for il in imageList:
        try:
            t = pxRequest.request(il["URL"], raw=True)
            il["data"] = t

        except Exception as e:
            errorImages.append(il)
            print(e)

    done = lib.BooleanContainer()
    GObject.idle_add(__buildImage, imageList, done, newHeight, newWidth, preserve_aspect_ratio, errorImages, toSet, True)
    while not done.get_value():
        time.sleep(.25)
    return {"err": errorImages, "downloaded": toSet}


def __buildImage(infos: list, done:lib.BooleanContainer, newHeight=None, newWidth=None, preserve_aspect_ratio=True, errimages=None, doneimages=None, downloaded=False):
    for info in infos:
        try:
            if info.get("pixb", None) is None:
                input_stream = Gio.MemoryInputStream.new_from_data(info["data"], None)
                if newHeight is not None and newWidth is not None:
                    pixbuf = Pixbuf.new_from_stream_at_scale(input_stream, width=newWidth, height=newHeight,
                                                             preserve_aspect_ratio=preserve_aspect_ratio,
                                                             cancellable=None)
                    info["pixbFile"] = Pixbuf.new_from_stream(Gio.MemoryInputStream.new_from_data(info["data"], None), None)
                else:
                    pixbuf = Pixbuf.new_from_stream(input_stream, None)
                info["pixb"] = pixbuf
            pixbuf = info.get("pixb", None)
            image = info.get("Image", None)
            if image is not None:
                image.set_from_pixbuf(pixbuf)
                if doneimages is not None:
                    doneimages.append(info)
            try:
                if info.get("Spinner", None) is not None:
                    info["Spinner"].stop()
                    info.get("Stack", None).remove(info["Spinner"])
                    info.get("Stack", None).show_all()
            except AttributeError as e:
                pass
            except Exception as e:
                print("In __buildImage was a exception thrown!")
                print(e)
        except Exception as e:
            print(e)
            if errimages is not None:
                errimages.append(info)
    if done is not None:
        done.set_value(True)
    """
     il["pixb"], il["Image"], il.get("Stack", None), il.get("Spinner", None)
    img.set_from_pixbuf(image)
    if spinner is not None:
        spinner.stop()
        stack.remove(spinner)
        spinner.destroy()
        stack.show_all()
    """