# -*- coding: utf-8 -*-
import threading
from os.path import expanduser

__author__ = "marcel"

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gdk
from gi.repository import GObject
from gi.repository.GdkPixbuf import Pixbuf
import lib.ProxerSerie
import lib.ProxerManga
import gui.gtkResources

__pxResourceCacheBase__ = expanduser("~/.cache/proxerMe/Mangas/")


class PxReaderGTK(Gtk.ApplicationWindow):
    __gtype_name__ = "PxReader"

    __force_chapter = None
    view_mode_single = True
    page_urls = []
    list_box_tracker = []
    listBoxDict = {}
    scale = {"x": 500, "y": 250}
    __resize_check = False
    __current_chapter = None
    __pxResourceCache = __pxResourceCacheBase__

    def __init__(self, parentWindow, pxreq):
        self.__pxreq = pxreq
        self.dady = parentWindow
        Gtk.ApplicationWindow.__init__(self, title="Manga lesen")
        self.set_border_width(10)
        self.set_default_size(400, 400)
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "Manga lesen"
        self.__headerbar = hb
        self.set_titlebar(hb)

        self.connect("key_press_event", self.on_key_press_event)

        self.__single_page_image = Gtk.Image()
        self.__double_page_image = Gtk.Image()

        pixbuf = Pixbuf.new_from_stream(gui.gtkResources.getPageIO())
        self.__single_page_image.set_from_pixbuf(pixbuf)
        pixbuf2 = Pixbuf.new_from_stream(gui.gtkResources.getPagesIO())
        self.__double_page_image.set_from_pixbuf(pixbuf2)
        self.pageButton = Gtk.Button()
        self.pageButton.set_image(self.__single_page_image)
        self.pageButton.connect("clicked", self.change_view_mode)
        hb.pack_end(self.pageButton)

        self.full_button = Gtk.Button()
        fullbuf = Pixbuf.new_from_stream(gui.gtkResources.getFullscreenIO())
        fullimg = Gtk.Image()
        fullimg.set_from_pixbuf(fullbuf)
        self.full_button.set_image(fullimg)
        self.full_button.connect("clicked", self.toggle_fullscreen)
        self.window_is_fullscreen = False
        hb.pack_end(self.full_button)

        self.prev_button = Gtk.Button()
        prevbuf = Pixbuf.new_from_stream(gui.gtkResources.getPriviousIO())
        previmg = Gtk.Image()
        previmg.set_from_pixbuf(prevbuf)
        self.prev_button.set_image(previmg)
        self.prev_button.connect("clicked", self.prev_page)
        hb.pack_start(self.prev_button)

        self.next_button = Gtk.Button()
        nextbuf = Pixbuf.new_from_stream(gui.gtkResources.getNextIO())
        nextimg = Gtk.Image()
        nextimg.set_from_pixbuf(nextbuf)
        self.next_button.set_image(nextimg)
        self.next_button.connect("clicked", self.next_page)
        hb.pack_start(self.next_button)

        self.__mainBox = Gtk.VBox()
        self.add(self.__mainBox)
        self.__mainBox.set_homogeneous(False)

        self.liste = Gtk.ListBox()
        self.liste.connect("row_selected", self.list_row_selected)
        self.listExpander = Gtk.Expander()
        self.listExpander.set_label("Kapitel")
        scrolling = Gtk.ScrolledWindow()
        viewport = Gtk.Viewport()
        scrolling.add(viewport)
        self.expandedBox = Gtk.HBox()
        self.expandedBox.add(self.liste)
        viewport.add(self.expandedBox)
        self.listExpander.add(scrolling)
        self.listExpander.connect("notify::expanded", self.handleExpanded)
        self.__mainBox.pack_start(self.listExpander, False, False, 0)

        ad1 = Gtk.Adjustment(0, 0, 10, 1, 1, 0)
        self.__page_scale = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=ad1)
        self.__page_scale.set_inverted(True)
        self.slider_handler_id = self.__page_scale.connect("value-changed", self.on_slider_change)
        self.__page_scale.set_digits(0)
        self.__mainBox.pack_start(self.__page_scale, False, False, 0)

        self.__pagesBox = Gtk.HBox()
        # self.__mainBox.pack_start(self.__pagesBox, False, True, 0)
        scbox = Gtk.ScrolledWindow()
        scbox.add(self.__pagesBox)
        self.__mainBox.add(scbox)
        self.__page1 = None
        self.__page2 = None

        self.connect("check_resize", self.on_check_resize)
        self.__pagesBox.set_homogeneous(True)

    def handleExpanded(self, *args):

        if self.listExpander is None:
            print("Expander reference is None")
            return
        if self.listExpander.get_expanded():
            self.__mainBox.set_child_packing(self.listExpander, True, True, 0, Gtk.PackType.START)
        else:
            self.__mainBox.set_child_packing(self.listExpander, False, True, 0, Gtk.PackType.START)

    def change_view_mode(self, *args, onlyupdate=False):
        if not onlyupdate:
            self.view_mode_single = not self.view_mode_single
        self.__page_scale.set_range(0, len(self.page_urls))

    def toggle_fullscreen(self, *args):
        if self.window_is_fullscreen:
            self.unfullscreen()
            self.window_is_fullscreen = False
        else:
            self.fullscreen()
            self.window_is_fullscreen = True

    def prev_page(self, *args):
        seek_time_secs = self.__page_scale.get_value()
        if self.view_mode_single and seek_time_secs < len(self.page_urls):
            self.slider_do(seek_time_secs + 1)
            return True
        elif not self.view_mode_single and seek_time_secs < len(self.page_urls):
            self.slider_do(seek_time_secs + 2)
            return True
            #

    def next_page(self, *args):
        seek_time_secs = self.__page_scale.get_value()
        if self.view_mode_single and seek_time_secs > 0:  # self.view_mode_single and
            self.slider_do(seek_time_secs - 1)
            return True
        elif not self.view_mode_single and seek_time_secs > 1:
            self.slider_do(seek_time_secs - 2)
            return True

    def on_slider_change(self, *args):
        self.change_view_mode(onlyupdate=True)
        curr_page = self.__page_scale.get_value()
        if self.__page1 is not None:
            self.__pagesBox.remove(self.__page1)
            self.__page1.destroy()
        if self.__page2 is not None:
            self.__pagesBox.remove(self.__page2)
            self.__page2.destroy()
        down_list = []
        self.__page1 = Gtk.Image()
        ipage = int(curr_page)
        if not self.page_urls:
            return False
        if ipage > (len(self.page_urls) - 1):
            nextPage = self.manga.get_episode_num(self.__current_chapter)
            if nextPage is None:
                return
            nextPage += 1
            try:
                self.open_manga(self.manga, self.manga.Episodes[nextPage])
            except IndexError as e:
                return
            return

        if not self.view_mode_single:
            self.__page2 = Gtk.Image()
            down_list.append({"Image": self.__page2, "URL": self.page_urls[ipage]})
            ipage += 1
        try:
            down_list.append({"Image": self.__page1, "URL": self.page_urls[ipage]})
            self.__pagesBox.pack_start(self.__page1, False, True, 0)
            if not self.view_mode_single:
                self.__pagesBox.pack_start(self.__page2, False, True, 5)
        except IndexError:
            pass
        self.__pagesBox.show_all()
        self.show_all()
        nw = self.scale.get("x")
        nh = self.scale.get("y")
        t = threading.Thread(target=gui.gtkResources.setImages,
                             args=(self.__pxResourceCache + self.manga.getName() + "_" + str(
                                 self.manga.get_episode_num(self.__current_chapter)), self.__pxreq, down_list, nw, nh,
                                   True))
        t.setName("NewsHeaderDownloaderThread")
        t.setDaemon(True)
        t.start()

    def on_key_press_event(self, widget, event, user_data=None):
        key = Gdk.keyval_name(event.keyval)
        print(key)
        if key == "Escape":
            if self.window_is_fullscreen:
                self.toggle_fullscreen()
            return True
        elif key == "Right":
            self.next_page()
        elif key == "Left":
            self.prev_page()
        return False

    def slider_do(self, page):
        if not self.view_mode_single:
            if page % 2:
                page -= 1
        # self.__page_scale.handler_block(self.slider_handler_id)
        self.__page_scale.set_value(page)
        # self.__page_scale.handler_unblock(self.slider_handler_id)
        self.__page_scale.set_restrict_to_fill_level(False)
        self.__page_scale.set_fill_level(self.__page_scale.get_value())
        self.__page_scale.set_show_fill_level(True)

    def open_manga(self, manga: lib.ProxerSerie.Serie, chapter=None):
        self.manga = manga
        self.__force_chapter = chapter
        self.__initEpList()

    def __threadedInitEpList(self):
        for item in self.list_box_tracker:
            self.liste.remove(item)
            item.destroy()

        self.list_box_tracker = []
        self.listBoxDict = {}

        for lang in self.manga.Episodes[0].get_subtypes().keys():
            button = Gtk.Button(lang)
            button.connect("clicked", lambda x: self.setPreferedSubType(lang))

        seen = self.manga.Gesehen
        for i in range(0, len(self.manga.Episodes)):
            cb = Gtk.CheckButton()
            if seen > i:
                cb.set_active(True)
            else:
                cb.set_active(False)
            cb.connect("toggled", self.set_episode_seen, i)
            lab = Gtk.Label("Episode " + str(i + 1))
            box = Gtk.VBox()
            box.pack_start(cb, False, False, 0)
            box.pack_start(lab, False, False, 5)
            row = Gtk.ListBoxRow()
            row.add(box)
            row.show_all()

            self.list_box_tracker.append(row)
            self.liste.add(row)
            self.listBoxDict[row] = i

        if self.__force_chapter is None:
            try:
                self.liste.select_row(self.list_box_tracker[seen])
            except IndexError:
                try:
                    self.liste.select_row(self.list_box_tracker[seen - 1])
                except:
                    pass
        else:
            num = self.manga.get_episode_num(self.__force_chapter)
            if num is None:
                try:
                    self.liste.select_row(self.list_box_tracker[seen])
                except IndexError:
                    try:
                        self.liste.select_row(self.list_box_tracker[seen - 1])
                    except:
                        pass
            else:
                self.liste.select_row(self.list_box_tracker[num])

        self.show_all()
        self.liste.show_all()

        self.change_view_mode(onlyupdate=True)
        self.slider_do(0)
        self.on_slider_change()
        self.change_view_mode(onlyupdate=True)

    def __threadedLoadStuff(self):
        self.show()
        lab = Gtk.Label("Informationen werden geladen...")
        row = Gtk.ListBoxRow()
        row.add(lab)
        self.list_box_tracker.append(row)
        self.liste.add(row)
        self.set_title("Proxer-Reader (" + self.manga.getName() + ")")
        self.show_all()

    def threaded(self):
        GObject.idle_add(self.__threadedLoadStuff)
        if not self.manga.Episodes:
            self.manga.loadEpisodeList()
        GObject.idle_add(self.__threadedInitEpList)

    def __initEpList(self, serie=None, episode=None, play=False, fullscreen=False):
        t = threading.Thread(target=self.threaded)
        t.setName("DownloadEpListeThread")
        t.setDaemon(True)
        t.start()

    def list_row_selected(self, listbox, row, *args):
        index = self.listBoxDict.get(row)
        if index is None:
            if self.__current_chapter is not None:
                index = self.manga.get_episode_num(self.__current_chapter)
                if index is not None:
                    row = self.list_box_tracker[index]
                    self.liste.select_row(row)
                    return
            print("Index nicht in dictonary!")
            return False
        episode = self.manga.Episodes[index]
        self.__current_chapter = episode
        try:
            self.page_urls = lib.ProxerManga.Chapter.getPictUrls(episode, None)
        except lib.ProxerSerie.EpisodeNotAvaliableException as e:
            self.set_title("Kapitel nicht verfügbar!")
            self.unfullscreen()
            print("No more chapters!")
        # self.page_urls.reverse()
        self.slider_do(0)
        self.change_view_mode(onlyupdate=True)
        self.resize(self.scale["x"], self.scale["y"])

    def set_episode_seen(self, button, epNum, *args):
        var = self.manga.Episodes[epNum].set_viewed()
        button.set_active(var)

    def resizeImage(self, x, y):
        if self.scale.get("x", 200.0) == x and self.scale.get("y", 300.0) == y:
            return
        # if y > 800 or y < 100:
        #     y = 750
        #     x = None
        self.scale = {"x": x, "y": y}
        self.on_slider_change()

    def on_check_resize(self, window):
        if not self.__resize_check:
            self.__resize_check = True
            # print("Checking resize....")

            boxAllocation = self.get_allocation()
            # self.click.set_allocation(boxAllocation)
            self.resizeImage((boxAllocation.width - 20) / 2,
                             boxAllocation.height - 30)
            self.__resize_check = False
