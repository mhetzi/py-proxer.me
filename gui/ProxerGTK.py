# -*- coding: utf-8 -*-
import threading
import time

from gui.ProxerReader import PxReaderGTK

try:
    import json
except:
    import simplejson as json

import traceback
from http.client import NotConnected

import gi

gi.require_version('Gtk', '3.0')

from gui import ProxerUserLists

gi.require_version("Gst", "1.0")
from gi.repository import Gtk, Gdk
from gi.repository import GObject
from gui.ProxerSearchGTK import PxSearchWidget
from gui.ProxerStart import PxStart

__author__ = 'marcel'
__slow_tick__ = True
__no_tick__ = True
__player_new_window__ = False
__silent_retry__ = True

import lib
import lib.ProxerLogin
import lib.ProxerRequest
import lib.ProxerUser
from lib.ProxerSerie import Cache as SerienCache
import libs.RepeatedTimer
import gui.ProxerPlayer
import urllib.error
import keyring
import os
from os.path import expanduser

# from gi.repository import GObject

cookieStorage = expanduser("~/.config/proxerMe.login")
configStorage = expanduser("~/.config/proxerMe.conf")

# GObject.threads_init()

class cfDialog(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Cloudflare", parent, 0)

        self.set_default_size(150, 100)
        box = self.get_content_area()

        self.obox = Gtk.VBox()
        self.spinner = Gtk.Spinner()
        self.label = Gtk.Label.new_with_mnemonic("Cloudflare DDoS Protection! Bitte warte 5 Sekunden...")


class LoginDialog(Gtk.Dialog):
    entPasswort = None
    entName = None
    cbSave = None
    spinner = None
    obox = None

    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Login", parent, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(150, 100)
        box = self.get_content_area()

        self.obox = Gtk.VBox()
        self.spinner = Gtk.Spinner()

        box.add(self.obox)
        box.add(self.spinner)

        lbName = Gtk.Label("Name:")
        self.entName = Gtk.Entry()

        lbPasswort = Gtk.Label("Passwort:")
        self.entPasswort = Gtk.Entry()
        self.entPasswort.set_visibility(False)

        self.cbSave = Gtk.CheckButton("Benutzerdaten speichern")

        self.obox.add(lbName)
        self.obox.add(self.entName)
        self.obox.add(lbPasswort)
        self.obox.add(self.entPasswort)
        self.obox.add(self.cbSave)
        self.show_all()

class HeaderBarWindow(Gtk.ApplicationWindow):
    __lastType = "header"
    __nowType = "none"
    __loginTick = 0
    __isOffline = False
    __player = None
    __reader = None

    pxRequest = None
    loginDialog = None
    cfDialog = None
    pxUser = None
    timer = None
    loginButtonText = None
    tbAnime = None
    tbManga = None
    tbSearch = None
    tbStart = None

    startPage = None
    searchPage = None
    animePage = None
    mangaPage = None
    lastPage = None

    def start_app(self, title="proxerMe", page=None):
        self.connect("delete-event", self.onDeleteWindow)
        self.set_wmclass(title, title)
        self.show_all()
        GObject.threads_init()
        if page is None:
            page = "start"
        if page is not None:
            t = threading.Timer(1.0, GObject.idle_add, (self.setActiveType, page))
            t.setDaemon(True)
            t.start()
        Gtk.main()
        self.save_config()
        print("Exited Gtk.main()")
        exit()

    def save_config(self):
        if self.serien_cache is not None:
            self.serien_cache.save()

        with open(configStorage, 'w') as f:
            json.dump(self.config, f, ensure_ascii=False)
        print("Settings saved!")

    def setActiveType(self, Type):

        self.tbAnime.set_sensitive(False)
        self.tbManga.set_sensitive(False)
        self.tbSearch.set_sensitive(False)
        self.tbStart.set_sensitive(False)

        # if self.__lastTypeThread is not None:
        #    if not self.__lastTypeThread.finished:
        #        return

        # thr = Timer(0.12, self.threadedSetActiveType, Type, None)
        # thr.start()
        # self.__lastTypeThread = thr

        self.threadedSetActiveType(Type)

    def threadedSetActiveType(self, *args, **kwargs):
        # GObject.threads_init()
        Type = "".join(args)

        self.__lastType = self.__nowType
        self.__nowType = Type

        try:
            if self.get_child() is not None:
                if self.lastPage is self.searchPage and self.searchPage is not None:
                    self.__content.remove(self.searchPage)
                elif self.lastPage is self.animePage and self.animePage is not None:
                    self.__content.remove(self.animePage)
                elif self.lastPage is self.mangaPage and self.mangaPage is not None:
                    self.__content.remove(self.mangaPage)
                elif self.lastPage is self.startPage and self.lastPage is not None:
                    self.__content.remove(self.startPage)
        except Exception as e:
            print(e)

        if Type == "anime":  # and not self.tbAnime.get_active():
            self.tbAnime.set_active(True)
            self.tbManga.set_active(False)
            self.tbSearch.set_active(False)
            self.tbStart.set_active(False)
            if self.animePage is None and self.pxUser is not None:
                self.animePage = ProxerUserLists.PxUserListGTK(ProxerUserLists.MODE_ANIME, self.pxUser, self)
                self.animePage.show_all()
                import threading
                t = threading.Thread(target=self.animePage.reopen_pages)
                t.setName("Reopen_Thread")
                t.setDaemon(True)
                t.start()

            if self.animePage is not None:
                self.__content.add(self.animePage)
                self.animePage.show_all()
                self.animePage.update()
            self.lastPage = self.animePage
        elif Type == "manga":  # and not self.tbManga.get_active():
            self.tbAnime.set_active(False)
            self.tbManga.set_active(True)
            self.tbSearch.set_active(False)
            self.tbStart.set_active(False)
            if self.mangaPage is None and self.pxUser is not None:
                self.mangaPage = ProxerUserLists.PxUserListGTK(ProxerUserLists.MODE_MANGA, self.pxUser, self)
                self.mangaPage.show_all()
                import threading
                t = threading.Thread(target=self.mangaPage.reopen_pages)
                t.setName("Reopen_Thread")
                t.setDaemon(True)
                t.start()

            if self.mangaPage is not None:
                self.__content.add(self.mangaPage)
                self.mangaPage.show_all()
                self.mangaPage.update()

            self.lastPage = self.mangaPage
        elif Type == "suche":
            self.tbSearch.set_active(True)
            self.tbAnime.set_active(False)
            self.tbManga.set_active(False)
            self.tbStart.set_active(False)

            if self.searchPage is None:
                spinner = Gtk.Spinner()
                self.__content.add(spinner)
                spinner.start()

                try:
                    Widget = PxSearchWidget(self.pxRequest, self)
                    self.searchPage = Widget
                    self.searchPage.getUserFromMainWindow = self.getUser
                    self.searchPage.openProxerUrl = self.handleProxerUrl
                    import threading
                    t = threading.Thread(target=Widget.reopen_pages)
                    t.setName("Reopen_Thread")
                    t.setDaemon(True)
                    t.start()
                except urllib.error.HTTPError:
                    dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
                                               Gtk.ButtonsType.OK, "Server fehler")
                    dialog.format_secondary_text(
                        "Erweiterte Daten für die Suche konnten nicht heruntergeladen werden!")
                    response = dialog.run()
                    dialog.destroy()
                spinner.stop()
                self.__content.remove(spinner)
                spinner.destroy()

            if self.lastPage != self.searchPage:
                self.__content.add(self.searchPage)
                self.show_all()
                self.lastPage = self.searchPage
        elif Type == "other":
            # self.threadedSetActiveType(self.__lastType)
            pass

        elif Type == "start":  # start is right
            self.tbSearch.set_active(False)
            self.tbAnime.set_active(False)
            self.tbManga.set_active(False)
            self.tbStart.set_active(True)
            if self.startPage is None:
                self.startPage = PxStart(self.pxRequest, self.pxUser, self)

            if self.lastPage != self.startPage and self.startPage is not None:
                self.__content.add(self.startPage)
                self.lastPage = self.startPage
                self.startPage.build_all()
                self.startPage.show_all()
        if not self.tbAnime.get_active() and not self.tbManga.get_active() and not self.tbSearch.get_active() and not self.tbStart.get_active():
            self.threadedSetActiveType("start")
            return

        self.tbAnime.set_sensitive(True)
        self.tbManga.set_sensitive(True)
        self.tbSearch.set_sensitive(True)
        self.tbStart.set_sensitive(True)

        self.show_all()
        self.show_now()

        # self.__lastTypeThread = None

    def login_stuff(self, dryrun=False):
        #todo more threading
        self.loginButtonText.set_text("Anmeldung läuft...")
        login = lib.ProxerLogin.Login(self.pxRequest)
        if not dryrun:
            testlogin = login.checkLogin()
        else:
            testlogin = None

        if not dryrun and not testlogin.loggedIn and not testlogin.ServerErr:
            print("Session abgelaufen!")
            self.pxUser = None
            try:
                keyn = keyring.get_password("proxer", "pxUsername")
                if keyn is None:
                    return False
                keyp = keyring.get_password("proxer", keyn)
                print("Logging in " + keyn + "...")
                login = login.doLogin(keyn, keyp)
                if login.error:
                    return False
                self.pxUser = lib.ProxerUser.User(self.pxRequest)
                print("Session erneuert!")
                self.loginButtonText.set_text(keyn)
                keyn = None
                keyp = None
                if self.timer is not None:
                    self.timer.interval = 1.0
                self.__loginTick = 45
                return True
            except NotConnected:
                self.__isOffline = True
                self.loginButtonText.set_text("Offline")
                return False
            except urllib.error.HTTPError as e:
                self.loginButtonText.set_text("Anmeldung: Server fehler")
                if self.timer is not None:
                    self.timer.interval = 10.0
                return False
        elif testlogin is not None and testlogin.ToManyConnections:
            self.loginButtonText.set_text("Zu viele Verbindungen!")
            if self.timer is not None:
                self.timer.interval = 1.0
            self.__loginTick = 10
            print("Too Many Connections")
            return False
        elif testlogin is not None and testlogin.loggedIn:
            if self.timer is not None:
                self.timer.interval = 1.0
            self.__loginTick = 120
            self.loginButtonText.set_text(keyring.get_password("proxer", "pxUsername"))
            return True
        elif testlogin is not None and testlogin.ServerErr:
            self.loginButtonText.set_text("Server fehler!")
            self.__isOffline = True
            if self.timer is not None:
                self.timer.interval = 1.0
            print("Server fehler!")
            return False
        elif self.loginButtonText.get_text() == "Anmelden" or self.loginButtonText.get_text() == "Anmeldung läuft..." \
                or self.loginButtonText.get_text() == "Verbindungsfehler! Neuer versuch...":
            uname = keyring.get_password("proxer", "pxUsername")
            if uname is not None:
                self.loginButtonText.set_text(uname)


    def refreshLogin(self):
        if self.__loginTick < 1 and not self.__isOffline:
            self.login_stuff()
        elif self.__isOffline:
            try:
                self.pxRequest.request("http://google.com/", None)
                if self.__isOffline:
                    self.loginButtonText.set_text("Anmelden")
                    self.__loginTick = 0
                self.__isOffline = False
            except NotConnected:
                self.__isOffline = True
                self.__isOffline = True
                self.loginButtonText.set_text("Offline")
                self.timer.interval = 1.0
        self.__loginTick -= 1
        if __slow_tick__:
            self.timer.interval = 66.6
            print("Warning SLOW-Login Tick enabled!")

    def __init__(self):
        lib.global_no_login_manager.set_no_login_handler(self.not_loggedin_handler)
        GObject.threads_init()
        self.__loginTick = -1
        self.__fullscreenevents = []
        try:
            with open(configStorage) as data_file:
                self.config = json.load(data_file)
        except Exception as e:
            print("Resetting config!")
            self.config = {}

        self.pxRequest = lib.ProxerRequest.ProxerRequest(cookieStorage, self.onCloudFlareScrape,
                                                         exception_callback=self.retry_dialog_show)
        self.pxRequest.set_fsk_handler(self.on_fsk_event)

        self.serien_cache = SerienCache(self.pxRequest)
        self.serien_cache.use_dict_as_cache(self.config)
        Gtk.ApplicationWindow.__init__(self, title="proxer.me")
        self.set_border_width(10)
        self.set_default_size(400, 820)

        self.window_is_fullscreen = False

        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "proxer.me"
        self.set_titlebar(hb)

        button = Gtk.Button()
        if self.config.get("main.is_logged_in", False):
            lg_button_text = keyring.get_password("proxer", "pxUsername")
            if lg_button_text is None:
                self.config["main.is_logged_in"] = False
        else:
            lg_button_text = "Anmelden"
        self.loginButtonText = Gtk.Label(lg_button_text)
        button.add(self.loginButtonText)
        # icon = Gio.ThemedIcon(name="security-high")
        # image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        # button.add(image)
        button.connect("clicked", self.userButtonHandler)
        hb.pack_end(button)

        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class(box.get_style_context(), "linked")

        self.tbStart = Gtk.ToggleButton("Start")
        self.tbStart.connect("toggled",
                             lambda b: self.setActiveType("start") if self.tbStart.get_active() else self.setActiveType(
                                 "other"))
        box.add(self.tbStart)

        self.tbAnime = Gtk.ToggleButton("Anime")
        self.tbAnime.connect("toggled",
                             lambda b: self.setActiveType("anime") if self.tbAnime.get_active() else self.setActiveType(
                                 "other"))
        box.add(self.tbAnime)

        self.tbManga = Gtk.ToggleButton("Mangas")
        # icon = Gio.ThemedIcon(name="accessories-dictionary")
        # image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        # button.add(image)
        self.tbManga.connect("toggled",
                             lambda b: self.setActiveType("manga") if self.tbManga.get_active() else self.setActiveType(
                                 "other"))
        box.add(self.tbManga)

        self.tbSearch = Gtk.ToggleButton("Suche")
        self.tbSearch.connect("toggled", lambda b: self.setActiveType(
            "suche") if self.tbSearch.get_active() else self.setActiveType("other"))
        box.add(self.tbSearch)

        hb.pack_start(box)

        self.__content = Gtk.HBox()
        # self.__player = gui.ProxerPlayer.PxPlayerGTK(self)
        # self.__content.add(self.__player)
        self.add(self.__content)

        if not __no_tick__:
            self.timer = libs.RepeatedTimer.RepeatedTimer(1.0, self.refreshLogin)
        else:
            self.timer = None
        # self.setActiveType("start")
        self.__nowType = "start"

        self.pxUser = lib.ProxerUser.User(self.pxRequest)

    def getUser(self):
        return self.pxUser

    def handleProxerUrl(self, url):
        pass

    def userButtonHandler(self, button):

        if self.__isOffline:
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
                                       Gtk.ButtonsType.OK, "Offline!")
            dialog.format_secondary_text(
                "Anwendung wird im offline Modus verwendet!\nPrüfen Sie ihre Internet verbindung!")
            response = dialog.run()
            if response == Gtk.ResponseType.YES:
                print("Abmelden...")
                os.remove(cookieStorage)
                self.pxUser = None
                self.loginButtonText.set_text("Anmelden")
                self.pxRequest = lib.ProxerRequest.ProxerRequest(cookieStorage, self.onCloudFlareScrape, exception_callback=self.retry_dialog_show)
            elif response == Gtk.ResponseType.NO:
                pass
            dialog.destroy()
            return

        if not lib.ProxerLogin.Login(self.pxRequest).checkLogin().loggedIn:
            self.loginDialog = LoginDialog(self)
            resp = self.loginDialog.run()
            if resp == Gtk.ResponseType.OK:
                self.loginDialog.spinner.start()
                print("Logging in...")
                login = lib.ProxerLogin.Login(self.pxRequest)
                uname = self.loginDialog.entName.get_text()
                userpasswort = self.loginDialog.entPasswort.get_text()

                ldata = login.doLogin(uname, userpasswort)

                if ldata.Error:
                    print("Userdaten nicht korrekt!")
                else:
                    print("Angemeldet!")
                    self.loginButtonText.set_text(uname)

                if self.loginDialog.cbSave.get_active():
                    keyring.set_password("proxer", "pxUsername", uname)
                    keyring.set_password("proxer", uname, userpasswort)
            elif resp == Gtk.ResponseType.CANCEL:
                print("Log in abgebrochen")

            self.loginDialog.destroy()
            self.loginDialog = None
        else:
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION,
                                       Gtk.ButtonsType.YES_NO, "Abmelden?")
            dialog.format_secondary_text(
                "Möchtest du dich wirklich abmelden?")
            response = dialog.run()
            if response == Gtk.ResponseType.YES:
                print("Abmelden...")
                os.remove(cookieStorage)
                self.loginButtonText.set_text("Anmelden")
                self.pxRequest.load()
                uname = keyring.get_password("proxer", "pxUsername")
                keyring.delete_password("proxer", "pxUsername")
                keyring.delete_password("proxer", uname)
            elif response == Gtk.ResponseType.NO:
                pass
            dialog.destroy()

    def onDeleteWindow(self, *args):
        print("Beende app...")
        self.destroy()

    def onCloudFlareScrape(self, done):
        GObject.idle_add(self.onCloudFlareScrapeThreaded, done)
        if not done:
            i = 20
            print("Cloudflare protection! Bitte warten...")
            while self.cfDialog is None:
                time.sleep(.25)
                i -= 1
                if i < 0:
                    return
            t = threading.Thread(target=self.cfDialog.run)
            t.setName("CloudflareScrapeDialog")
            t.setDaemon(True)
            t.start()
        else:
            print("Cloudflare scraped!")

    def onCloudFlareScrapeThreaded(self, done):
        if done:
            if self.cfDialog is not None:
                self.cfDialog.destroy()
                self.cfDialog = None
        else:
            if self.cfDialog is None:
                d = cfDialog(self)
                self.cfDialog = d

    def on_fsk_event(self):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION,
                                   Gtk.ButtonsType.YES_NO, "18+")
        dialog.format_secondary_text(
            "Diese Inhalte sind nicht Jugendfrei!\nMöchtest du wirklich fortfahren?")
        response = dialog.run()
        if response == Gtk.ResponseType.YES:
            self.pxUser.setAdultMode(True)
            dialog.destroy()
            return True
        elif response == Gtk.ResponseType.NO:
            dialog.destroy()
            return False

    __fullscreenChanged = False

    def on_window_state_change(self, window, event):
        print(window)
        print(event)
        test = Gdk.WindowState
        self.window_is_fullscreen = bool(
            test.FULLSCREEN & event.new_window_state)

        if self.window_is_fullscreen is not self.__fullscreenChanged:
            self.__fullscreenChanged = self.window_is_fullscreen
            for ev in self.__fullscreenevents:
                try:
                    ev(self.window_is_fullscreen)
                except Exception as e:
                    print("Catched Exception on executing a Fullscreen hook!")
                    traceback.print_exc()

    def toggleFullscreen(self, event):
        doIt = None
        try:
            if event.keyval:
                doIt = True
        except:
            pass
        try:
            if event.get_label() == "Fullscreen" or event.get_label() == "Vollbild":
                doIt = True
        except:
            pass
        if doIt is not None and doIt:
            if self.window_is_fullscreen:
                self.unfullscreen()
            else:
                self.fullscreen()

    def addFullscreenHandler(self, func):
        self.__fullscreenevents.append(func)

    def push_to_player(self, serie, episode=None, play=False, fullscreen=False):
        if self.__player is not None:
            self.__player.initEpList(serie, episode, play, fullscreen)
        else:
            self.__player = gui.ProxerPlayer.PxPlayerGTK(self, True)
            self.__player.initEpList(serie, episode, play, fullscreen)

    def playerDestroy(self, *args, **kwargs):
        if self.__player is not None:
            self.__player.destroy()
            self.__player = None
        return True

    def push_to_reader(self, serie, chapter=None):
        if self.__reader is None:
            self.__reader = PxReaderGTK(self, self.pxRequest)

        self.__reader.open_manga(serie, chapter)

    def not_loggedin_handler(self):
        print("Not logged in! Try to loggin...")
        val = self.login_stuff()
        if val is not None:
            self.config["main.is_logged_in"] = val
        if lib.ProxerLogin.Login(self.pxRequest).checkLogin():
            return True
        return False

    def suche(self, text):
        self.setActiveType("suche")
        if self.searchPage is not None:
            self.searchPage.do_search(text)

    def retry_dialog_show(self, exception):
        if __silent_retry__:
            GObject.idle_add(self.loginButtonText.set_text, "Verbindungsfehler! Neuer versuch...")
            from threading import Timer
            time.sleep(5.0)
            timeer = Timer(1.5, self.__reset_login_button)
            timeer.setName("LoginButtonResetter")
            timeer.setDaemon(True)
            timeer.start()
            return True
        else:
            boolc = lib.ClassContainer(None)
            GObject.idle_add(self.__retry_dialog_show_glib, exception, boolc)
            while boolc.get_value() is None:
                time.sleep(.25)
            self.__retry_dialog_show(boolc.get_value())

    def __retry_dialog_show_glib(self, exception, bc: lib.ClassContainer):
        self.onCloudFlareScrape(True)
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
                                   Gtk.ButtonsType.OK_CANCEL, "Server fehler")
        dialog.format_secondary_text(
            "Fehler bei der Verbindung mit dem Server (" + str(exception) + ")\n Wiederholen?")
        bc.set_value(dialog)

    def __retry_dialog_show(self, dialog):

        response = dialog.run()
        GObject.idle_add(dialog.destroy)

        if response == Gtk.ResponseType.OK:
            return True
        return False

    def __reset_login_button(self):
        try:
            text, r = self.pxRequest.request("http://google.com/", None, retry=False)
            if r.status_code != 200:
                print("Test server status code: " + r.status_code)
                return
        except:
            print("Offline!")
            return
        self.login_stuff(True)

    def destroy(self):
        t = threading.Thread(target=self.__destroy)
        t.setName("WindowDestroyer")
        t.setDaemon(True)
        t.start()

    def __destroy(self):
        if self.__player is not None:
            self.__player.destroy(True)
        bc = lib.BooleanContainer(False)
        GObject.idle_add(self.__destroy_1, bc)
        while not bc.get_value():
            time.sleep(.11)
        Gtk.main_quit()
        if self.timer is not None:
            self.timer.stop()

    def __destroy_1(self, bc: lib.BooleanContainer):
        self.player = None
        super(HeaderBarWindow, self).destroy()
        bc.set_value(True)

    def get_player(self):
        return self.__player

    def __serie_to_anime(self, serie, do_open=True):
        if self.animePage is None or do_open:
            lib.execute_gobject_idle_add_blocking(self.setActiveType, "anime")
        val = lib.execute_gobject_idle_add_blocking(self.animePage.select_serie, serie, no_select=True)
        if not val:
            lib.execute_gobject_idle_add_blocking(self.animePage.reopen_pages_idle, None, serie)
        if do_open:
            lib.execute_gobject_idle_add_blocking(self.animePage.select_serie, serie)

    def serie_to(self, serie, do_open=True, thread_running=False):
        if not thread_running:
            t = threading.Thread(target=self.serie_to, args=(serie, do_open, True))
            t.setName("serie_pusher")
            t.setDaemon(True)
            t.start()
        else:
            if serie.is_anime:
                self.__serie_to_anime(serie, do_open)
            else:
                self.__serie_to_manga(serie, do_open)

    def __serie_to_manga(self, serie, do_open=True):
        if self.mangaPage is None or do_open:
            lib.execute_gobject_idle_add_blocking(self.setActiveType, "manga")
        val = lib.execute_gobject_idle_add_blocking(self.mangaPage.select_serie, serie, no_select=True)
        if not val:
            lib.execute_gobject_idle_add_blocking(self.mangaPage.reopen_pages_idle, None, serie)
        if do_open:
            lib.execute_gobject_idle_add_blocking(self.mangaPage.select_serie, serie)
if __name__ == "__main__":
    win = HeaderBarWindow()
    win.start_app()
