# -*- coding: utf-8 -*-
import weakref

import re

__author__ = 'marcel'
import os
import threading
import time
import gi

gi.require_version("Gst", "1.0")
gi.require_version('GstVideo', '1.0')
gi.require_version('GdkX11', '3.0')

from gi.repository import Gtk, Gst, Gdk
from gi.repository import GObject
from gi.repository.GdkPixbuf import Pixbuf
from gi.repository import GstVideo, GdkX11
from libs.RepeatedTimer import RepeatedTimer
import lib.ProxerSerie
import lib.ProxerVideo
import libs.Kodi as kodi
import lib
try:
    import libs.dbus as dbus
    __no_dbus__ = False
except ImportError as ie:
    __no_dbus__ = True
import urllib.parse
import gui.gtkResources as res

__no_motion_check__ = False
__timer_tick__ = 0.5

class HostnameDialog(Gtk.Dialog):
    entName = None
    obox = None

    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Hostname/IP vom KODI", parent, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(150, 100)
        box = self.get_content_area()

        self.obox = Gtk.VBox()
        box.add(self.obox)

        lbName = Gtk.Label("Hostname:")
        self.entName = Gtk.Entry()

        self.obox.add(lbName)
        self.obox.add(self.entName)
        self.show_all()


class episodes_dialog(Gtk.Dialog):
    def __init__(self, parent, scrolling: Gtk.ScrolledWindow, on_hidden):
        Gtk.Dialog._init(self, "Episoden", parent, 0)
        self.__scrolling = scrolling
        self.set_default_size(150, 100)
        box = self.get_content_area()
        box.pack_start(scrolling, False, False, 0)
        self.__scrolling.show_all()
        self.connect("delete_event", self.hidee)
        self.on_hidded = on_hidden
        self.show_all()

    def hidee(self, *args, **kwargs):
        self.hide()
        if callable(self.on_hidded):
            self.on_hidded()
        self.get_content_area().remove(self.__scrolling)
        self.destroy()
        return True


class PxPlayerGTK(Gtk.Window):
    preferedSubType = ""
    listBoxTracker = []
    listBoxDict = {}
    __notes_listbox_traker = []
    __mySerie = None
    __forceEpisode = None
    __played_once = False
    __force_play = False
    playing = False
    haveDuration = False
    slider_handler_id = None
    __force_fullscreen = False
    __gtype_name__ = "PxPlayer"
    __kodi_backend = None
    __next_pending = False
    __plid = None
    __force_position_secs = -1
    __current_position = 0
    __load_stuff_done = False
    __ignore_filter = None
    __last_url = None
    __restart = False
    __buffering = -1
    __duration = 0
    __player_state = Gst.State.NULL
    __current_episode = None
    __playbutton_status_tracker = ""
    __notes_button_visible = False
    __playing_progess_show_override = False
    __download_percent = -1
    __notifications = {}
    __qos = None
    __last_cursor = None

    def create_player(self, onmessage, onSyncMessage):
        player = Gst.ElementFactory.make("playbin", "player")
        if player is None:
            player = Gst.ElementFactory.make("playbin2", "player")
            if player is None:
                player = Gst.ElementFactory.make("playbin", None)
                if player is None:
                    player = Gst.ElementFactory.make("playbin2", None)

        bus = player.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect("message", onmessage)
        bus.connect("sync-message::element", onSyncMessage)
        return player

    def __init__(self, parentWindow, reuse_window=False):
        super(PxPlayerGTK, self).__init__(Gtk.WindowType.TOPLEVEL)
        GObject.threads_init()
        Gst.init(None)
        self.dady = parentWindow
        self.__reuse = reuse_window
        self._having_next = False
        self.set_double_buffered(True)
        self.set_default_size(800, 400)

        if not __no_motion_check__:
            self.timer = RepeatedTimer(__timer_tick__, weakref.WeakMethod(self.motion_check))
        else:
            self.timer = None
            print("Video motion check Disabled, controls wont hide!")

        self.set_events(Gdk.EventMask.POINTER_MOTION_MASK
                        | Gdk.EventMask.LEAVE_NOTIFY_MASK | Gdk.EventMask.ENTER_NOTIFY_MASK
                        | Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK
                        | Gdk.EventMask.SCROLL_MASK
                        | Gdk.EventMask.KEY_PRESS_MASK)
        self.connect("motion-notify-event", self.motion_detect)
        self.connect("key_press_event", self.on_key_press_event)
        self.__last_playbutton_reference = None

        playimgpxb = Pixbuf.new_from_stream_at_scale(res.get_player_play(), width=24, height=24,
                                                     preserve_aspect_ratio=True, cancellable=None)
        self.__playimage = Gtk.Image.new_from_pixbuf(playimgpxb)

        pixbuf_pause = Pixbuf.new_from_stream_at_scale(res.get_player_pause(), width=24, height=24,
                                                       preserve_aspect_ratio=True, cancellable=None)
        self.__icon_pause = Gtk.Image.new_from_pixbuf(pixbuf_pause)

        nextimgpxb = Pixbuf.new_from_stream_at_scale(res.get_player_next(), width=24, height=24,
                                                     preserve_aspect_ratio=True, cancellable=None)
        self.__nextimage = Gtk.Image.new_from_pixbuf(nextimgpxb)

        fullimgpxb = Pixbuf.new_from_stream_at_scale(res.get_player_fullscreen(), width=24, height=24,
                                                     preserve_aspect_ratio=True, cancellable=None)
        self.__fullimage = Gtk.Image.new_from_pixbuf(fullimgpxb)

        self.set_title("Video-Player")
        self.set_default_size(500, 400)
        self.connect("delete_event", parentWindow.playerDestroy)
        vbox = Gtk.VBox()
        overlay = Gtk.Overlay()
        overlay.add_overlay(vbox)
        overlay.set_events(Gdk.EventMask.POINTER_MOTION_MASK)
        overlay.connect("motion-notify-event", self.motion_detect)
        vbox.set_halign(Gtk.Align.CENTER)
        vbox.set_valign(self.get_osd_anchor_position_in_overlay("control"))
        # vbox.set_app_paintable(True)
        # screen = self.get_screen()
        # vbox.set_visual(screen.get_rgba_visual())
        # vbox.set_halign(Gtk.Align.END)
        # vbox.set_valign(Gtk.Align.END)
        self.add(overlay)
        hbox = Gtk.HBox()
        self.extraBox = vbox
        cont_box = Gtk.VBox()
        cont_box.set_homogeneous(False)
        cont_box.pack_start(hbox, False, False, 0)
        vbox.pack_start(cont_box, True, True, 0)

        self.button = Gtk.Button()  # ("Start")
        self.button.add(self.__playimage)
        self.__last_playbutton_reference = self.__playimage
        hbox.pack_start(self.button, False, False, 0)
        self.button.connect("clicked", self.start_stop)
        self.full = Gtk.Button()  # ("Fullscreen")
        self.full.connect("clicked", self.toggle)
        self.full.add(self.__fullimage)

        self.nextButton = Gtk.Button()  # ("Nächste Episode")
        next_button_box = Gtk.VBox()
        next_button_box.pack_start(self.__nextimage, False, False, 0)
        self.__next_spinner = Gtk.Spinner()
        next_button_box.pack_start(self.__next_spinner, False, False, 0)
        self.__next_spinner.hide()
        self.nextButton.add(next_button_box)
        self.nextButton.connect("clicked", self.nextVideo)
        hbox.pack_start(self.nextButton, False, False, 0)
        hbox.pack_start(self.full, False, False, 0)

        self.__notes_button = Gtk.Button("Notes")
        self.__notes_button.connect("clicked", self.__notes_button_handler)
        hbox.pack_start(self.__notes_button, False, False, 0)

        self.next = Gtk.CheckButton("Automatisch fortfahren")
        hbox.pack_start(self.next, False, False, 0)
        self.next.connect("toggled", self.next_episode_checkbox_handler)
        doPlayNext = self.__get_player_config().get("continueNextEpisode", False)

        self.__host_picker = Gtk.ComboBoxText()
        self.__host_picker.set_entry_text_column(0)
        self.__host_picker.connect("changed", self.on_host_combo_changed)
        self.__host_picker.show_all()
        host = self.__get_player_config().get("activeHost", None)
        hostlist = self.__get_player_config().get("hosts", None)

        if host is None:
            host = "lokal"
        if hostlist is None:
            hostlist = ["lokal", "Neues Kodi hinzufügen"]
            self.__get_player_config()["hosts"] = hostlist

        for i in range(0, len(hostlist)):
            self.__host_picker.append_text(hostlist[i])
            if host == hostlist[i]:
                host = i

        if doPlayNext is None:
            doPlayNext = False

        self.next.set_active(doPlayNext)

        self.status = Gtk.Label("")
        overlay.add_overlay(self.status)
        self.status.set_halign(Gtk.Align.CENTER)
        self.status.set_valign(self.get_osd_anchor_position_in_overlay("state"))

        # vbox.pack_start(self.status, False, False, 0)
        hbox.pack_start(self.__host_picker, False, False, 0)

        ad1 = Gtk.Adjustment(0, 0, 10, 1, 1, 0)
        self.play_progress = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=ad1)
        self.play_progress.set_draw_value(False)
        cont_box.pack_start(self.play_progress, True, True, 0)
        self.slider_handler_id = self.play_progress.connect("value-changed", self.on_slider_seek)

        self.movie_window = Gtk.DrawingArea()
        self.movie_window.set_events(Gdk.EventMask.POINTER_MOTION_MASK)
        self.movie_window.connect("motion-notify-event", self.motion_detect)

        self.show_all()
        self.player = self.create_player(self.on_message, self.on_sync_message)
        self.window_is_fullscreen = False
        self.connect_object('window-state-event',
                            self.on_window_state_change,
                            self)

        self.liste = Gtk.ListBox()
        self.list_row_handler_id = self.liste.connect("row-selected", self.handle_liste_selected)
        self.listExpander = Gtk.Expander()
        self.listExpander.set_label("Episoden")

        self.scrolled = Gtk.ScrolledWindow()
        viewport = Gtk.Viewport()
        self.scrolled.add(viewport)
        self.__lang_buttons = Gtk.ButtonBox()
        self.expandedBox = Gtk.VBox()
        self.expandedBox.set_homogeneous(False)
        self.expandedBox.pack_start(self.__lang_buttons, False, False, 0)
        self.expandedBox.add(self.liste)
        viewport.add(self.expandedBox)

        self.listExpander.connect("notify::expanded", self.handleExpanded)

        vbox.pack_start(self.listExpander, False, False, 0)
        vbox.set_homogeneous(False)
        overlay.add(self.movie_window)
        self.mainBox = vbox
        self.__listbox_container = Gtk.Frame()
        self.__listbox_container.set_label("Episoden:")
        self.__listbox_container.set_valign(Gtk.Align.CENTER)
        self.__listbox_container.set_halign(Gtk.Align.FILL)
        self.__listbox_container.set_size_request(-1, 300)
        listbox_vbox = Gtk.VBox()
        self.__listbox_container.add(listbox_vbox)
        listbox_vbox.pack_start(self.scrolled, True, True, 0)
        overlay.add_overlay(self.__listbox_container)

        self.__listbox_container.show_all()
        listbox_vbox.show_all()
        self.scrolled.show_all()
        self.liste.show_all()

        self.scrolled_notes = Gtk.ScrolledWindow()
        viewport_notes = Gtk.Viewport()
        self.scrolled_notes.add(viewport_notes)

        self.__notes_liste_frame = Gtk.Frame()
        self.__notes_liste_frame.set_label("Notizen:")
        self.__notes_liste_frame.set_valign(Gtk.Align.CENTER)
        self.__notes_liste_frame.set_halign(Gtk.Align.FILL)
        self.__notes_liste_frame.set_size_request(-1, 300)
        self.__notes_liste_frame.add(self.scrolled_notes)
        overlay.add_overlay(self.__notes_liste_frame)

        self.__notes_liste = Gtk.ListBox()
        viewport_notes.add(self.__notes_liste)
        self.__notes_liste_frame.show_all()
        self.scrolled_notes.show_all()
        self.__notes_liste.show_all()

        self.show_all()
        self.hide()
        self.set_host(host)
        self._connected = {}
        if __no_dbus__:
            self.bus = dbus.MPRIS(self, "proxerMe")
        self.__eps_dialog = None
        self.__listbox_container.hide()

    def select_listbox_row(self, row):
        self.liste.handler_block(self.list_row_handler_id)
        self.liste.select_row(row)
        self.liste.handler_unblock(self.list_row_handler_id)

    def handle_liste_selected(self, liste, list_row):
        self.start_stop("start")

    def handleExpanded(self, *args):

        if self.listExpander is None:
            print("Expander reference is None")
            return
        if self.listExpander.get_expanded():
            self.mainBox.set_child_packing(self.listExpander, True, True, 0, Gtk.PackType.START)
            self.__listbox_container.show_all()
            self.__notes_liste_frame.hide()
        else:
            self.mainBox.set_child_packing(self.listExpander, False, True, 0, Gtk.PackType.START)
            self.__listbox_container.hide()

    def start_stop(self, w, no_parent=False, delay=False, keep_url=False, done=None):
        if w == "restart":
            self.__force_position_secs = self.__current_position
            self.start_stop("stop")
            w = "start"
            self.__restart = True
        if w == "stop":
            if self.player is not None:
                self.player.set_state(Gst.State.NULL)
                self.player = None
            self.set_playbutton_image("play")
            self.player = None
            self.__player_state = Gst.State.NULL
            if self.timer is not None:
                self.timer.stop()
            self.__plid = None
            if done is not None:
                if callable(done):
                    done()
                elif not done.get_value():
                    done.set_value(True)
        elif (w == "start" or self.__last_playbutton_reference is self.__playimage) and w != "stop":
            if self.__player_state == Gst.State.PAUSED:
                self.play_pause()
            else:
                if self.timer is not None:
                    self.timer.start()
                row = self.liste.get_selected_row()
                if row is None:
                    return
                index = self.listBoxDict[row]
                self.set_title("Episode " + str(index + 1) + " von " + self.__mySerie.getName())

                if self.__played_once and not no_parent and not self.__reuse:
                    self.dady.playerDestroy()
                    Episode = self.__mySerie.Episodes[int(index)]
                    self.dady.push_to_player(self.__mySerie, Episode, True, self.window_is_fullscreen)
                    return

                if not self.__played_once:
                    self.handleExpanded()

                self.__played_once = True

                t = threading.Thread(target=self.threaded_start_stop,
                                     args=(str(index), delay, self.__last_url if keep_url else None, done))
                t.setName("LinkFetcherThread")
                t.setDaemon(True)
                t.start()
        elif self.__last_playbutton_reference is self.__icon_pause or w == "pause":
            self.play_pause()

    def set_playbutton_image(self, set):
        self.button.remove(self.__last_playbutton_reference)
        if set == "play":
            self.button.add(self.__playimage)
            self.__last_playbutton_reference = self.__playimage
        elif set == "pause":
            self.button.add(self.__icon_pause)
            self.__last_playbutton_reference = self.__icon_pause
        if self.__last_playbutton_reference is not None:
            self.__last_playbutton_reference.show()

    def threaded_start_stop(self, index, delay, filepath=None, done=None, sub_path=None, *args):
        if int(index) > -1:
            Episode = self.__mySerie.Episodes[int(index)]
            self.__current_episode = Episode
            if filepath is None:
                filepath = lib.ProxerVideo.ProxerVideo.find_video(Episode, self.preferedSubType, self.updateStatus,
                                                                  check_other_langs=True,
                                                                  ignore_str=self.__ignore_filter)
                if filepath is not None and "evented" in filepath:
                    if done is not None:
                        if callable(done):
                            done()
                        else:
                            done.set_value(True)
                    return
        if filepath is not None:
            print("Episode URL: " + filepath)
        self.__last_url = filepath
        if delay is not None:
            if delay is not False:
                time.sleep(0.5)

        GObject.idle_add(self.start_stop_ui_stuff, filepath, self.__kodi_backend is None, sub_path=sub_path)
        if self.__kodi_backend is not None:
            pid = self.__kodi_backend.get_video_playlist_id()
            self.__kodi_backend.clear_playlist(pid)
            self.__plid = self.__kodi_backend.get_active_players()
            if not self.__kodi_backend.add_to_playlist(filepath, pid, True):
                GObject.idle_add(self.start_stop, "stop")
                print("Playing on kodi backend not possible!")
        self.__ignore_filter = None
        if done is not None:
            if callable(done):
                done()
            else:
                done.set_value(True)

    def start_stop_ui_stuff(self, filepath, play_lokal=True, sub_path=None):
        if self.player is not None:
            self.player.set_state(Gst.State.NULL)
            self.player = None
        self.player = self.create_player(self.on_message, self.on_sync_message)
        if filepath is None:
            self.set_title("No Video found for this Episode!")
            return
        from lib import Regex
        if os.path.isfile(filepath):
            filepath = os.path.realpath(filepath)
            self.set_playbutton_image("pause")
            if play_lokal:
                self.player.set_property("uri", "file://" + filepath)
                if sub_path is not None:
                    if os.path.isfile(sub_path):
                        self.player.set_property("suburi", "file://" + sub_path)
                        self.player.set_property("subtitle-font-desc", "Sans, 18")

                self.player.set_state(Gst.State.PLAYING)
            self.playing = True
        elif Regex.urlValidationRegex.match(filepath):
            self.set_playbutton_image("pause")
            if play_lokal:
                self.player.set_property("uri", filepath.replace(" ", "%20"))
                self.player.set_state(Gst.State.PLAYING)
            self.playing = True
        else:
            self.player.set_state(Gst.State.NULL)
            self.set_playbutton_image("play")
            self.timer.stop()
            self.playing = False

    def on_message(self, bus, message):
        t = message.type
        # print(t)
        if t == Gst.MessageType.EOS:
            self.player.set_state(Gst.State.NULL)
            self.set_playbutton_image("play")
            self.nextVideo()
        elif t == Gst.MessageType.ERROR:
            self.player.set_state(Gst.State.NULL)
            err, debug = message.parse_error()
            print("Error: %s" % err, debug)
            self.set_playbutton_image("play")
            if "gst-stream-error-quark" in str(err) or "gst-resource-error-quark" in str(err):
                try:
                    self.__ignore_filter = re.search("://.*?\..*?/", self.__last_url).group(0)
                    self.start_stop("restart")
                except Exception as e:
                    print(e)
        elif t == Gst.MessageType.QOS:
            qos = message.parse_qos()
            self.__handle_qos(*qos)
        elif t == Gst.MessageType.WARNING:
            warn = message.parse_warning()
            self.__handle_warning(*warn)
        elif t == Gst.MessageType.DURATION_CHANGED:
            self.haveDuration, duration = self.player.query_duration(Gst.Format.TIME)
            if not self.haveDuration:
                print("Duration unknown!")
                GObject.idle_add(self.show_duration, None)
            else:
                self.show_duration(duration)
        elif t == Gst.MessageType.STATE_CHANGED:
            if message.src == self.player:
                old_state, new_state, pending_state = message.parse_state_changed()
                print("Pipeline state changed from %s to %s." % (old_state.value_nick, new_state.value_nick))
                self.playing = (new_state == Gst.State.PLAYING)
                self.__player_state = new_state
                status_changed = self._connected.get("status-changed", None)
                if status_changed is not None:
                    if callable(status_changed):
                        status_changed()
                self.show_duration(None, err=95)
            else:
                print("Unexpected message received.")

        elif t == Gst.MessageType.BUFFERING:
            percent = message.parse_buffering()
            # if not self.haveDuration:
            #     self.haveDuration, duration = self.player.query_duration(Gst.Format.TIME)
            #     if not self.haveDuration:
            #         print("Duration unknown!")
            #     else:
            #         self.show_duration(duration)
            if percent < 95:
                self.__buffering = percent

                if self.playing:
                    if self.__force_position_secs > 5:
                        print("Found position to seek! Pausing playback and waiting for buffer to fill up...")
                        self.player.set_state(Gst.State.PAUSED)
                        self.playing = False
            else:
                self.__buffering = -1
                if self.__force_position_secs > 5 and not self.playing:
                    print("Found position to seek! Buffer full! Seeking and resuming playback...")
                    self.player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
                                            self.__force_position_secs * Gst.SECOND)
                    self.__force_position_secs = 0
                    self.player.set_state(Gst.State.PLAYING)
                    self.playing = True
        elif t == Gst.MessageType.NEED_CONTEXT:
            try:
                imagesink = message.src
                aspect = True
                try:
                    imagesink.set_property("force-aspect-ratio", True)
                except Exception as e:
                    print("Force aspect ratio property not set!")
                    aspect = False
                if self.movie_window is None:
                    print("setting xid... MovieWindow == None!")
                gdkWindow = self.movie_window.get_property('window')
                xid = gdkWindow.get_xid()
                if xid is not None:
                    print("Setting xid:" + str(xid))
                    imagesink.set_window_handle(xid)
                    if not aspect:
                        print("Restarting the player to ensure the video gets resized...")
                        self.start_stop("restart", keep_url=True)
            except AttributeError as ae:
                print("Unimportant message ignored!")
            except Exception as e:
                print("Setting Context failed!")
                print(e)
        else:
            print(t)

    def on_sync_message(self, bus, message):
        try:
            if message.get_structure().get_name() == 'prepare-window-handle':
                imagesink = message.src
                imagesink.set_property("force-aspect-ratio", True)
                if self.movie_window is None:
                    print("setting xid... MovieWindow == None!")
                gdkWindow = self.movie_window.get_property('window')
                xid = gdkWindow.get_xid()
                if xid is not None:
                    print("Setting xid:" + str(xid))
                    imagesink.set_window_handle(xid)
        except AttributeError as ae:
            print("Unimportant message ignored!")
        except Exception as e:
            print("setting context for player failed!")
            print(e)

    def on_window_state_change(self, window, event):
        print(window)
        print(event)
        test = gi.repository.Gdk.WindowState
        self.window_is_fullscreen = bool(
            test.FULLSCREEN & event.new_window_state)

    def toggle(self, event):
        doIt = False
        try:
            if event.keyval:
                doIt = True
        except:
            pass
        try:
            if event.get_label() == "Fullscreen" or event.get_child() is self.__fullimage:
                doIt = True
        except:
            pass
        if event is None:
            doIt = True
        if doIt:
            if self.window_is_fullscreen:
                self.unfullscreen()
            else:
                self.fullscreen()

    def setPreferedSubType(self, st: Gtk.RadioButton):
        self.preferedSubType = st.get_label()

    def setCurrentSerie(self, serie: lib.ProxerSerie.Serie):
        self.__mySerie = serie

    def __threadedInitEpList(self):
        for item in self.listBoxTracker:
            self.liste.remove(item)
            item.destroy()

        self.listBoxTracker = []
        self.listBoxDict = {}
        for child in self.__lang_buttons.get_children():
            self.__lang_buttons.remove(child)
            child.destroy()

        lastbutton = None
        for lang in self.__mySerie.Episodes[0].get_subtypes().keys():
            button = Gtk.RadioButton.new_with_label_from_widget(lastbutton, lang)
            button.connect("clicked", self.setPreferedSubType)
            self.__lang_buttons.pack_start(button, False, False, 0)
            lastbutton = button
            if "eng" in lang:
                button.set_active(True)
                self.preferedSubType = lang

        seen = self.__mySerie.Gesehen
        for i in range(0, len(self.__mySerie.Episodes)):
            cb = Gtk.CheckButton()
            if seen > i:
                cb.set_active(True)
            else:
                cb.set_active(False)
            cb.connect("toggled", self.set_episode_seen, i)
            lab = Gtk.Label("Episode " + str(i + 1))
            box = Gtk.VBox()
            box.pack_start(cb, False, False, 0)
            box.pack_start(lab, False, False, 5)
            row = Gtk.ListBoxRow()
            row.add(box)
            row.show_all()

            self.listBoxTracker.append(row)
            self.liste.add(row)
            self.listBoxDict[row] = i

        if self.__forceEpisode is None:
            try:
                self.select_listbox_row(self.listBoxTracker[seen])
            except IndexError:
                try:
                    self.select_listbox_row(self.listBoxTracker[seen - 1])
                except:
                    pass
        else:
            num = self.__mySerie.get_episode_num(self.__forceEpisode)
            if num is None:
                try:
                    self.select_listbox_row(self.listBoxTracker[seen])
                except IndexError:
                    try:
                        self.select_listbox_row(self.listBoxTracker[seen - 1])
                    except:
                        pass
            else:
                self.select_listbox_row(self.listBoxTracker[num])

        self.movie_window.show()
        self.show_all()
        self.liste.show_all()
        self.__load_stuff_done = True
        self.__notes_liste_frame.hide()
        self.__played_once = False

    def __threadedLoadStuff(self):
        self.show()
        lab = Gtk.Label("Informationen werden geladen...")
        row = Gtk.ListBoxRow()
        row.add(lab)
        self.listBoxTracker.append(row)
        self.liste.add(row)
        self.set_title("Proxer-Player (" + self.__mySerie.getName() + ")")
        self.show_all()
        # self.start_stop("stop")
        self.__load_stuff_done = True

    def threaded(self):
        self.__load_stuff_done = False
        GObject.idle_add(self.__threadedLoadStuff)
        while not self.__load_stuff_done:
            time.sleep(0.1)
        if self.__mySerie.Episodes is None or not self.__mySerie.Episodes:
            self.__mySerie.loadEpisodeList()
            self.__load_stuff_done = False
        time.sleep(1.5)
        GObject.idle_add(self.__threadedInitEpList)
        while not self.__load_stuff_done:
            time.sleep(0.1)
        time.sleep(1.5)
        GObject.idle_add(self.__threade_init_autotasks)

    def __threade_init_autotasks(self):
        if self.__force_play:
            self.start_stop("start", delay=True)

        if self.__force_fullscreen and not self.window_is_fullscreen:
            self.fullscreen()

    def __threaded_init_notes(self):
        cache = self.__get_marks_cache(self.__mySerie.get_id())
        for e in self.__notes_listbox_traker:
            self.__notes_liste.remove(e)
        for note in cache:
            # TODO read cache to listbox
            row = Gtk.ListBoxRow()
            box = Gtk.VBox()
            row.add(box)

            title = Gtk.Label(note.get("title", "kein Titel"))
            box.pack_start(title, False, False, 0)
            note_box = Gtk.HBox()
            box.pack_start(note_box)

            entrys = note.get("entrys", [])

            for en in entrys:
                pass
            pass

    def __handle_note_label_click(self, data):
        pass

    def initEpList(self, serie: lib.ProxerSerie.Serie, episode=None, play=False, fullscreen=False):
        if serie is not None:
            self.__mySerie = serie
        self.__forceEpisode = episode
        self.__force_play = play
        self.__force_fullscreen = fullscreen
        self._having_next = True
        t = threading.Thread(target=self.threaded)
        t.setName("DownloadEpListeThread")
        t.setDaemon(True)
        t.start()

    __visible = False

    def hide_stuff(self, *args, **kwargs):
        if self.__visible:
            self.extraBox.hide()
            self.__visible = False

            c = Gdk.Cursor.new_for_display(self.get_display(), Gdk.CursorType.BLANK_CURSOR)
            self.get_window().set_cursor(c)

    def show_stuff(self, *args, **kwargs):
        if not self.__visible:
            self.__visible = True
            self.extraBox.show()
            self.extraBox.show_all()
            c = Gdk.Cursor.new_from_name(self.get_display(), "default")
            self.get_window().set_cursor(c)



    __past = 0

    def motion_detect(self, *args, **kwargs):
        self.show_stuff()
        self.__past = time.time()

    def motion_check(self, *args, **kwargs):
        if self.__load_stuff_done:
            GObject.idle_add(self.motion_check_idle, kwargs.get("RepeatedTimerRef", None))

    def motion_check_idle(self, timer):
        success = False
        if self.__kodi_backend is None:
            try:
                if self.player is None:
                    success = False
                else:
                    success, position = self.player.query_position(Gst.Format.TIME)
                    position = float(position) / Gst.SECOND
                    self.__current_position = position
            except AttributeError as e:
                print(e)
                success = False
        else:
            r = self.__kodi_backend.get_playing_infos(self.__plid)
            timek = r.get("time", {})
            ttime = r.get("ttime", {})

            position = timek.get("hours", 0) * 60 + timek.get("minutes", 0) * 60 + timek.get("seconds", 0)
            ttimes = ttime.get("hours", 0) * 60 + ttime.get("minutes", 0) * 60 + ttime.get("seconds", 0)
            if position > 0:
                success = True
            self.play_progress.set_range(0, ttimes)

        if not success:
            # if self.destroyed():
            #     print("Found the world around me destroyed. I´m going to kill myself :( bye.")
            #     if timer() is not None:
            #         #timer().stop()
            #         pass
            # print("Position not updated!")
            pass
        elif self.play_progress is not None:
            self.play_progress.handler_block(self.slider_handler_id)
            self.play_progress.set_value(position)
            self.play_progress.handler_unblock(self.slider_handler_id)
            self.play_progress.set_restrict_to_fill_level(False)
            if self.__buffering > 0:
                val = (self.__duration / 100) * self.__buffering
            elif 0 < self.__download_percent < 101:
                val = (self.__duration / 100) * self.__download_percent
            else:
                val = self.play_progress.get_value()
            self.play_progress.set_fill_level(val)
            self.play_progress.set_show_fill_level(True)
            seeked = self._connected.get("seeked", None)
            if seeked is not None:
                if callable(seeked):
                    seeked(self, val)

        if (self.__past + 4.5) < time.time() and self.playing and self.__kodi_backend is None:
            self.hide_stuff()
        if not self.playing or self.__kodi_backend is not None or self.__playing_progess_show_override:
            self.show_stuff()

        if self.__next_pending:
            self.set_title("Remote: Spielt zurzeit etwas anderes ab!")
            if position > ttimes - 5:
                print("Try to go to next video...")
                self.nextVideo(False)

        self.__show_status_stuff()

    def __set_episode_seen_thread(self, *args, **kwargs):
        GObject.idle_add(self.__set_episode_gui, False, kwargs["button"], True)
        is_done = self.__mySerie.Episodes[kwargs["epNum"]].set_viewed()
        GObject.idle_add(self.__set_episode_gui, is_done, kwargs["button"], False)

    def set_episode_seen(self, button, epNum, *args):
        t = threading.Thread(target=self.__set_episode_seen_thread, kwargs={"epNum": epNum, "button": button})
        t.setName("set_episode_seen")
        t.setDaemon(True)
        t.start()

    def __set_episode_gui(self, is_done, button, beginning):
        if not beginning:
            button.set_active(is_done)
            button.set_label("Episode gesehen")
        else:
            button.set_label("Bitte warten...")

    def nextVideo(self, force=False, *args):
        self.__next_button_spinning(True)
        if self.next.get_active() or force:
            if self.__kodi_backend is not None:
                pid = self.__kodi_backend.get_video_playlist_id()
                pending = self.__kodi_backend.get_playlist_item_count(pid)
                if pending > 0:
                    self.__next_pending = True
                    return
                else:
                    self.__next_pending = False
            self.start_stop("stop")
            try:
                item = self.liste.get_selected_row()
                index = self.listBoxDict[item]
                nitem = self.listBoxTracker[index + 1]
                self.select_listbox_row(nitem)
                self.start_stop("start", done=lambda: self.__next_button_spinning(False))
            except IndexError:
                print("Keine Episode mehr übrig!")
                self.set_title("Ende")
                self._having_next = False
                self.__next_button_spinning(False)

    def next_episode_checkbox_handler(self, button):
        self.__get_player_config()["continueNextEpisode"] = self.next.get_active()

    def updateStatus(self, stri):
        if isinstance(stri, dict):
            # {"id": "QOS_WARN", "visible": True, "timeout": 10, "text": "Die Verbindung ist sehr langsam :("}
            not_id = stri.get("id", None)
            if not_id is not None:
                self.__notifications[not_id] = {"visible": stri.get("visible", True), "timeout": stri.get("timeout", 320), "text": stri.get("text", "")}
                return
            status = stri.get("status", None)
            try:
                if status is None:
                    return
                if "downloading" in status:
                    down_bytes = stri.get("downloaded_bytes", 0)
                    total_bytes = stri.get("total_bytes_estimate", None)
                    if total_bytes is None:
                        total_bytes = stri.get("total_bytes", 0)
                    eta = stri.get("eta", "0")
                    if eta is None:
                        eta = "not ready"
                    if total_bytes is None:
                        total_bytes = 1
                    if down_bytes is None:
                        down_bytes = 1
                    from libs.pretifier import Pretty as p

                    pretty_down_bytes = p.human_size(down_bytes)
                    pretty_total_bytes = p.human_size(total_bytes)
                    down_text = "Wird heruntergeladen (" + pretty_down_bytes + "/" + pretty_total_bytes + " ETA:" + str(
                            eta) + ")"
                    self.__notifications["EVENT_STATUS"] = {"visible": True, "timeout": 2, "text": down_text}
                    self.__download_percent = (int(down_bytes) / int(total_bytes)) * 100
                    filep = os.path.join(os.getcwd(), stri.get("tmpfilename", "none"))
                    if self.__last_url is None:
                        self.__last_url = "HierGiebtEsKeineVersteckteNachricht:D"
                    if filep not in self.__last_url and self.__download_percent > 5:
                        self.threaded_start_stop(-1, None, filepath=filep)
                    return
                elif "finished" in status:
                    filep = os.path.join(os.getcwd(), stri.get("filename", "none"))
                    self.__notifications["EVENT_STATUS"] = {"visible": True, "timeout": 2, "text": "Download abgeschlossen"}
                    self.__force_position_secs = self.__current_position
                    if filep is not None:
                        sub_p = os.path.join(os.getcwd(), stri.get("filename", "none").replace(".flv", ".deDE.ass"))
                        if not os.path.isfile(sub_p):
                            sub_p = os.path.join(os.getcwd(), stri.get("filename", "none").replace(".flv", ".enUS.ass"))
                            if not os.path.isfile(sub_p):
                                sub_p = None
                        self.threaded_start_stop(-1, None, filepath=filep, sub_path=sub_p)
                        stri = None
                    return
                elif "get_infos" in status:
                    self.__notifications["EVENT_STATUS"] = {"visible": True, "timeout": 2, "text": "Lade Informationen zum Video..."}
            except Exception as e:
                print(e)
                stri = None
        elif isinstance(stri, tuple):
            if "hide" in stri[0]:
                self.__notifications[time.time()] = {"visible": True, "timeout": 5, "text": stri[1]}
                return
        elif stri is None or stri == "":
            del self.__notifications["r/s"]
        else:
            self.__notifications["r/s"] = {"visible": True, "timeout": 1000, "text": stri}

    def __show_status_stuff(self):
        entrys = []
        delete = []
        for sid in self.__notifications.keys():
            entry = self.__notifications.get(sid, None)
            if entry is not None:
                entry["timeout"] -= __timer_tick__
                if entry["timeout"] < 0:
                    delete.append(sid)
                    continue
            if entry["visible"]:
                entrys.append(entry["text"].replace("[:COUNTDOWN:]", str(entry["timeout"])))
        for de in delete:
            try:
                del self.__notifications[de]
            except KeyError as e:
                print("Unable to remove a notification!")

        if len(entrys) > 0:
            GObject.idle_add(self.status.set_visible, True)
            import libs.pretifier as pretty
            GObject.idle_add(self.status.set_text, pretty.Pretty.list_to_string(entrys, separator="\n"))
        else:
            GObject.idle_add(self.status.set_visible, False)
            GObject.idle_add(self.status.set_text, "")

    def on_key_press_event(self, widget, event, user_data=None):
        key = Gdk.keyval_name(event.keyval)
        print(key)
        if key == "space":
            self.play_pause()
            return True
        elif key == "Escape":
            if self.window_is_fullscreen:
                self.unfullscreen()
            return True
        elif key == "Right":
            seek_time_secs = self.play_progress.get_value()
            self.player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
                                    (seek_time_secs + 30) * Gst.SECOND)
            return True
        elif key == "Left":
            seek_time_secs = self.play_progress.get_value()
            self.player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
                                    (seek_time_secs - 15) * Gst.SECOND)
            return True
        elif key == "n":
            self.nextVideo(force=True)
        elif key == "r":
            self.start_stop("restart")
        elif key == "f":
            self.toggle(None)
        elif key == "e":
            self.listExpander.set_expanded(not self.listExpander.get_expanded())
        return False

    def show_duration(self, durr, err=0):
        if durr is None:
            self.haveDuration, duration = self.player.query_duration(Gst.Format.TIME)
            durr = duration
            if self.haveDuration is False:
                if err < 100:
                    err += 1
                    GObject.idle_add(self.show_duration, None, err)
                    curr_changed = self._connected.get("current-changed", None)
                    if curr_changed is not None:
                        if callable(curr_changed):
                            curr_changed(self)

        if durr != 0:
            print("Duration: " + str(durr / Gst.SECOND))
            self.play_progress.set_range(0, durr / Gst.SECOND)
            self.__duration = durr / Gst.SECOND

    def on_slider_seek(self, widget):
        seek_time_secs = self.play_progress.get_value()
        self.seek(seek_time_secs)

    def seek(self, seek_time_secs):
        print("Seeking to " + str(seek_time_secs * Gst.SECOND) + " seconds...")
        self.player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
                                seek_time_secs * Gst.SECOND)

    def __get_player_config(self):
        myconf = self.dady.config.get("Player", None)
        if myconf is None:
            self.dady.config["Player"] = {}
            return self.__get_player_config()
        return myconf

    def on_host_combo_changed(self, combo):
        text = combo.get_active_text()
        if text != None:
            print("Selected: name=%s" % (text))
            if "hinzufügen" in text:
                hd = HostnameDialog(self)
                resp = hd.run()
                if resp == Gtk.ResponseType.OK:
                    name = hd.entName.get_text()
                    self.set_host(name)
                    conf = self.__get_player_config().get("hosts", None)
                    if conf is None:
                        conf = []
                        self.__get_player_config()["hosts"] = conf
                    conf.append(name)
                    self.__host_picker.append_text(name)
                hd.destroy()
            else:
                self.set_host(text)
        else:
            entry = combo.get_child()
            print("Entered: %s" % entry.get_text())
            self.__host_store.append([self.__host_store, entry.get_text()])
            self.set_host(entry.get_text())

    def set_host(self, hostname):
        self.__get_player_config()["activeHost"] = hostname
        if hostname == "lokal":
            self.__kodi_backend = None
        else:
            if self.__kodi_backend is None:
                if self.__mySerie is not None:
                    self.__kodi_backend = kodi.Kodi(self.__mySerie._pxreq, hostname)
            else:
                self.__kodi_backend.set_host(hostname)
            if self.playing:
                self.start_stop("restart")

    def destroy(self, no_threading=False):
        if no_threading:
            self.__destroy()
        else:
            t = threading.Thread(target=self.__destroy)
            t.setName("PlayerDestroyer")
            t.setDaemon(True)
            t.start()

    def __destroy(self):
        self.__kodi_backend = None
        bc = lib.BooleanContainer(False)
        print("Stopping video...")
        GObject.idle_add(self.start_stop, "stop", False, False, False, bc)
        while not bc.get_value():
            time.sleep(0.25)
        print("Video stopped!")
        time.sleep(0.5)
        print("Destroying playerWindow...")
        GObject.idle_add(self.__destroy_1)

    def __destroy_1(self):
        self.player = None
        self.movie_window.destroy()
        if self.timer is not None:
            self.timer.destroy()
            self.timer = None
        if not __no_dbus__:
            try:
                self.bus.close()
            except AttributeError:
                pass
        super(PxPlayerGTK, self).destroy()

    def connect_player(self, name, func):
        self._connected[name] = func

    def get_status(self):
        return self.__player_state

    def pause(self):
        GObject.idle_add(self.__pause)

    def play(self):
        GObject.idle_add(self.__play)

    def __play(self):
        if self.player is not None:
            self.player.set_state(Gst.State.PLAYING)
        self.set_playbutton_image("pause")

    def __pause(self):
        if self.player is not None:
            self.player.set_state(Gst.State.PAUSED)
        self.set_playbutton_image("play")

    def play_pause(self):
        if self.playing:
            self.pause()
        else:
            self.play()

    def have_next_video(self):
        return self._having_next

    def get_position(self):
        return self.__current_position

    def get_ep_num(self):
        return self.__mySerie.get_episode_num(self.__current_episode)

    def get_current_serie(self):
        return self.__mySerie

    def get_duration(self):
        return self.__duration

    def get_last_url(self):
        return self.__last_url if self.__last_url is not None else ""

    def __get_marks_cache(self, id):
        player_cache = self.__get_player_config()
        mark_cache = player_cache.get("marker", None)
        if mark_cache is None:
            player_cache["marker"] = {}
            return self.__get_marks_cache(id)
        id_cache = mark_cache.get(id, None)
        if id_cache is None:
            mark_cache[id] = []
            return self.__get_marks_cache(id)
        return id_cache

    def __notes_button_handler(self, *args, **kwargs):
        if self.__notes_button_visible:
            self.__notes_liste_frame.hide()
        else:
            if self.listExpander.get_expanded():
                self.listExpander.set_expanded(False)
            self.__notes_liste_frame.show()
        self.__notes_button_visible = not self.__notes_button_visible

    def __next_button_spinning(self, wahr):
        if wahr:
            self.__playing_progess_show_override = True
            self.__next_spinner.show_all()
            self.__next_spinner.start()
        else:
            self.__playing_progess_show_override = False
            self.__next_spinner.stop()
            self.__next_spinner.hide()

    def __handle_qos(self, live, running_time, stream_time, timestamp, duration):
        if self.__qos is None:
            self.__qos = {"message_count": 1, "messages": []}
        self.__qos["messages"].append((live, running_time, stream_time, timestamp, duration))

        dur = 0
        count = 0
        for m in self.__qos["messages"]:
            if dur != duration:
                dur = duration
            else:
                count += 1
        if count > 2:
            self.updateStatus({"id": "QOS_WARN", "visible": True, "timeout": 10, "text": "Die Verbindung ist sehr langsam :("})

    def __handle_warning(self, error, debug):
        self.updateStatus({"id": "QOS_WARN_2", "visible": True, "timeout": 10, "text": "Die Verbindung ist zu langsam! Neustart..."})
        self.__ignore_filter = re.search("://.*?\..*?/", self.__last_url).group(0)
        self.start_stop("restart")

    def get_osd_anchor_position_in_overlay(self, osd="control"):
        vals = {"bottom": Gtk.Align.END, "top": Gtk.Align.START}
        c = self.__get_player_config().get("osd_pos", None)
        if c is None:
            self.__get_player_config()["osd_pos"] = "bottom"
            return self.get_osd_anchor_position_in_overlay()
        if osd == "control":
            return vals[c]
        elif osd == "state":
            if c == "bottom":
                return vals["top"]
            else:
                return vals["bottom"]