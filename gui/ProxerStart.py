from os.path import expanduser

from gui import gtkResources

__author__ = 'marcel'

import threading
from gi.repository import Gtk, GObject
from gi.repository.GdkPixbuf import Pixbuf
import lib.ProxerNews
import lib.ProxerRequest
import lib.ProxerUser
import lib

__pxResourceCache__ = expanduser("~/.cache/proxerMe/Resources")


class PxStart(Gtk.Viewport):
    __gtype_name__ = "PxMainpageWidget"

    __notBox = None
    __ep_popover = None

    class serien_button(Gtk.Button):
        def __init__(self, serie, callback, name, *args, **kwargs):
            super().__init__(name, *args, **kwargs)
            self.__serie = serie
            self.__callback = callback
            self.connect("clicked", self.open_serie)

        def open_serie(self, *args, **kwargs):
            self.__callback(self.__serie)

    def __init__(self, pxReq: lib.ProxerRequest.ProxerRequest, pxUsr: lib.ProxerUser.User, parent, *args, **kwargs):
        super(PxStart, self).__init__(None, None)
        self.mainwindow = parent
        self.__pxReq = pxReq
        self.__usr = pxUsr
        self.__news = lib.ProxerNews.News(pxReq)
        self.__oldNews = []
        self.__mainBox = Gtk.VBox()
        self.__headerBox = Gtk.VBox()
        self.__contentBox = Gtk.HBox()

        ###### Make Header #######

        self.headerLoadingSpiner = Gtk.Spinner()
        self.lbLadelLade = Gtk.Label("Wird geladen...")
        self.__headerImage = Gtk.Image()

        self.__headerBox.set_homogeneous(False)
        self.__headerBox.add(self.headerLoadingSpiner)
        self.__headerBox.add(self.lbLadelLade)
        self.__headerBox.add(self.__headerImage)

        ###### Make NewsBox ########

        self.newsFeed = Gtk.ListBox()

        scrolling = Gtk.ScrolledWindow()
        viewport = Gtk.Viewport()
        scrolling.add(viewport)
        viewport.add(self.newsFeed)

        scrolling.connect("edge-reached", self.loadNextNews)
        # scrolling.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        # scrolling.set_hexpand(False)
        scrolling.set_vexpand(False)
        self.__contentBox.set_homogeneous(False)
        self.__contentBox.pack_start(scrolling, True, True, 0)

        self.__mainBox.pack_start(self.__headerBox, False, False, 0)
        self.__mainBox.pack_start(self.__contentBox, True, True, 0)
        self.add(self.__mainBox)
        scrolling.show_all()
        viewport.show_all()
        self.newsFeed.set_vexpand(False)
        self.newsFeed.show_all()
        self.newsFeed.set_selection_mode(Gtk.SelectionMode.NONE)

    def update(self):
        self.buildNews()
        self.buildNotifications()

    def build_all(self, is_thread=False, *args, **kwargs):
        if is_thread:
            self.__build_news()
            self.build_notifications_download()
        else:
            t = threading.Thread(target=self.build_all, args=(True, None))
            t.setDaemon(True)
            t.setName("Start_Backgroundtasks")
            t.start()


    def buildNews(self):
        self.headerLoadingSpiner.show_all()
        self.headerLoadingSpiner.start()
        self.lbLadelLade.set_text("Überprüfe loggin...")

        t = threading.Thread(target=self.__build_news)
        t.setDaemon(True)
        t.setName("NewsDownloader")
        t.start()

    def __build_news(self):
        import lib.ProxerLogin as logi
        from lib import global_no_login_manager
        login = logi.Login(self.__pxReq)
        lo = login.checkLogin()
        if lo.error:
            try:
                global_no_login_manager.checkException()
            except lib.NoLoginException:
                GObject.idle_add(self.lbLadelLade.set_text, "Fehler beim Anmelden...")
                import time
                time.sleep(1.0)
        if self.__usr is not None:
            args = ([{"Image": self.__headerImage, "URL": self.__usr.getHeaderUrl()}])
        else:
            args = ([{"Image": self.__headerImage, "URL": lib.ProxerUser.User.getHeaderUrl()}])
        t = threading.Thread(target=gtkResources.setImages, args=(__pxResourceCache__, self.__pxReq, args))
        t.setName("NewsHeaderDownloaderThread")
        t.setDaemon(True)
        t.start()

        for n in self.__oldNews:
            GObject.idle_add(self.newsFeed.remove, n)
            GObject.idle_add(n.destroy)
        self.__oldNews = []
        self.__news.reset()

        self.makeList(threaded=True)

    def makeListBeginUI(self):

        self.lbLadelLade.set_text("Lade News...")
        self.headerLoadingSpiner.start()

    def makeList(self, retry=False, threaded=False, *args, **kwargs):
        GObject.idle_add(self.makeListBeginUI)
        if threaded:
            self.makeListThread(True)
        else:
            t = threading.Thread(target=self.makeListThreadd)
            t.setDaemon(True)
            t.setName("NewsDownloader")
            t.start()

    def makeListThread(self, retry=False):
        try:
            if self.__usr is not None:
                nonRead = self.__usr.getNotifications().news
            else:
                nonRead = 0
        except lib.NoLoginException as e:
            if e.retry and retry:
                self.makeListThread(False)
                return
            else:
                nonRead = 0

        alist = self.__news.loadNextNews()
        GObject.idle_add(self.makeListUI, alist, nonRead)

    def makeListThreadd(self, *args, **kwargs):
        self.makeListThread(True)

    def makeListUI(self, alist, nonRead):
        imageList = []
        i = 0
        for a in alist:
            item = Gtk.ListBoxRow()
            itemBox = Gtk.HBox(2)

            # stack = Gtk.VBox()
            # aSpiner = Gtk.Spinner()
            # aSpiner.start()
            aPic = Gtk.Image()

            # stack.pack_start(aPic, False, False, 0)
            # stack.pack_start(aSpiner, False, False, 0)

            # GObject.idle_add(self.__buildImage, a.imageURL, aPic, stack, aSpiner)
            imageList.append({"URL": a.imageURL, "Image": aPic, "Stack": None, "Spinner": None})

            textBox = Gtk.VBox()
            itemBox.set_homogeneous(False)
            itemBox.pack_start(aPic, False, False, 0)

            if nonRead > 0:
                if i < nonRead:
                    i += 1
                    newImg = Gtk.Image()
                    pixbuf = Pixbuf.new_from_stream(gtkResources.getNewIO(), None)
                    newImg.set_from_pixbuf(pixbuf)
                    itemBox.pack_start(newImg, False, False, 0)

            itemBox.pack_start(textBox, True, True, 0)

            title = Gtk.Label(a.Titel)
            desc = Gtk.Label(a.Beschreibung)
            extended = Gtk.VBox()
            desc.set_line_wrap(True)

            textBox.pack_start(title, False, False, 0)
            textBox.pack_start(desc, True, True, 0)
            textBox.pack_start(extended, False, False, 0)

            zeit = Gtk.Label(a.Zeit)
            cat = Gtk.Label(a.category)
            views = Gtk.Label(a.Views)
            open = Gtk.LinkButton(a.ThreadURL, label="In Webbrowser öffnen")
            # open.connect("clicked", lambda x: webbrowser.open(a.ThreadURL, 2, True))

            extended.pack_start(zeit, False, False, 0)
            extended.pack_start(cat, False, False, 0)
            extended.pack_start(views, False, False, 0)
            extended.pack_start(open, False, False, 0)

            item.add(itemBox)

            self.newsFeed.add(item)
            self.__oldNews.append(item)
            item.show_all()
            itemBox.show_all()

        self.newsFeed.show_all()
        t = threading.Thread(target=gtkResources.setImages, args=(__pxResourceCache__, self.__pxReq, imageList))
        t.setDaemon(True)
        t.start()

    def loadNextNews(self, *args, **kwargs):
        if args[1] is Gtk.PositionType.BOTTOM:
            self.makeList()

    def build_notifications_begin(self):
        if self.__usr is None:
            self.__usr = lib.ProxerUser.User(self.__pxReq)
        if self.__usr is not None:
            if self.__notBox is not None:
                self.__contentBox.remove(self.__notBox)
                self.__notBox.destroy()

    def build_notifications_download(self, retry=True, *args, **kwargs):
        GObject.idle_add(self.build_notifications_begin)
        no_login = False
        notifics = None
        GObject.idle_add(self.lbLadelLade.set_text, "Rufe Benachrichtigungen ab...")
        try:
            notifics = self.__usr.getNotifications()
        except lib.NoLoginException as e:
            if e.retry and retry:
                self.build_notifications_download(False)
                return
            else:
                no_login = True
        new_eps = None
        if not no_login:
            GObject.idle_add(self.lbLadelLade.set_text, "Nach neuen Folgen suchen...")
            new_eps = self.check_new_episodes()
            if not new_eps:
                new_eps = None
        GObject.idle_add(self.build_notifications_end, notifics, True, no_login, new_eps)

    def build_notifications_end(self, notifics, retry=True, no_login=False, new_eps=None):
        notBox = Gtk.VBox()
        notBox.set_homogeneous(False)
        self.__notBox = notBox
        self.__contentBox.pack_start(notBox, False, False, 0)

        if no_login or notifics.err:
            errIma = Gtk.Image()
            pixbuf = Pixbuf.new_from_stream(gtkResources.getErrorIO(), None)
            errIma.set_from_pixbuf(pixbuf)
            errIma.connect("button-press-event", self.redownload)
            self.__notBox.pack_start(errIma, False, False, 0)
            self.lbLadelLade.set_text("Fehler beim Laden der Benachrichtigungen!")
            self.lbLadelLade.show()
            notBox.show_all()
        else:
            benBox = Gtk.VBox()
            benBox.set_homogeneous(False)
            notBox.pack_start(benBox, False, False, 0)
            benIma = Gtk.Image()
            pixbuf = Pixbuf.new_from_stream(gtkResources.getBellIO(), None)
            benIma.set_from_pixbuf(pixbuf)
            benBox.pack_start(benIma, False, False, 0)
            benLab = Gtk.Label(str(notifics.andere))
            benBox.pack_start(benLab, False, False, 0)

            freBox = Gtk.VBox()
            freBox.set_homogeneous(False)
            notBox.pack_start(freBox, False, False, 10)
            freIma = Gtk.Image()
            pixbuf = Pixbuf.new_from_stream(gtkResources.getFriendsIO(), None)
            freIma.set_from_pixbuf(pixbuf)
            freLab = Gtk.Label(str(notifics.freundschaftsAnfragen))
            freBox.pack_start(freIma, False, False, 0)
            freBox.pack_start(freLab, False, False, 0)

            mesBox = Gtk.VBox()
            mesBox.set_homogeneous(False)
            notBox.pack_start(mesBox, False, False, 10)
            notIma = Gtk.Image()
            pixbuf = Pixbuf.new_from_stream(gtkResources.getMessageIO(), None)
            notIma.set_from_pixbuf(pixbuf)
            notLab = Gtk.Label(str(notifics.pn))
            mesBox.pack_start(notIma, False, False, 0)
            mesBox.pack_start(notLab, False, False, 0)

            newBox = Gtk.VBox()
            newBox.set_homogeneous(False)
            notBox.pack_start(newBox, False, False, 10)
            newEvB = Gtk.EventBox()
            newIma = Gtk.Image()
            newEvB.add(newIma)
            pixbuf = Pixbuf.new_from_stream(gtkResources.getNewsIO(), None)
            newIma.set_from_pixbuf(pixbuf)
            newLab = Gtk.Label(str(notifics.news))
            newBox.pack_start(newEvB, False, False, 0)
            newBox.pack_start(newLab, False, False, 0)
            newEvB.connect("button-press-event", self.newsNotifcationBoxHandler)

            if new_eps is not None:
                if self.__ep_popover is not None:
                    self.__ep_popover.destroy()
                self.__ep_popover = self.build_new_eps_popover(new_eps)
                menButton = Gtk.MenuButton()
                mennewimg = Gtk.Image()
                pixbuf = Pixbuf.new_from_stream(gtkResources.getNewIO(), None)
                mennewimg.set_from_pixbuf(pixbuf)
                menButton.set_image(mennewimg)
                menButton.set_use_popover(True)
                menButton.set_direction(Gtk.DirectionType.LEFT)
                menButton.set_valign(Gtk.Align.CENTER)
                menButton.set_popover(self.__ep_popover)
                notBox.pack_start(menButton, False, False, 0)
        notBox.show_all()

        self.lbLadelLade.hide()
        self.lbLadelLade.set_text("")
        self.headerLoadingSpiner.stop()
        self.headerLoadingSpiner.hide()

    def buildNotifications(self, retry=False):
        t = threading.Thread(target=self.build_notifications_download)
        t.setDaemon(True)
        t.setName("NotificationsDownloader")
        t.start()


    def newsNotifcationBoxHandler(self, *args, **kwargs):
        self.__news.reset()
        self.__usr.getNotifications().resetNews()
        self.buildNews()
        self.buildNotifications()
        self.show_all()

    def redownload(self):
        t = threading.Thread(target=self.build_notifications_download, args=())
        t.setDaemon(True)
        t.start()

    def check_new_episodes(self):
        am_schauen = self.__usr.getAnmimeList()["seeing"]
        am_schauen = am_schauen + self.__usr.getMangaList()["seeing"]
        cache = self.mainwindow.serien_cache

        new_eps = []

        for s in am_schauen:
            serie = cache.get_serie_by_serienitem(s, nodownload=True)
            tup = serie.check_new_eps()
            if tup[0]:
                new_eps.append({"id": serie.get_id(), "newest_ep": tup[1], "seen": serie.Gesehen, "name": serie.getName()})
                serie.loadDetails(retry=True)
        return new_eps

    def build_new_eps_popover(self, eps):
        pop = Gtk.Popover()
        box = Gtk.HBox()
        scrolling = Gtk.ScrolledWindow()
        viewport = Gtk.Viewport()
        scrolling.add(viewport)
        viewport.add(box)
        pop.add(scrolling)
        scrolling.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)
        pop.set_hexpand(True)

        row = 0
        if eps is not None:
            for ep in eps:
                id = ep.get("id", None)
                newest_ep = ep.get("newest_ep", -1)
                gesehen = ep.get("seen", 0)
                name = ep.get("name", "kein Name")

                lb_name = PxStart.serien_button(id, self.push_serie_to_main, name)
                lb_neu = Gtk.Label(str(newest_ep - gesehen))
                bbox = Gtk.VBox()
                bbox.pack_start(lb_neu, False, False, 0)
                bbox.pack_start(lb_name, False, False, 10)
                box.pack_start(bbox, False, False, 0)
        else:
            pop.destroy()
            return None
        scrolling.show_all()
        box.show_all()
        return pop

    def push_serie_to_main(self, serie):
        cache = self.mainwindow.serien_cache
        s = cache.get_serie_by_id(serie, nodownload=True)
        self.mainwindow.serie_to(s)