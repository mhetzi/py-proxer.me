# -*- coding: utf-8 -*-
from time import sleep

__author__ = 'marcel'
__package__ = 'gui'

from os.path import expanduser
import threading
from gui import gtkResources

MODE_ANIME = "show_animes"
MODE_MANGA = "show-mangas"
MODE_MIXED = "show_mixed"

from gi.repository import Gtk, GObject
import lib.ProxerSerie
import lib.ProxerRequest
import lib
import lib.ProxerUser

# import gui.ProxerGTK
__pxResourceCache__ = expanduser("~/.cache/proxerMe/Resources")


class Relations(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Verbindungen", parent.main_window, 0,
                            (Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE))

        self.okasan = parent
        self.set_default_size(400, 350)
        self.__box = self.get_content_area()

        self.__mainBox = Gtk.VBox()
        self.__loading_label = Gtk.Label("Lade...")
        self.__mainBox.pack_start(self.__loading_label, False, False, 0)
        self.__box.add(self.__mainBox)
        self.__mainBox.show_all()
        self.clo = None
        self.po = None
        self.update_lists = None

    def build_relations(self):
        t = threading.Thread(target=self.__build_relations_thread)
        t.setName("relations_loading")
        t.setDaemon(True)
        t.start()

    def __build_relations_thread(self):
        sleep(0.5)
        rel = self.okasan.serie.get_relations()
        GObject.idle_add(self.__build_relations_gui, rel)

    def __build_relations_gui(self, rel: list):
        self.__box.remove(self.__mainBox)
        scrolling = Gtk.ScrolledWindow()
        viewport = Gtk.Viewport()
        scrolling.add(viewport)
        self.__box.pack_start(scrolling, True, True, 0)
        liste = Gtk.ListBox()
        viewport.add(liste)

        for r in rel:
            serie = lib.ProxerSerie.Serie(self.okasan.pxreq)
            serie.loadSearchResult(r)
            item = PxUserListItemGTK(None, serie, self.okasan.notebook, self.okasan.main_window, self.okasan.pxreq)
            item.on_opening = self.po
            item.on_closing = self.clo
            item.update_lists = self.update_lists
            irow = Gtk.ListBoxRow()
            item.show_all()
            irow.add(item)
            irow.show_all()
            liste.add(irow)
        liste.show_all()
        self.__box.show_all()
        # self.__mainBox.remove(self.__loading_spinner)
        self.__mainBox.remove(self.__loading_label)

        self.__loading_label.destroy()
        # self.__loading_spinner.destroy()


class PxUserListItemGTK(Gtk.HBox):
    __gtype_name__ = "PxUserListItem"
    __box = None
    __sp = None
    __widget = None
    __lab = None
    completely_loaded = False

    def __init__(self, image: Gtk.Image, Serie: lib.ProxerSerie.Serie, notebook: Gtk.Notebook, window,
                 Req: lib.ProxerRequest.ProxerRequest, no_gui=False, *args, **kwargs):
        super(PxUserListItemGTK, self).__init__(*args, **kwargs)
        # self.__mainBox = Gtk.VBox()
        # self.pack_start(image, False, False, 0)
        self.__notebook = notebook
        self.__window = window
        self.__req = Req
        self.serie = Serie
        self.on_closing = None
        self.on_opening = None
        self.update_lists = None

        if not no_gui:
            self.__contentBox = Gtk.VBox()

            self.__image = image

            self.__toolTip = Gtk.Tooltip()
            self.set_has_tooltip(True)
            if image is not None:
                self.connect("query-tooltip", self.__toolTipHandler)

            self.__name = Gtk.Label(Serie.getName())
            self.__name.set_line_wrap(True)
            self.__extendedBox = Gtk.HBox()
            self.__contentBox.pack_start(self.__name, False, False, 0)
            self.__contentBox.pack_start(self.__extendedBox, False, True, 0)
            self.__stand = Gtk.Label(str(Serie.Gesehen) + "/" + str(Serie.EpisodesInsgesammt))
            self.__media = Gtk.Label(Serie.Type)
            self.__extendedBox.pack_start(self.__media, False, False, 0)
            self.__extendedBox.pack_start(self.__stand, False, False, 20)

            if self.serie.AiringTime != "":
                self.__release = Gtk.Label(str(self.serie.AiringTime))
                self.__extendedBox.pack_end(self.__release, False, False, 0)
            self.__oButton = Gtk.Button("Öffne")
            self.__oButton.connect("clicked", self.addPage)

            self.pack_start(self.__contentBox, False, True, 0)
            self.pack_end(self.__oButton, False, False, 0)

    def addPage(self, button):
        t = threading.Thread(target=self.threaded)
        t.setName("NotebookPageCreatorThread")
        t.setDaemon(True)
        t.start()

    def threaded(self):
        GObject.idle_add(self.__createLoadingStuff)
        sleep(0.3)
        self.__widget = PxUserNoteEntryGTK(self.serie, self.__req, self.__window, self.__notebook)
        self.__widget.update_lists = self.update_lists
        GObject.idle_add(self.__cleanLoadingStuff)
        if self.on_opening is not None:
            try:
                self.on_opening(self.serie)
            except:
                pass
        self.__widget.page_closing = self.on_closing
        self.__widget.page_opening = self.on_opening
        self.completely_loaded = True

    def __cleanLoadingStuff(self):
        self.__box.pack_start(self.__widget, True, True, 0)
        self.__box.remove(self.__sp)
        self.__box.remove(self.__lab)
        self.__sp.stop()
        self.__sp.destroy()
        self.__lab.destroy()
        self.__box.show_all()

    def __createLoadingStuff(self):
        parent = self.get_parent()
        if parent is not None:
            parent.remove(self)
            print("__createLoadingStuff parent is none")
        try:
            if self.__contentBox is not None:
                self.__contentBox.destroy()
        except AttributeError:
            pass
        try:
            if self.__oButton is not None:
                self.__oButton.destroy()
        except AttributeError:
            pass
        self.__box = self
        self.__sp = Gtk.Spinner()
        self.__sp.start()
        lab = Gtk.Label("Lade Daten...")
        self.__box.pack_start(self.__sp, False, False, 0)
        self.__box.pack_start(lab, False, False, 0)
        self.__box.show_all()
        self.__lab = lab
        event_box = Gtk.EventBox()

        tab_box = Gtk.HBox(False, 2)
        tab_label = Gtk.Label(label=self.serie.getName())

        tab_button = Gtk.Button()
        tab_button.connect('clicked', self.on_closetab_button_clicked)

        # Add a picture on a button
        self._add_icon_to_button(tab_button)

        event_box.show()
        tab_button.show()
        tab_label.show()

        tab_box.pack_start(tab_label, True, False, 0)
        tab_box.pack_start(tab_button, True, False, 0)

        tab_box.show_all()
        event_box.add(tab_box)
        self.__notebook.append_page(self, event_box)

    def _add_icon_to_button(self, button):
        icon_box = Gtk.HBox()
        image = Gtk.Image()
        image.set_from_stock(Gtk.STOCK_CLOSE, Gtk.IconSize.MENU)
        Gtk.Button.set_relief(button, Gtk.ReliefStyle.NONE)

        settings = Gtk.Widget.get_settings(button)
        valid_, w, h = Gtk.icon_size_lookup_for_settings(settings,
                                                         Gtk.IconSize.MENU)
        Gtk.Widget.set_size_request(button, w + 4, h + 4)
        image.show()
        icon_box.pack_start(image, True, False, 0)
        button.add(icon_box)
        icon_box.show()

    def on_closetab_button_clicked(self, sender):
        self.__notebook.remove(self.__box)
        if self.__widget is not None:
            self.__widget.destroy()
        self.__widget = None
        self.__box.destroy()
        if self.on_closing is not None:
            try:
                self.on_closing(self.serie)
            except:
                pass

    def __toolTipHandler(self, widget, x, y, keyboard_mode, tooltip):
        tooltip.set_custom(self.__image)
        return True


class PxUserNoteEntryGTK(Gtk.VBox):
    __gtype_name__ = "PxUserNoteEntry"
    __is_manga = False

    def __init__(self, serie: lib.ProxerSerie.Serie, req: lib.ProxerRequest.ProxerRequest, window,
                 notebook: Gtk.Notebook, *args, **kwargs):
        serie.loadDetails()
        # serie.loadEpisodeList(alternative_crawler=False)
        self.serie = serie
        self.pxreq = req
        self.notebook = notebook
        self.main_window = window
        self.page_closing = None
        self.page_opening = None
        self.update_lists = None

        GObject.idle_add(self.__threded_init, args, kwargs)

    def __threded_init(self, *args, **kwargs):
        super(PxUserNoteEntryGTK, self).__init__()

        self.__ovInf = Gtk.HBox()
        self.__ovInf.set_homogeneous(False)
        self.pack_start(self.__ovInf, False, False, 0)
        self.__image = Gtk.Image()

        t = threading.Thread(target=gtkResources.setImages, args=(__pxResourceCache__, self.pxreq, [
            {"URL": self.serie.coverUrl, "Image": self.__image, "Stack": None, "Spinner": None}]))
        t.setName("SerienCoverThread")
        t.setDaemon(True)
        t.start()
        self.__panel = Gtk.VBox()
        self.__panel.pack_start(self.__image, False, False, 0)

        self.__set_handler_infos = {}
        notieren = Gtk.Button()
        notieren.connect("clicked", self.set_note)
        self.__panel.pack_start(notieren, False, False, 0)
        self.__set_handler_infos["noteButton"] = notieren
        noteLabel = Gtk.Label("Notieren")
        self.__set_handler_infos["noteChild"] = noteLabel
        self.__set_handler_infos["noteLabel"] = noteLabel
        notieren.add(noteLabel)

        abgeschlossen = Gtk.Button()
        abgeschlossen.connect("clicked", self.set_abgeschlossen)
        self.__panel.pack_start(abgeschlossen, False, False, 0)
        self.__set_handler_infos["DoneButton"] = abgeschlossen
        doneLabel = Gtk.Label("Abgeschlossen")
        self.__set_handler_infos["DoneChild"] = doneLabel
        self.__set_handler_infos["DoneLabel"] = doneLabel
        abgeschlossen.add(doneLabel)

        fav_button = Gtk.Button()
        fav_button.connect("clicked", self.set_favorite)
        self.__panel.pack_start(fav_button, False, False, 0)
        self.__set_handler_infos["FavButton"] = fav_button
        favLabel = Gtk.Label("Favorisieren")
        self.__set_handler_infos["FavChild"] = favLabel
        self.__set_handler_infos["FavLabel"] = favLabel
        fav_button.add(favLabel)

        self.__relations_button = Gtk.Button("Verbindungen")
        self.__relations_button.connect("clicked", self.__build_relations)
        self.__panel.pack_start(self.__relations_button, False, False, 0)

        self.__ovInf.pack_start(self.__panel, False, False, 0)

        self.__moreInf = Gtk.VBox()
        self.__moreInf.set_homogeneous(False)
        self.__ovInf.pack_start(self.__moreInf, True, True, 0)

        self.__names = Gtk.VBox()
        self.__namesFrame = Gtk.Frame()
        self.__namesFrame.set_label("Namen:")
        self.__namesFrame.add(self.__names)
        self.__moreInf.pack_start(self.__namesFrame, True, True, 0)
        if self.serie.OrigName is not None:
            self.__oName = Gtk.Label("Name: " + self.serie.OrigName)
            self.__names.pack_start(self.__oName, False, False, 0)
        if self.serie.EngName is not None:
            self.__eName = Gtk.Label("Englischer Name: " + self.serie.EngName)
            self.__names.pack_start(self.__eName, False, False, 0)
        if self.serie.GerName is not None:
            self.__dName = Gtk.Label("Deutscher Name: " + self.serie.GerName)
            self.__names.pack_start(self.__dName, False, False, 0)
        if self.serie.JapName is not None:
            self.__jName = Gtk.Label("Japanischer Name: " + self.serie.JapName)
            self.__names.pack_start(self.__jName, False, False, 0)

        self.__genresFrame = Gtk.Frame()
        self.__genresFrame.set_label("Genres: ")
        self.__moreInf.pack_start(self.__genresFrame, True, True, 0)
        self.__genresFrame.add(Gtk.Label(self.serie.getGenresAsString(", ")))

        if self.serie.getSynonymesAsString(", "):
            self.__synonyme = Gtk.Frame()
            self.__synonyme.set_label("Synonyme:")
            self.__synonymes = Gtk.VBox()
            for syn in self.serie.Synonym:
                lab = Gtk.Label(syn)
                lab.set_line_wrap(True)
                self.__synonymes.pack_start(lab, False, False, 0)
            self.__synonyme.add(self.__synonymes)
            self.__moreInf.pack_start(self.__synonyme, False, False, 0)

        fskPics = []
        if self.serie.FSK:
            self.__FSK = Gtk.HBox()
            self.__FSKFrame = Gtk.Frame()

            self.__moreInf.pack_start(self.__FSKFrame, True, True, 0)
            self.__FSKFrame.add(self.__FSK)
            self.__FSKFrame.set_label("FSK: ")
            for v in self.serie.FSK.keys():
                img = Gtk.Image()
                img.set_tooltip_text(v)
                fskPics.append({"URL": self.serie.FSK[v], "Image": img, "Stack": None, "Spinner": None})
                self.__FSK.pack_start(img, False, False, 10)

        flagPics = []
        if self.serie.Industrie:
            self.__Industrie = Gtk.VBox()
            self.__IndustrieFrame = Gtk.Frame()
            self.__IndustrieFrame.set_label("Industrie: ")
            self.__IndustrieFrame.add(self.__Industrie)
            self.__moreInf.pack_start(self.__IndustrieFrame, True, True, 0)

            for ind in self.serie.Industrie:
                industrieBox = Gtk.HBox()
                flagge = Gtk.Image()
                flagPics.append({"URL": ind["FlaggeURL"], "Image": flagge, "Stack": None, "Spinner": None})
                name = Gtk.Label()
                markupString = "<a href=\"" + ind["Link"] + "\"" + " title=\" \">" + ind["Name"] + "</a>"
                name.set_markup(markupString)
                art = Gtk.Label(ind["Art"])
                flagge.set_tooltip_text(ind["Countrycode"])

                industrieBox.pack_start(flagge, False, False, 0)
                industrieBox.pack_start(name, False, False, 10)
                industrieBox.pack_start(art, False, False, 10)
                self.__Industrie.pack_start(industrieBox, False, False, 0)

        t = threading.Thread(target=gtkResources.setImages, args=(__pxResourceCache__, self.pxreq, fskPics + flagPics))
        t.setName("SerienCoverThread")
        t.setDaemon(True)
        t.start()
        self.__seasonFrame = Gtk.Frame()
        self.__seasonFrame.set_label("Season: ")
        self.__seasonInfo = Gtk.VBox()
        self.__seasonFrame.add(self.__seasonInfo)
        self.__moreInf.pack_start(self.__seasonFrame, False, False, 0)
        if self.serie.AiringTime == "":
            self.__seasonInfo.pack_start(Gtk.Label(self.serie.AiringTime), False, False, 0)
        self.__seasonInfo.pack_start(Gtk.Label("Status: " + self.serie.AiringStatus), False, False, 0)

        self.set_homogeneous(False)
        descFrame = Gtk.Frame()
        descFrame.set_label("Beschreibung: ")
        descLab = Gtk.Label(self.serie.Beschreibung)
        descLab.set_line_wrap(True)
        scbox = Gtk.ScrolledWindow()
        scbox.add(descLab)
        descFrame.add(scbox)

        self.ep_combobox = Gtk.ComboBox()
        play_box = Gtk.HBox()


        self.pack_start(descFrame, True, True, 0)
        if self.serie.is_anime:
            btext = "Serie abspielen"
        else:
            btext = "Manga lesen"
        self.__playThis = Gtk.Button(btext)
        self.pack_start(self.__playThis, True, False, 0)
        self.__playThis.connect("clicked", lambda x: self.main_window.push_to_player(
            self.serie, play=True) if self.serie.is_anime else self.main_window.push_to_reader(self.serie))

        self.show_all()

    def __set_handler(self, typee: str):
        t = threading.Thread(target=self.__set_handler_thread, kwargs={"type": typee})
        t.setName("__set_handler")
        t.setDaemon(True)
        t.start()

    def __set_handler_beginning(self, a: dict):
        button = a["button"]
        sp = Gtk.Spinner()
        button.remove(a["Label"])
        button.add(sp)
        sp.start()
        sp.show()
        a["sp"] = sp

    def __set_handler_done(self, infos: dict):
        sp = infos["sp"]
        infos["button"].remove(sp)
        infos["button"].add(infos["Label"])
        sp.destroy()
        if self.update_lists is not None:
            if callable(self.update_lists):
                self.update_lists()

    def __set_handler_thread(self, *args, **kwargs):
        what_to_do = kwargs.get("type", "nothing")
        infos = {}
        infos["text"] = ""
        if what_to_do == "nothing":
            return
        if what_to_do == "finished":
            infos["button"] = self.__set_handler_infos["DoneButton"]
            infos["Label"] = self.__set_handler_infos["DoneLabel"]
            GObject.idle_add(self.__set_handler_beginning, infos)
            self.serie.set_finished()
            infos["text"] = "Abgeschlossen"
        elif what_to_do == "fav":
            infos["button"] = self.__set_handler_infos["FavButton"]
            infos["Label"] = self.__set_handler_infos["FavChild"]
            GObject.idle_add(self.__set_handler_beginning, infos)
            self.serie.set_favorite()
            infos["text"] = "Favorisieren"
        elif what_to_do == "notiz":
            infos["button"] = self.__set_handler_infos["noteButton"]
            infos["Label"] = self.__set_handler_infos["noteLabel"]
            GObject.idle_add(self.__set_handler_beginning, infos)
            self.serie.do_note()
            infos["text"] = "Notieren"
        GObject.idle_add(self.__set_handler_done, infos)

    def set_abgeschlossen(self, *args):
        self.__set_handler("finished")

    def set_favorite(self, *args):
        self.__set_handler("fav")

    def set_note(self, *args):
        self.__set_handler("notiz")

    def __build_relations(self, *args, **kwargs):
        dia = Relations(self)
        dia.build_relations()
        dia.po = self.page_opening
        dia.clo = self.page_closing
        dia.update_lists = self.update_lists
        # dia.run()
        val = dia.run()
        dia.destroy()


class PxUserListGTK(Gtk.VBox):
    __gtype_name__ = "PxUserListsViewer"
    ViewMode = "noInit"
    pxUser = None

    def __init__(self, viewMode, user: lib.ProxerUser.User, parent, no_lists=False, override_get_cache=None, *args, **kwargs):
        super(PxUserListGTK, self).__init__(*args, **kwargs)
        self.ViewMode = viewMode
        self.pxUser = user
        self.__window = parent
        # self.__mainBox = Gtk.VBox()
        self.__headerBox = Gtk.VBox()
        self.__listsBox = Gtk.VBox()
        self.__contentBox = Gtk.HBox()
        self.set_homogeneous(False)
        self.pack_start(self.__headerBox, False, True, 0)
        self.pack_start(self.__contentBox, True, True, 0)

        if callable(override_get_cache):
            self.__get_cache_last = override_get_cache

        if not no_lists:
            self.__contentBox.pack_start(self.__listsBox, True, True, 0)
            self.__listsBox.pack_start(Gtk.Label("Am schauen"), False, False, 10)
            self.__serienSeeningListe = Gtk.ListBox()
            self.__serienSeeningListTracker = []
            scrollingSeening = Gtk.ScrolledWindow()
            viewportSeening = Gtk.Viewport()
            scrollingSeening.add(viewportSeening)
            viewportSeening.add(self.__serienSeeningListe)
            self.__listsBox.pack_start(scrollingSeening, True, True, 0)

            self.__listsBox.pack_start(Gtk.Label("Fertig geschaut"), False, False, 10)
            self.__serienSeenListe = Gtk.ListBox()
            self.__serienSeenListTracker = []
            scrollingSeen = Gtk.ScrolledWindow()
            viewportSeen = Gtk.Viewport()
            scrollingSeen.add(viewportSeen)
            viewportSeen.add(self.__serienSeenListe)
            self.__listsBox.pack_start(scrollingSeen, True, True, 0)

            self.__listsBox.pack_start(Gtk.Label("Wird noch geschaut"), False, False, 10)
            self.__serienToSeeListe = Gtk.ListBox()
            self.__serienToSeeListTracker = []
            scrollingToSee = Gtk.ScrolledWindow()
            viewportToSee = Gtk.Viewport()
            scrollingToSee.add(viewportToSee)
            viewportToSee.add(self.__serienToSeeListe)
            self.__listsBox.pack_start(scrollingToSee, True, True, 0)

            self.__listsBox.pack_start(Gtk.Label("Abgebrochen"), False, False, 10)
            self.__serienCancledListe = Gtk.ListBox()
            self.__serienCancledListTracker = []
            scrollingCancled = Gtk.ScrolledWindow()
            viewportCancled = Gtk.Viewport()
            scrollingCancled.add(viewportCancled)
            viewportCancled.add(self.__serienCancledListe)
            self.__listsBox.pack_start(scrollingCancled, True, True, 0)

        self.__openSeries = Gtk.Notebook()
        self.__openSeries.set_scrollable(True)
        self.__contentBox.pack_start(self.__openSeries, True, True, 0)

        self.show_all()

    def get_notebook(self):
        return self.__openSeries

    def get_selected_serie(self):
        noteEntryInt = self.__openSeries.get_current_page()
        if noteEntryInt == -1:
            return
        noteEntry = self.__openSeries.get_nth_page(noteEntryInt)
        return noteEntry.serie

    def select_serie(self, serie, no_select=False):
        for i in range(0, self.__openSeries.get_n_pages()):
            page = self.__openSeries.get_nth_page(i)
            tserie = page.serie
            if serie.get_id() == tserie.get_id():
                if not no_select:
                    self.__openSeries.set_current_page(i)
                return True
        return False

    def update(self):
        userlist = None
        """ More threading please """
        #todo threads
        try:
            if self.ViewMode == MODE_ANIME:
                userlist = self.pxUser.getAnmimeList()
            elif self.ViewMode == MODE_MANGA:
                userlist = self.pxUser.getMangaList()
            elif self.ViewMode == MODE_MIXED:
                userlist = []
                userlist = userlist + self.pxUser.getAnmimeList()
                userlist = userlist + self.pxUser.getMangaList()
        except lib.NoLoginException as e:
            userlist = "Not logged in!"
        for i in self.__serienSeenListTracker:
            self.__serienSeenListe.remove(i)
            i.destroy()
        self.__serienSeenListTracker = []

        for i in self.__serienCancledListTracker:
            self.__serienCancledListe.remove(i)
            i.destroy()
        self.__serienCancledListTracker = []

        for i in self.__serienSeeningListTracker:
            self.__serienSeeningListe.remove(i)
            i.destroy()
        self.__serienSeeningListTracker = []

        for i in self.__serienToSeeListTracker:
            self.__serienToSeeListe.remove(i)
            i.destroy()
        self.__serienToSeeListTracker = []

        if userlist == "Not logged in!":
            userlist = {}
            userlist["seeing"] = "Nicht angemeldet"
            userlist["toSee"] = "Nicht angemeldet"
            userlist["cancled"] = "Nicht angemeldet"
            userlist["seen"] = "Nicht angemeldet"

        imageList = []

        imageList = imageList + self.__fillInList(userlist["seeing"], self.__serienSeeningListe,
                                                  self.__serienSeeningListTracker)
        imageList = imageList + self.__fillInList(userlist["toSee"], self.__serienToSeeListe,
                                                  self.__serienToSeeListTracker)
        imageList = imageList + self.__fillInList(userlist["seen"], self.__serienSeenListe,
                                                  self.__serienSeenListTracker)
        imageList = imageList + self.__fillInList(userlist["cancled"], self.__serienCancledListe,
                                                  self.__serienCancledListTracker)

        t = threading.Thread(target=gtkResources.setImages,
                             args=(__pxResourceCache__, self.pxUser.pxreq, imageList, None, None, True))
        t.setDaemon(True)
        t.start()

    def __fillInList(self, items, listbox: Gtk.ListBox, listboxTracker: list):
        imageList = []

        if items == "Nicht angemeldet":
            itemRow = Gtk.ListBoxRow()
            itemRow.add(Gtk.Label(items))
            listbox.add(itemRow)
            listboxTracker.append(itemRow)
            itemRow.show_all()
            listbox.show_all()
            return []

        for si in items:
            mainPic = Gtk.Image()
            imageList.append({"URL": si.CoverUrl, "Image": mainPic, "Stack": None, "Spinner": None})
            serie = self.__window.serien_cache.get_serie_by_serienitem(si, nodownload=True)
            item = PxUserListItemGTK(mainPic, serie, self.__openSeries, self.__window, self.pxUser.pxreq)
            item.on_closing = self.on_page_close
            item.on_opening = self.add_to_search_cache
            item.update_lists = self.update
            itemRow = Gtk.ListBoxRow()
            itemRow.add(item)
            listbox.add(itemRow)
            listboxTracker.append(itemRow)
            itemRow.show_all()
        listbox.show_all()
        return imageList

    def prepFullscreen(self, mode):
        pass

    def __get_cache_last(self):
        """
        :return dict:dict
        """
        mystuff = self.__window.config.get("UC.Cache." + self.ViewMode, None)
        if mystuff is None:
            self.__window.config["UC.Cache." + self.ViewMode] = {}
            return self.__get_cache_last()
        last = mystuff.get("zo", None)
        if last is None:
            mystuff["zo"] = {}
            return self.__get_cache_last()
        return last

    def on_page_close(self, serie: lib.ProxerSerie.Serie):
        id_sort = lambda dicd: dicd["id"] if dicd["id"] != "none" else dicd["name"]
        last = self.__get_cache_last()
        del last[serie._MainURL]
        try:
            del last[serie._MainURL + "#top"]
        except:
            pass
        try:
            del last[serie._MainURL.replace("#top", "")]
        except:
            pass
        tl = []
        for i in last.keys():
            last[i]["url"] = i
            tl.append(last[i])
        tl.sort(key=id_sort)
        last.clear()
        current_id = 0
        for e in tl:
            el = {}
            last[e["url"]] = el
            el["name"] = e["name"]
            el["id"] = current_id
            current_id += 1
        last.sort(key=id_sort)

    def add_to_search_cache(self, serie: lib.ProxerSerie.Serie):
        last = self.__get_cache_last()
        tl = last.get(serie._MainURL, None)
        tl2 = last.get(serie._MainURL + "#top", None)
        if tl is None and tl2 is None:
            tl = {}
            last[serie._MainURL] = tl
            tl["name"] = serie.getName()
            tl["id"] = len(last)
            self.__window.save_config()
        else:
            return "already in cache"

    def reopen_pages(self):
        last = self.__get_cache_last()
        if not last:
            return
        usrlist = []
        if self.pxUser is not None:
            usr = self.pxUser
        else:
            return
        aliste = usr.getAnmimeList()
        for k in aliste.keys():
            usrlist.extend(aliste[k])
        aliste = usr.getMangaList()
        for k in aliste.keys():
            usrlist.extend(aliste[k])
        found_list = []
        tmp_list = []
        del_list = []
        for t in last.keys():
            if t.replace("#top", "") in tmp_list:
                del_list.append(t)
            else:
                tmp_list.append(t.replace("#top", ""))
        for dell in del_list:
            del last[dell]
        dell = None
        tmp_list = None
        for i in last.keys():
            found = False
            for s in usrlist:
                if i == s.mainUrl.replace("#top", ""):
                    found = True
                    se = self.__window.serien_cache.get_serie_by_serienitem(s)
                    if se in found_list:
                        continue
                    try:
                        found_list.append((se, last[i]["id"]))
                    except:
                        found_list.append((se, 1000))
            if not found:
                se = self.__window.serien_cache.get_serie_by_url(i)
                if se in found_list:
                    continue
                try:
                    se.OrigName = last[i]["name"]
                except:
                    se.OrigName = last[i]
                try:
                    found_list.append((se, last[i]["id"]))
                except:
                    found_list.append((se, 1000))
        found_list.sort(key=lambda tup: tup[1])
        for f in found_list:
            cc = lib.ClassContainer(None)
            GObject.idle_add(self.reopen_pages_idle, cc, f[0])
            while True:
                if cc.get_value() is not None:
                    if cc.get_value().completely_loaded:
                        break
                sleep(0.1)

    def reopen_pages_idle(self, cc: lib.ClassContainer, serie):
        if self.select_serie(serie, no_select=True):
            return
        item = PxUserListItemGTK(None, serie, self.__openSeries, self.__window,
                                 self.pxUser.pxreq, True)
        item.addPage(None)
        item.on_closing = self.on_page_close
        item.on_opening = self.add_to_search_cache
        item.update_lists = self.update
        if cc is not None:
            cc.set_value(item)
