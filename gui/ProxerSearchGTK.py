__author__ = 'marcel'

from gi.repository import Gtk, GObject

import gui.ProxerUserLists
import lib.ProxerRequest
import lib.ProxerSearch
import lib.ProxerSerie
import lib.ProxerUser
import lib
import weakref

class PxSearchWidget(Gtk.Viewport):
    __gtype_name__ = "PxSearchWidget"

    pxSearch = None
    genres = []
    exGenres = []
    Type = ""
    searchLastItems = None

    resultListBox = None

    getUserFromMainWindow = None

    expander = None
    topBox = None
    suchEntry = None
    __lambda_liste = {}

    def do_search(self, text, x=None, y=None):
        if self.suchEntry is not None:
            if self.suchEntry.get_text() != text and isinstance(text, str):
                self.suchEntry.set_text(text)
        if self.resultListBox is not None and self.searchLastItems is not None:
            for item in self.searchLastItems:
                self.resultListBox.remove(item)
                item.destroy()

        try:
            self.pxSearch.setGenres(self.genres)
            self.pxSearch.setExcludeGenres(self.exGenres)
            items = self.pxSearch.suche(self.suchEntry.get_text())
        except AttributeError as e:
            lbr = Gtk.ListBoxRow()
            lab = Gtk.Label.new_with_mnemonic("Nichts gefunden!")
            lbr.add(lab)
            lbr.show_all()
            self.searchLastItems = [lbr]
            self.resultListBox.add(lbr)
            print(e)
            return

        self.searchLastItems = []

        for item in items:
            name = item["name"]
            url = item["url"]
            genres = item["Genres"]
            Typ = item["Typ"]
            Bewertung = item["Bewertung"]
            openitem = item

            lbr = Gtk.ListBoxRow()
            self.resultListBox.add(lbr)
            self.searchLastItems.append(lbr)

            topBox = Gtk.HBox()
            topBox.set_homogeneous(False)
            lbr.add(topBox)

            descBox = Gtk.VBox()
            lbName = Gtk.Label("Name: " + name)
            lbName.set_line_wrap(True)

            genStr = ""
            for gen in genres:
                genStr = genStr + " " + gen

            lbTg = Gtk.Label(Typ + "/" + genStr)  # ("Type/Genre: " + Typ + "/" + genStr)
            lbTg.set_line_wrap(True)
            lbWertung = Gtk.Label("Bewertung: " + Bewertung)
            descBox.add(lbName)
            descBox.add(lbTg)
            descBox.add(lbWertung)

            actionBox = Gtk.VBox()
            openButton = Gtk.Button("Öffne")

            serie = self.__window.serien_cache.get_serie_by_searchresult(item)

            ulitem = gui.ProxerUserLists.PxUserListItemGTK(None, serie, self.__list.get_notebook(), self.__window,
                                                           self.pxSearch.pxReq, True)
            ulitem.on_closing = self.__list.on_page_close
            ulitem.on_opening = self.__list.add_to_search_cache
            self.__lambda_liste[openButton] = {"ui": ulitem, "b": openButton, "s": serie}
            openButton.connect("clicked", ulitem.addPage)
            actionBox.add(openButton)

            topBox.add(descBox)
            topBox.add(actionBox)

            lbr.show_all()
        self.resultListBox.show_all()
        self.resultListBox.show_now()

    def genreHandler(self, button, exclude):
        genre = button.get_label()
        if not exclude:
            if button.get_active():
                self.genres.append(genre)
            else:
                self.genres.remove(genre)
        else:
            if button.get_active():
                self.exGenres.append(genre)
            else:
                self.exGenres.remove(genre)

    def __init__(self, pxReq: lib.ProxerRequest.ProxerRequest, window, *args, **kwargs):
        super(PxSearchWidget, self).__init__(None, None)
        self.__window = window
        self.pxSearch = lib.ProxerSearch.AdvancedSearch(pxReq)
        mainBox = Gtk.HBox()
        topBox = Gtk.VBox(3)
        self.add(mainBox)
        mainBox.pack_start(topBox, True, True, 0)

        self.__list = user_list_wrapper(window.pxUser, window, self, self.get_cache_last)
        mainBox.pack_start(self.__list, True, True, 0)
        self.__list.get_notebook().connect("switch-page", self.handle_page_changed)

        suchEntry = Gtk.Entry()
        iconPos = Gtk.EntryIconPosition.SECONDARY
        suchEntry.set_icon_from_icon_name(iconPos, "system-search")
        suchEntry.set_icon_activatable(iconPos, True)
        suchEntry.connect("activate", self.do_search)
        suchEntry.connect("icon-press", self.do_search)
        suchEntry.set_hexpand(False)
        suchEntry.set_hexpand_set(False)
        topBox.set_homogeneous(False)
        topBox.pack_start(suchEntry, expand=False, fill=False, padding=0)

        self.suchEntry = suchEntry

        expander = Gtk.Expander.new_with_mnemonic("Optionen")
        expandedBox = Gtk.VBox()
        expandedBox.set_homogeneous = False
        expandedBox2 = Gtk.VBox()
        expander.add(expandedBox2)
        topBox.pack_start(expander, False, True, 0)
        expander.connect("notify::expanded", self.handleExpanded)
        self.expander = expander
        self.topBox = topBox

        typeFrame = Gtk.Frame()
        typeFrame.set_label("Types")
        typeBox = Gtk.Grid()
        expandedBox.add(typeFrame)
        typeFrame.add(typeBox)

        scrolling = Gtk.ScrolledWindow()
        viewport = Gtk.Viewport()
        scrolling.add(viewport)
        expandedBox2.add(scrolling)
        viewport.add(expandedBox)

        frb = None

        x = 0
        y = 0

        for Type in self.pxSearch.getTypes():
            trb = Gtk.RadioButton.new_with_label_from_widget(frb, Type)
            trb.connect("toggled", lambda x: self.pxSearch.setType(x.get_label()))
            typeBox.attach(trb, x, y, 1, 1)
            x += 1
            if x > 4:
                x = 0
                y += 1
            if frb is None:
                frb = trb

        genFrame = Gtk.Frame()
        genFrame.set_label("Genre")
        genBox = Gtk.Grid()
        genFrame.add(genBox)
        expandedBox.add(genFrame)

        x = 0
        y = 0

        for gen in self.pxSearch.getAllGenres:
            cb = Gtk.CheckButton.new_with_label(gen)
            cb.connect("toggled", lambda x: self.genreHandler(x, False))
            genBox.attach(cb, x, y, 1, 1)
            x += 1
            if x > 4:
                x = 0
                y += 1

        genExFrame = Gtk.Frame()
        genExFrame.set_label("Genre ausschließen")
        genExBox = Gtk.Grid()
        genExFrame.add(genExBox)
        expandedBox.add(genExFrame)

        x = 0
        y = 0

        for gen in self.pxSearch.getAllGenres:
            cb = Gtk.CheckButton.new_with_label(gen)
            cb.connect("toggled", lambda x: self.genreHandler(x, True))
            genExBox.attach(cb, x, y, 1, 1)
            x += 1
            if x > 4:
                x = 0
                y += 1

        sortFrame = Gtk.Frame()
        sortFrame.set_label("Sortierung")
        sortBox = Gtk.Grid()
        sortFrame.add(sortBox)
        expandedBox.add(sortFrame)

        x = y = 0

        frb = None
        for sort in self.pxSearch.getAllSortierungen:
            trb = Gtk.RadioButton.new_with_label_from_widget(frb, sort)
            trb.connect("toggled", lambda x: self.pxSearch.setSortierung(x.get_label()))
            sortBox.attach(trb, x, y, 1, 1)
            x += 1
            if x > 4:
                x = 0
                y += 1
            if frb is None:
                frb = trb

        scrolling = Gtk.ScrolledWindow()
        viewport = Gtk.Viewport()
        scrolling.add(viewport)
        self.resultListBox = Gtk.ListBox()
        viewport.add(self.resultListBox)
        # viewport.add(Gtk.Label.new_with_mnemonic("Test"))
        self.resultListBox.set_selection_mode(Gtk.SelectionMode.NONE)
        topBox.pack_start(scrolling, True, True, 0)

        send_to_page_button = Gtk.Button("Zu Animes hinzufügen")
        send_to_page_button.connect("clicked", self.handle_send_button)
        self.__send_to_page_button = send_to_page_button
        topBox.pack_end(send_to_page_button, False, False, 0)

        topBox.show_all()

    def handle_send_button(self, *args, **kwargs):
        s = self.__list.get_selected_serie()
        self.__window.serie_to(s)

    def handle_page_changed(self, notebook, page, *args, **kwargs):
        if page.serie.is_anime:
            self.__send_to_page_button.set_label("Zu Animes hinzufügen")
        else:
            self.__send_to_page_button.set_label("Zu Mangas hinzufügen")

    def handleExpanded(self, *args):

        if self.expander is None or self.topBox is None:
            print("Expander reference is None")
            return
        if self.expander.get_expanded():
            self.topBox.set_child_packing(self.expander, True, True, 0, Gtk.PackType.START)
        else:
            self.topBox.set_child_packing(self.expander, False, True, 0,
                                          Gtk.PackType.START)  # set_child_packing(child, expand, fill, padding, pack_type)

    def get_cache_last(self):
        """
        :return dict:dict
        """
        mystuff = self.__window.config.get("Search.Cache", None)
        if mystuff is None:
            self.__window.config["Search.Cache"] = {}
            return self.get_cache_last()
        last = mystuff.get("Zuletzt-offen", None)
        if last is None:
            mystuff["Zuletzt-offen"] = {}
            return self.get_cache_last()
        return last

    def reopen_pages(self):
        self.__list.reopen_pages()

class user_list_wrapper(gui.ProxerUserLists.PxUserListGTK):

    def __init__(self, user: lib.ProxerUser.User, mainwindow, searchwindow, ogc, *args, **kwargs):
        super().__init__(gui.ProxerUserLists.MODE_MIXED, user, mainwindow, no_lists=True,override_get_cache=ogc, *args, **kwargs)
        searchwindow = weakref.ref(searchwindow)
        self.__my_search_parent = searchwindow

    def __get_cache_last(self):
        real_val = self.__my_search_parent()
        if real_val is not None:
            return real_val.get_cache_last()
        raise AttributeError("searchwindow has no get_cache_last() method")
